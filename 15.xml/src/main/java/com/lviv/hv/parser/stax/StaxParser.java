package com.lviv.hv.parser.stax;

import com.lviv.hv.Model.Device;
import com.lviv.hv.Model.DeviceGroup;
import com.lviv.hv.Model.Port;
import com.lviv.hv.Model.Type;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class StaxParser {

  static final String DEVICE_TAG = "device";
  static final String NAME_TAG = "name";
  static final String ORIGIN_TAG = "origin";
  static final String PRICE_TAG = "price";
  static final String CRITICAL_TAG = "critical";
  static final String TYPES_TAG = "types";
  static final String PERIPHERAL_TAG = "peripheral";
  static final String DEVICE_GROUP_TAG = "deviceGroup";
  static final String ENERGY_CONSUMPTION_TAG = "energyConsumption";
  static final String COOLER_TAG = "cooler";
  static final String PORTS_TAG = "ports";
  static final String PORT_TAG = "port";

  static final String DEVICE_ID_ATTRIBUTE = "deviceId";
  static final String DEVICE_GROUP_ID_ATTRIBUTE = "id";

  private XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

  private List<Device> devices = new ArrayList<>();
  private Device device;
  private Type type;
  private DeviceGroup deviceGroup;
  private List<Port> ports;

  public List<Device> parse(File xml) {
    try {
      XMLEventReader xmlEventReader = xmlInputFactory
          .createXMLEventReader(new FileInputStream(xml));
      while (xmlEventReader.hasNext()) {
        XMLEvent xmlEvent = xmlEventReader.nextEvent();
        if (xmlEvent.isStartElement()) {
          StartElement startElement = xmlEvent.asStartElement();
          String name = startElement.getName().getLocalPart();
          switch (name) {
            case DEVICE_TAG: {
              device = new Device();
              Attribute deviceId = startElement.getAttributeByName(new QName(DEVICE_ID_ATTRIBUTE));
              if (deviceId != null) {
                device.setDeviceId(Integer.parseInt(deviceId.getValue()));
              }
              break;
            }
            case NAME_TAG: {
              xmlEvent = xmlEventReader.nextEvent();
              device.setName(xmlEvent.asCharacters().getData());
              break;
            }
            case ORIGIN_TAG: {
              xmlEvent = xmlEventReader.nextEvent();
              device.setOrigin(xmlEvent.asCharacters().getData());
              break;
            }
            case PRICE_TAG: {
              xmlEvent = xmlEventReader.nextEvent();
              device.setPrice(Integer.parseInt(xmlEvent.asCharacters().getData()));
              break;
            }
            case CRITICAL_TAG: {
              xmlEvent = xmlEventReader.nextEvent();
              device.setCritical(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
              break;
            }
            case TYPES_TAG: {
              type = new Type();
              break;
            }
            case PERIPHERAL_TAG: {
              xmlEvent = xmlEventReader.nextEvent();
              type.setPeripheral(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
              break;
            }
            case DEVICE_GROUP_TAG: {
              deviceGroup = new DeviceGroup();
              Attribute id = startElement.getAttributeByName(new QName(DEVICE_GROUP_ID_ATTRIBUTE));
              deviceGroup.setId(Integer.parseInt(id.getValue()));
              xmlEvent = xmlEventReader.nextEvent();
              deviceGroup.setName(xmlEvent.asCharacters().getData());
              break;
            }
            case ENERGY_CONSUMPTION_TAG: {
              xmlEvent = xmlEventReader.nextEvent();
              type.setEnergyConsumption(Integer.parseInt(xmlEvent.asCharacters().getData()));
              break;
            }
            case COOLER_TAG: {
              xmlEvent = xmlEventReader.nextEvent();
              type.setCooler(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
              break;
            }
            case PORTS_TAG: {
              ports = new ArrayList<>();
              break;
            }
            case PORT_TAG: {
              xmlEvent = xmlEventReader.nextEvent();
              Port port = new Port(xmlEvent.asCharacters().getData());
              ports.add(port);
              break;
            }
          }
        }

        if (xmlEvent.isEndElement()) {
          EndElement endElement = xmlEvent.asEndElement();
          String name = endElement.getName().getLocalPart();
          switch (name) {
            case DEVICE_TAG: {
              devices.add(device);
              device = null;
              break;
            }
            case TYPES_TAG: {
              device.setType(type);
              type = null;
              break;
            }
            case DEVICE_GROUP_TAG: {
              type.setDeviceGroup(deviceGroup);
              deviceGroup = null;
              break;
            }
            case PORTS_TAG: {
              type.setPorts(ports);
              ports = null;
              break;
            }
          }
        }
      }
    } catch (XMLStreamException | FileNotFoundException e) {
      e.printStackTrace();
    }
    return devices;
  }
}

