package com.lviv.hv.parser.dom;

import com.lviv.hv.Model.Device;
import com.lviv.hv.Model.DeviceGroup;
import com.lviv.hv.Model.Port;
import com.lviv.hv.Model.Type;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DOMParser {

  public static List<Device> parse(Document document) {
    List<Device> devices = new ArrayList<>();
    NodeList nodeList = document.getElementsByTagName("device");
    for (int index = 0; index < nodeList.getLength(); index++) {
      Device device = new Device();
      Type type;
      Node node = nodeList.item(index);

      Element element = (Element) node;
      device.setDeviceId(Integer.parseInt(element.getAttribute("deviceId")));
      device.setName(element.getElementsByTagName("name").item(0).getTextContent());
      device.setOrigin(element.getElementsByTagName("origin").item(0).getTextContent());
      device.setPrice(Integer.parseInt(element.getElementsByTagName("price").item(0)
          .getTextContent()));
      device.setCritical(Boolean.parseBoolean(element.getElementsByTagName("critical").item(0)
          .getTextContent()));

      type = getType(element.getElementsByTagName("types"));
      device.setType(type);
      devices.add(device);
    }
    return devices;
  }

  private static Type getType(NodeList nodeList) {
    Type type = new Type();
    DeviceGroup deviceGroup = new DeviceGroup();

    if (nodeList.item(0).getNodeType() == Node.ELEMENT_NODE) {
      Element element = (Element) nodeList.item(0);
      type.setPeripheral(Boolean.parseBoolean(element.getElementsByTagName("peripheral").item(0)
          .getTextContent()));
      type.setEnergyConsumption(Integer.parseInt(element.getElementsByTagName("energyConsumption")
          .item(0).getTextContent()));
      type.setCooler(Boolean.parseBoolean(element.getElementsByTagName("cooler")
          .item(0).getTextContent()));

      Element deviceGroupElement = (Element) element.getElementsByTagName("deviceGroup").item(0);
      deviceGroup.setId(Integer.parseInt(deviceGroupElement.getAttribute("id")));
      deviceGroup.setName(deviceGroupElement.getTextContent());
      type.setDeviceGroup(deviceGroup);

      type.setPorts(getPorts(element.getElementsByTagName("port")));
    }
    return type;
  }

  private static List<Port> getPorts(NodeList nodeList) {
    List<Port> ports = new ArrayList<>();
    for (int index = 0; index < nodeList.getLength(); index++) {
      Port port = new Port();
      Node node = nodeList.item(index);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        Element element = (Element) node;
        port.setName(element.getTextContent());
        ports.add(port);
      }
    }
    return ports;
  }
}
