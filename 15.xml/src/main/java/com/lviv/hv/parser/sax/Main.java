package com.lviv.hv.parser.sax;

import com.lviv.hv.Model.Device;
import java.io.File;
import java.util.List;

public class Main {

  public static void main(String... args) {
    File xmlFile = new File("src\\main\\resources\\xml\\devices.xml");
    File xsdFile = new File("src\\main\\resources\\xml\\devicesXSD.xsd");

    List<Device> devices = SaxParser.parse(xmlFile, xsdFile);
    System.out.println(devices);

  }

}
