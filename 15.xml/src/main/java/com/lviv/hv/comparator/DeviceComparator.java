package com.lviv.hv.comparator;

import com.lviv.hv.Model.Device;

import java.util.Comparator;

public class DeviceComparator implements Comparator<Device> {

  @Override
  public int compare(Device o1, Device o2) {
    return o1.getDeviceId() - o2.getDeviceId();
  }
}
