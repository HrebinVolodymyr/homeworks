package com.lviv.hv.Model;

public class Device {

  private int deviceId;
  private String name;
  private String origin;
  private int price;
  private Type type;
  private boolean critical;

  public int getDeviceId() {
    return deviceId;
  }

  public void setDeviceId(int deviceId) {
    this.deviceId = deviceId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getOrigin() {
    return origin;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  public Type getType() {
    return type;
  }

  public void setType(Type type) {
    this.type = type;
  }

  public boolean isCritical() {
    return critical;
  }

  public void setCritical(boolean critical) {
    this.critical = critical;
  }

  @Override
  public String toString() {
    return "Device{" +
        "deviceId=" + deviceId +
        ", name='" + name + '\'' +
        ", origin='" + origin + '\'' +
        ", price=" + price +
        ", type=" + type +
        ", critical=" + critical +
        "}\n";
  }
}
