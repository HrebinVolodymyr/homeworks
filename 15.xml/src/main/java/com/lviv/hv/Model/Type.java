package com.lviv.hv.Model;

import java.util.ArrayList;
import java.util.List;

public class Type {

  private DeviceGroup deviceGroup;
  private boolean peripheral;
  private int energyConsumption;
  private boolean cooler = false;
  private List<Port> ports = new ArrayList<Port>();

  public void addPort(Port port) {
    ports.add(port);
  }

  public DeviceGroup getDeviceGroup() {
    return deviceGroup;
  }

  public void setDeviceGroup(DeviceGroup deviceGroup) {
    this.deviceGroup = deviceGroup;
  }

  public boolean isPeripheral() {
    return peripheral;
  }

  public void setPeripheral(boolean peripheral) {
    this.peripheral = peripheral;
  }

  public int getEnergyConsumption() {
    return energyConsumption;
  }

  public void setEnergyConsumption(int energyConsumption) {
    this.energyConsumption = energyConsumption;
  }

  public boolean isCooler() {
    return cooler;
  }

  public void setCooler(boolean cooler) {
    this.cooler = cooler;
  }

  public List<Port> getPorts() {
    return ports;
  }

  public void setPorts(List<Port> ports) {
    this.ports = ports;
  }

  @Override
  public String toString() {
    return "Type{" +
        "deviceGroup=" + deviceGroup.toString() +
        ", peripheral=" + peripheral +
        ", energyConsumption=" + energyConsumption +
        ", cooler=" + cooler +
        ", ports=" + ports +
        '}';
  }
}
