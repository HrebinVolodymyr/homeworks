package com.epam.hv;

public class Connection implements AutoCloseable {

  private String dbName;
  private boolean closed;

  Connection(String dbName) {
    super();
    this.dbName = dbName;
    closed = false;
  }

  public String getDbName() throws Exception {
    if (true) {
      throw new Exception("Сan't get DB name");
    }
    return dbName;
  }

  public boolean isClosed() {
    return closed;

  }

  public void close() throws Exception {
    Thread.sleep(500);
    if (true) {
      throw new Exception("Сan't close connection");
    }
    closed = true;
    System.out.println("*** C L O S E D ***");
  }
}
