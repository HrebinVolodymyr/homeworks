/*
 * Copyright (c) 2018, EPAM.
 */

package com.epam.hv;

import static com.epam.hv.Printer.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Class {@code Main} is the root of the program.
 *
 * @author Hrebin Vladimir
 * @version ver.1.0
 */
public class Main {

  public static void main(String... args) {
    Main main = new Main();
    main.go();
  }

  private void go() {
    printHead();
    while (true) {
      Interval interval = getInterval();
      int sizeFibonacciSet = getSizeFibonacciSet();

      printDelimiter();
      printOutput("Odd numbers from start to end: ");
      println(interval.getOddNumbers());
      printOutput("Even numbers from end to start: ");
      println(reverseArray(interval.getEvenNumbers()));

      printOutput("The sum of odd numbers: ");
      println(sumUpNumbers(interval.getOddNumbers()));
      printOutput("The sum of even numbers: ");
      println(sumUpNumbers(interval.getEvenNumbers()));

      FibonacciCalculator fibonacciCalc = new FibonacciCalculator(
          maxValue(interval.getOddNumbers()),
          maxValue(interval.getEvenNumbers()),
          sizeFibonacciSet);
      long[] fibonacciSet = fibonacciCalc.getFibonacciSet();
      printDelimiter();
      println(Printer.ANSI_RED + "F I B O N A C C I   S E T :" + Printer.ANSI_RESET);
      println(fibonacciSet);
      printOutput("The percentage of odd and even Fibonacci numbers:  ");
      println(fibonacciCalc.getPercentageOddAndEven());
      printDelimiter();
    }
  }

  private long maxValue(long[] array) {
    long max = array[0];
    for (long number : array) {
      if (number > max) {
        max = number;
      }
    }
    return max;
  }

  private long[] reverseArray(long[] array) {
    for (int leftIndex = 0; leftIndex < (array.length / 2); leftIndex++) {
      int rightIndex = (array.length - 1) - leftIndex;
      long temp = array[leftIndex];
      array[leftIndex] = array[rightIndex];
      array[rightIndex] = temp;
    }
    return array;
  }

  private long sumUpNumbers(long[] array) {
    long sum = 0L;
    for (long number : array) {
      sum += number;
    }
    return sum;
  }

  private String readLine() {
    String line = null;
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    try {
      line = br.readLine();
    } catch (IOException e) {
      System.out.println("IOException");
    }
    return line;
  }

  private Interval getInterval() {
    Interval interval;
    while (true) {
      printMessage("Enter the interval (for example: 1;100)");
      printInput();
      String data = readLine();
      if (data.equals("q")) {
        System.exit(0);
      }
      try {
        interval = new Interval(data);
      } catch (IllegalArgumentException e) {
        printError(e.getMessage());
        continue;
      } catch (ArrayIndexOutOfBoundsException e) {
        printError("One number is entered");
        continue;
      }
      break;
    }
    return interval;
  }

  private int getSizeFibonacciSet() {
    int sizeFibonacciSet;
    while (true) {
      printMessage("Enter the size of the Fibonacci set ");
      printInput();
      try {
        sizeFibonacciSet = Integer.parseInt(readLine());
        if (sizeFibonacciSet < 2) {
          throw new NumbersComparisonException("Set size should be more than 2.");
        }
      } catch (IllegalArgumentException e) {
        printError(e.getMessage());
        continue;
      }
      break;
    }
    return sizeFibonacciSet;
  }

}
