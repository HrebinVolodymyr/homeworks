package com.epam.hv;

public class NumbersComparisonException extends IllegalArgumentException {

  NumbersComparisonException(String message) {
    super(message);
  }

  NumbersComparisonException(String message, Throwable cause) {
    super(message, cause);
  }
}
