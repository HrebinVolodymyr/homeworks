/*
 * Copyright (c) 2018, EPAM.
 */
package com.epam.hv;

/**
 * Class {@code FibonacciCalculator} calculates Fibonacci numbers.
 *
 * @author Hrebin Vladimir
 * @version ver.1.0
 */
class FibonacciCalculator {

  private long[] fibonacciNumbers;

  FibonacciCalculator(final long first, final long second, final int sizeSet) {
    long firsNumber = first;
    long secondNumber = second;
    long thirdNumber;
    long[] fibonacciNumbers = new long[sizeSet];
    fibonacciNumbers[0] = first;
    fibonacciNumbers[1] = second;
    for (int i = 2; i < sizeSet; i++) {
      thirdNumber = firsNumber + secondNumber;
      fibonacciNumbers[i] = thirdNumber;
      firsNumber = secondNumber;
      secondNumber = thirdNumber;
    }
    this.fibonacciNumbers = fibonacciNumbers;
  }

  public String getPercentageOddAndEven() {
    Interval fibonacciInterval = new Interval(fibonacciNumbers);
    int oddNumbersCount = fibonacciInterval.getOddNumbers().length;
    int evenNumbersCount = fibonacciInterval.getEvenNumbers().length;
    double oddNumbersPercent =
        (oddNumbersCount * 1.00 / (oddNumbersCount + evenNumbersCount)) * 100.00;
    double evenNumbersPercent = 100 - oddNumbersPercent;
    return (String.format("%.2f",oddNumbersPercent) + " / " + String.format("%.2f",evenNumbersPercent));
  }

  long[] getFibonacciSet() {
    return fibonacciNumbers;
  }
}
