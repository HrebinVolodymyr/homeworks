package com.epam.hv;

public class TestTryWithResources {

  public static void main(String... args) {

    try (Connection connection = new Connection("mySQL")) {

      System.out.println("DB name: " + connection.getDbName());
      System.out.println("Is connection closed: " + connection.isClosed());

    } catch (Exception e) {
      System.out.println("-----*****" + e.getMessage() + "*****-----");
      e.printStackTrace();
    }

  }

}
