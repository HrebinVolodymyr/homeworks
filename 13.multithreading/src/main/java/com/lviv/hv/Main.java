package com.lviv.hv;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class Main {

  public static void main(String... args)
      throws Exception {
//    1. Write simple “ping-pong” program using wait() and notify().
    com.lviv.hv.waitNotify.Main main = new com.lviv.hv.waitNotify.Main();
    main.main();

//    2. Create a task that produces a sequence of n Fibonacci numbers, where n is provided
//    to the constructor of the task. Create a number of these tasks and drive them using
//    threads.
//    executeTask2();

//    3. Repeat previous exercise using the different types of executors.
//    executeTask3();

//    4. Modify Exercise 2 so that the task is a Callable that sums the values of all the Fibonacci
//    numbers. Create several tasks and display the results.
//    executeTask4();

//    5. Create a task that sleeps for a random amount of time between 1 and 10 seconds,
//    then displays its sleep time and exits. Create and run a quantity (given on the command
//    line) of these tasks. Do it by using ScheduledThreadPool.
//    executeTask5();

//    6. Create a class with three methods containing critical sections that all synchronize on
//    the same object. Create multiple tasks to demonstrate that only one of these methods
//    can run at a time. Now modify the methods so that each one synchronizes on a different
//    object and show that all three methods can be running at once.
//    com.lviv.hv.synchronize.Main main = new com.lviv.hv.synchronize.Main();
//    main.main();

//    7. Write program in which two tasks use a pipe to communicate.
//    com.lviv.hv.pipeThread.Main main = new com.lviv.hv.pipeThread.Main();
//    main.main();
  }

  public static void executeTask2() {
    ThreadGroup fibonacciCalculatorThreads = new ThreadGroup("FibonacciCalculatorThreads");
    for (int i = 1; i <= 5; i++) {
      FibonacciCalculator fCalculator = new FibonacciCalculator(20);
      Thread thread = new Thread(fCalculator);
      thread.start();
    }
  }

  public static void executeTask3() throws InterruptedException, ExecutionException {
    System.out.println("********** Executors.newSingleThreadExecutor() **********");
    ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();
    singleThreadExecutor.execute(new FibonacciCalculator(10));
    Future<Long> future = singleThreadExecutor.submit(new FibonacciCalculatorCallable(10));
    while (true) {
      Thread.sleep(10);
      boolean isDone = future.isDone();
      System.out.println("Is done: " + isDone);
      if (isDone) {
        System.out.println("Sum of Fibonacci Numbers: " + future.get());
        break;
      }
    }
    singleThreadExecutor.shutdown();

    Thread.sleep(1000);

    System.out.println();
    System.out.println("********** Executors.newFixedThreadPool() **********");
    ExecutorService fixedThreadPool = Executors.newFixedThreadPool(4);
    for (int i = 1; i <= 10; i++) {
      FibonacciCalculator fCalculator = new FibonacciCalculator(10);
      fixedThreadPool.execute(fCalculator);
    }
    fixedThreadPool.shutdown();

    Thread.sleep(2500);

    System.out.println();
    System.out.println("********** Executors.newWorkStealingPool() **********");
    ExecutorService workStealingPool = Executors.newWorkStealingPool();
    for (int i = 1; i <= 10; i++) {
      FibonacciCalculator fCalculator = new FibonacciCalculator(10);
      workStealingPool.execute(fCalculator);
    }
    Thread.sleep(2500);
    workStealingPool.shutdown();

    System.out.println();
    System.out.println("********** Executors.newCachedThreadPool() **********");
    ExecutorService cachedThreadPool = Executors.newCachedThreadPool();
    ThreadPoolExecutor pool = (ThreadPoolExecutor) cachedThreadPool;
    System.out.println(pool.getPoolSize());
    for (int i = 1; i <= 10; i++) {
      FibonacciCalculator fCalculator = new FibonacciCalculator(10);
      cachedThreadPool.execute(fCalculator);
      System.out.println("POOL SIZE: " + pool.getPoolSize());
    }
    cachedThreadPool.shutdown();
  }

  public static void executeTask4()
      throws InterruptedException, TimeoutException, ExecutionException {
    Thread.sleep(1000);

    System.out.println();
    System.out.println("********** Executors.newFixedThreadPool() **********");
    ExecutorService fixedThreadPool = Executors.newFixedThreadPool(4);
    List<FibonacciCalculatorCallable> tasks = new ArrayList<>();
    for (int i = 1; i <= 10; i++) {
      FibonacciCalculatorCallable fCalculator = new FibonacciCalculatorCallable(10);
      tasks.add(fCalculator);
    }
    System.out.println("Before invokeAll()");
    List<Future<Long>> futures = fixedThreadPool.invokeAll(tasks);
    System.out.println("After invokeAll()");
    for (Future<Long> future : futures) {
      long sum = future.get();
      System.out.println("Sum of Fibonacci Numbers: " + sum);
    }
    fixedThreadPool.shutdown();
  }

  public static void executeTask5() throws InterruptedException, ExecutionException {
    System.out.println("********** Executors.newSingleThreadScheduledExecutor() **********");
    ScheduledExecutorService scheduledExecutor = Executors
        .newSingleThreadScheduledExecutor();
    for (int i = 1; i <= 3; i++) {
      int delay = randInt(1, 10);
      System.out.println("TASK:" + i);
      System.out.println("Schedule delay: " + delay);
      ScheduledFuture<String> future =
          scheduledExecutor.schedule(new Task(), delay, TimeUnit.SECONDS);
      while (true) {
        System.out.println("Delay: " + future.getDelay(TimeUnit.SECONDS));
        boolean isDone = future.isDone();
        if (isDone) {
          System.out.println(future.get());
          break;
        }
        Thread.sleep(1000);
      }
    }
    scheduledExecutor.shutdown();
  }

  private static int randInt(int min, int max) {
    Random rand = new Random();
    int randomNum = rand.nextInt((max - min) + 1) + min;
    return randomNum;
  }

}
