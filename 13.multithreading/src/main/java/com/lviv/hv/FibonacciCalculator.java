/*
 * Copyright (c) 2018, EPAM.
 */
package com.lviv.hv;

import java.util.Arrays;

class FibonacciCalculator implements Runnable {

  private int sizeSet;

  public FibonacciCalculator(int sizeSet) {
    this.sizeSet = sizeSet;
  }

  public long[] getFibonacciSet(final int sizeSet) {
    long firsNumber = 0;
    long secondNumber = 1;
    long thirdNumber;
    long[] fibonacciNumbers = new long[sizeSet];
    fibonacciNumbers[0] = firsNumber;
    fibonacciNumbers[1] = secondNumber;
    for (int i = 2; i < sizeSet; i++) {
      thirdNumber = firsNumber + secondNumber;
      fibonacciNumbers[i] = thirdNumber;
      firsNumber = secondNumber;
      secondNumber = thirdNumber;
    }
    return fibonacciNumbers;
  }

  public void run() {
    long[] numbers = getFibonacciSet(sizeSet);
    System.out.println(Thread.currentThread().getName() + ": " + Arrays.toString(numbers));

    try {
      Thread.sleep(50);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    for (long number : numbers) {
      System.out.println(Thread.currentThread().getName() + ": " + number);

      try {
        Thread.sleep(50);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}
