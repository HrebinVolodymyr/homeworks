package com.lviv.hv.waitNotify;

import java.util.Date;
import java.util.LinkedList;
import java.util.Queue;

public class Main {

  private static Queue<String> queue = new LinkedList<>();

  public static void main(String... args) {

    Thread writer = new Thread(() -> {
      synchronized (queue) {
        while (true) {
          if (queue.size() != 0) {
            try {
              queue.wait();
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
          System.out.println("Began to add");
          for (int i = 0; i < 5; i++) {
            queue.add(new Date().toString());
            try {
              Thread.sleep(2000);
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
          System.out.println("Added:" + queue.size());
          queue.notify();
        }
      }
    });

    Thread reader = new Thread(() -> {
      synchronized (queue) {
        while (true) {
          if (queue.size() == 0) {
            try {
              queue.wait();
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
          while (queue.size() > 0) {
            System.out.println(queue.remove());
            try {
              Thread.sleep(2000);
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }
          queue.notify();
        }
      }
    });

    writer.setName("WRITER");
    reader.setName("READER");

    writer.start();
    reader.start();

  }

}
