package com.lviv.hv.pipeThread;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.PipedReader;
import java.io.PipedWriter;

public class Printer implements Runnable {

  PipedWriter toClient;
  PipedReader fromClient;

  public Printer(PipedWriter toClient, PipedReader fromClient) {
    this.toClient = toClient;
    this.fromClient = fromClient;
  }

  @Override
  public void run() {
    System.out.println("******* PRINTER STARTED ******* ");
    try (
        BufferedReader bufferedReader = new BufferedReader(fromClient);
        BufferedWriter bufferedWriter = new BufferedWriter(toClient)) {
      while (true) {
        String text = bufferedReader.readLine();
        if (text.equals("FINISH")) {
          continue;
        }
        if (text != null) {
          Thread.sleep(100);
          System.out.println(text);
          bufferedWriter.write("NEXT" + "\n");
          bufferedWriter.flush();
        } else {
          break;
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
