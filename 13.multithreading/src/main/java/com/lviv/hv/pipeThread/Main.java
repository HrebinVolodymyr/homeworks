package com.lviv.hv.pipeThread;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PipedReader;
import java.io.PipedWriter;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Main {

  public static void main(String... args) throws Exception {
    PipedWriter toPrinter = new PipedWriter();
    PipedReader fromClient = new PipedReader(toPrinter);

    PipedWriter toClient = new PipedWriter();
    PipedReader fromPrinter = new PipedReader(toClient);

    ScheduledExecutorService pool = Executors.newScheduledThreadPool(3);
    pool.schedule(new Printer(toClient, fromClient), 1, TimeUnit.SECONDS);
    pool.schedule(new Client(toPrinter, fromPrinter), 3, TimeUnit.SECONDS);
    pool.schedule(new Client(toPrinter, fromPrinter), 9, TimeUnit.SECONDS);
    //pool.shutdown();
  }

}
