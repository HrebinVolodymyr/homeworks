package com.lviv.hv.pipeThread;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PipedReader;
import java.io.PipedWriter;

public class Client implements Runnable {

  PipedWriter toPrinter;
  PipedReader fromPrinter;

  public Client(PipedWriter toPrinter, PipedReader fromPrinter) {
    this.toPrinter = toPrinter;
    this.fromPrinter = fromPrinter;
  }

  @Override
  public void run() {
    System.out.println("******* CLIENT STARTED ******* ");
    BufferedReader bufferedReader = new BufferedReader(fromPrinter);
    BufferedWriter bufferedWriter = new BufferedWriter(toPrinter);
    try {
      String[] lines = text.split("\n");
      bufferedWriter.write(lines[0] + "\n");
      bufferedWriter.flush();
      for (int index = 1; index < lines.length; index++) {
        String command = bufferedReader.readLine();
        if (command.equals("NEXT")) {
          bufferedWriter.write(lines[index] + "\n");
          bufferedWriter.flush();
        }
      }
      bufferedWriter.write("FINISH" + "\n");
      bufferedWriter.flush();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  String text = "1.\tТарас Григорович Шевченко\n"
      + "2.\t«Тополя»\n"
      + "3.\tПолюбила чорнобрива\n"
      + "4.\tКозака дівчина.\n"
      + "5.\tПолюбила – не спинила:\n"
      + "6.\tПішов – та й загинув...\n"
      + "7.\tЯкби знала, що покине –\n"
      + "8.\tБуло б не любила;\n"
      + "9.\tЯкби знала, що загине —\n"
      + "10.\tБуло б не пустила;\n"
      + "11.\tЯкби знала – не ходила б\n"
      + "12.\tПізно за водою,\n"
      + "13.\tНе стояла б до півночі\n"
      + "14.\tЗ милим під вербою…\n"
      + "15.\tМинув і рік, минув другий –\n"
      + "16.\tКозака немає;\n"
      + "17.\tСохне вона, як квіточка;\n"
      + "18.\tМати не питає:\n"
      + "19.\t\"Чого в'янеш, моя доню?\"\n"
      + "20.\tСтара не спитала,\n"
      + "21.\tЗа сивого, багатого\n"
      + "22.\tТихенько єднала.\n"
      + "23.\t\"Іди, доню, – каже мати, –\n"
      + "24.\tНе вік дівувати!\n"
      + "25.\tВін багатий, одинокий –\n"
      + "26.\tБудеш панувати\".\n"
      + "27.\t\"Не хочу я панувати,\n"
      + "28.\tНе піду я, мамо!\n"
      + "29.\tРушниками, що придбала,\n"
      + "30.\tСпусти мене в яму.\n"
      + "31.\tНехай попи заспівають,\n"
      + "32.\tА дружки заплачуть,\n"
      + "33.\tЛегше, мамо, в труні лежать,\n"
      + "34.\tНіж його побачить\"…\n"
      + "35.\tОтак тая чорнобрива\n"
      + "36.\tПлакала, співала...\n"
      + "37.\tІ на диво серед поля\n"
      + "38.\tТополею стала.\n"
      + "39.\tПо діброві вітер виє,\n"
      + "40.\tГуляє по полю,\n"
      + "41.\tКрай дороги гне тополя\n"
      + "42.\tДо самого долу…";
}
