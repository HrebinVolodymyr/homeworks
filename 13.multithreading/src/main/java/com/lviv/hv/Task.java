package com.lviv.hv;

import java.util.Random;
import java.util.concurrent.Callable;

public class Task implements Callable {

  @Override
  public String call() throws Exception {
    int timeSleep = randInt(1, 5);
    System.out.println(
        "THREAD START: " + Thread.currentThread().getName() + "- Time sleep = " + timeSleep
            + " seconds.");
    Thread.sleep(timeSleep * 1000);
    return "THREAD DONE: " + Thread.currentThread().getName();
  }

  private int randInt(int min, int max) {
    Random rand = new Random();
    int randomNum = rand.nextInt((max - min) + 1) + min;
    return randomNum;
  }
}
