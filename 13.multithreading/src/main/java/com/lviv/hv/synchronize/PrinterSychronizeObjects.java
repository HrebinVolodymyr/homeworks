package com.lviv.hv.synchronize;

public class PrinterSychronizeObjects {

  Object one = new Object();
  Object two = new Object();
  Object three = new Object();

  public void printOne() {
    synchronized (one) {
      System.out.println(Thread.currentThread().getName() + ": ONE");
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  public void printTwo() {
    synchronized (two) {
      System.out.println(Thread.currentThread().getName() + ": TWO");
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  public void printThree() {
    synchronized (three) {
      System.out.println(Thread.currentThread().getName() + ": THREE");
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

}
