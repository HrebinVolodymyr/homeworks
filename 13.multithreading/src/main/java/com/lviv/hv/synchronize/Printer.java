package com.lviv.hv.synchronize;

public class Printer {

  public void printOne() {
    synchronized (this) {
      System.out.println(Thread.currentThread().getName() + ": ONE");
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  public void printTwo() {
    synchronized (this) {
      System.out.println(Thread.currentThread().getName() + ": TWO");
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  public void printThree() {
    synchronized (this) {
      System.out.println(Thread.currentThread().getName() + ": THREE");
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

}
