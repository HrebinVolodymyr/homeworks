/*
 * Copyright (c) 2018, EPAM.
 */
package com.lviv.hv;

import java.util.Arrays;
import java.util.concurrent.Callable;
import java.util.stream.LongStream;

class FibonacciCalculatorCallable implements Callable<Long> {

  private int sizeSet;

  public FibonacciCalculatorCallable(int sizeSet) {
    this.sizeSet = sizeSet;
  }

  public long[] getFibonacciSet(final int sizeSet) {
    long firsNumber = 0;
    long secondNumber = 1;
    long thirdNumber;
    long[] fibonacciNumbers = new long[sizeSet];
    fibonacciNumbers[0] = firsNumber;
    fibonacciNumbers[1] = secondNumber;
    for (int i = 2; i < sizeSet; i++) {
      thirdNumber = firsNumber + secondNumber;
      fibonacciNumbers[i] = thirdNumber;
      firsNumber = secondNumber;
      secondNumber = thirdNumber;
    }
    return fibonacciNumbers;
  }

  public void run() {

  }

  @Override
  public Long call() throws Exception {
    Thread.sleep(1000);
    long[] numbers = getFibonacciSet(sizeSet);
    System.out.println(Thread.currentThread().getName() + ": " + Arrays.toString(numbers));
    return LongStream.of(numbers).sum();
  }
}
