package com.lviv.hv;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class Main {

  public static void main(String... args) {

    //     MyArrayList Test
    MyArrayList myArrayList = new MyArrayList();
    myArrayList.add("Hello ");
    myArrayList.add("World");
    myArrayList.add("!");
    myArrayList.add("!");
    myArrayList.add("!");
    System.out.println(myArrayList.getSize());

    String result = "";
    for (int i = 0; i < myArrayList.getSize(); i++) {
      result += myArrayList.get(i);
    }
    System.out.println(result);
    System.out.println("****************************************");

    // Comparable Test

    Generator generator = new Generator();
    ArrayList<StringContainer> stringContainersList = new ArrayList<StringContainer>();
    StringContainer[] stringContainersArray = new StringContainer[10];

    for (int i = 0; i < 10; i++) {
      stringContainersList.add(generator.getCoupleCountryCity());
      stringContainersArray[i] = generator.getCoupleCountryCity();
    }

    System.out.println("LIST: " + stringContainersList);
    System.out.println("ARRAY: " + Arrays.toString(stringContainersArray));

    System.out.println("****************************************");

    Collections.sort(stringContainersList);
    Arrays.sort(stringContainersArray);
    System.out.println("SORT BY COUNTRY(LIST): " + stringContainersList);
    System.out.println("SORT BY COUNTRY(ARRAY): " + Arrays.toString(stringContainersArray));

    System.out.println("****************************************");

    Collections.sort(stringContainersList, new StringContainerComparator());
    Arrays.sort(stringContainersArray, new StringContainerComparator());
    System.out.println("SORT BY CAPITAL(LIST): " + stringContainersList);
    System.out.println("SORT BY CAPITAL(ARRAY): " + Arrays.toString(stringContainersArray));

    System.out.println("****************************************");

    int index = Arrays.binarySearch(stringContainersArray, generator.getCoupleCountryCity(),
        new StringContainerComparator());
    System.out.println(index);

  }

}
