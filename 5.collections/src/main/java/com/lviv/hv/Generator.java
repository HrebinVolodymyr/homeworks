package com.lviv.hv;

import java.util.Random;

public class Generator {

  private String[][] countrys = {
      {"Afghanistan", "Albania", "Canada", "Chile", "England", "Georgia", "Indonesia",
          "Netherlands", "Ukraine"},
      {"Kabul", "Tirana", "Ottawa", "Santiago", "London", "Tbilisi", "Indonesia", "Amsterdam",
          "Kiev"}};

  public static int randInt(int min, int max) {
    Random rand = new Random();

    int randomNum = rand.nextInt((max - min) + 1) + min;

    return randomNum;
  }

  public StringContainer getCoupleCountryCity() {
    int index = randInt(0, countrys[0].length - 1);
    return new StringContainer(countrys[0][index], countrys[1][index]);
  }
}
