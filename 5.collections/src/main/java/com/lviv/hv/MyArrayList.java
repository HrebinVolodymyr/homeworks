package com.lviv.hv;

import java.util.ArrayList;

public class MyArrayList {

  private String[] array = new String[1];
  private int nextIndexPosition = 0;

  public void add(String string) {
    if (nextIndexPosition == array.length) {
      String[] tmp = new String[array.length * 2];
      System.arraycopy(array, 0, tmp, 0, array.length);
      array = tmp;
      array[nextIndexPosition] = string;
      nextIndexPosition++;

      System.out.println("New resize. Capacity: "+array.length);
    } else {
      array[nextIndexPosition] = string;
      nextIndexPosition++;
    }
  }

  public String get(int index) {
    return array[index];
  }

  public int getSize() {
    return nextIndexPosition;
  }
}
