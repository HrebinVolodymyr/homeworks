package com.lviv.hv;

import java.util.Comparator;

public class StringContainerComparator implements Comparator<StringContainer> {

  public int compare(StringContainer first, StringContainer second) {

    return first.getSecondString().compareTo(second.getSecondString());
  }
}
