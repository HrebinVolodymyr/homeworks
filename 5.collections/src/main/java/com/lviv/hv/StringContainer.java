package com.lviv.hv;

public class StringContainer implements Comparable {

  String firstString;
  String secondString;

  public StringContainer(String firstString, String secondString) {
    this.firstString = firstString;
    this.secondString = secondString;
  }

  public int compareTo(Object o) {
    String str = ((StringContainer)o).getFirstString();
    return firstString.compareTo(str);
  }

  @Override
  public String toString() {
    return "{" + firstString + "=" + secondString + "}";
  }

  public String getFirstString() {
    return firstString;
  }

  public String getSecondString() {
    return secondString;
  }
}
