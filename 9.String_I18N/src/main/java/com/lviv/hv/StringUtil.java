package com.lviv.hv;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StringUtil {

    List<Object> parameters = new ArrayList<>();

    public StringUtil() {
    }

    public StringUtil addParameter(Object parameter) {
        parameters.add(parameter);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        parameters.stream().forEach(o -> stringBuilder.append(o.toString()));
        parameters.clear();
        return stringBuilder.toString();
    }
}
