package com.lviv.hv;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String... agrs) {
        StringUtil stringUtil = new StringUtil();
        stringUtil.addParameter("Hello ")
                .addParameter("World ")
                .addParameter(20)
                .addParameter(18);
        System.out.println(stringUtil.toString());

        System.out.println(isCorrectSentence(readLine()));
        System.out.println(Arrays.toString(splitText(readLine())));
        System.out.println(replaceVowels(readLine()));
    }

    private static boolean isCorrectSentence(String text) {
        Pattern p = Pattern.compile("[A-Z][^\\.]*\\.");
        Matcher m = p.matcher(text);
        int count = 0;
        int end = 0;
        while (m.find()) {
            count++;
            end = m.end();
        }
        return count == 1 && text.length() == end;
    }

    private static String replaceVowels(String text) {
        return text.replaceAll("[auoeiAUOEI]", "_");
    }

    private static String[] splitText(String text) {
        return text.split("the |you ");
    }

    private static String readLine() {
        String line = null;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            line = br.readLine();
        } catch (IOException e) {
            System.out.println("IOException");
        }
        return line;
    }

}
