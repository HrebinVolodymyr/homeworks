package com.lviv.hv;

public class PassengerAirplane extends Airplane {

  private int passengerSeats;
  private int cockpitCrew;

  public PassengerAirplane(String name, String tailNumber, int numberOfCabinCrew, int cruiseSpeed,
      int cargoCapacity, int operatingEmptyWeight, int maximumTakeoffWeight,
      int flightRangeWithMaximumWeight, double fuelCapacity, int passengerSeats,
      int cockpitCrew) {
    super(name, tailNumber, numberOfCabinCrew, cruiseSpeed, cargoCapacity, operatingEmptyWeight,
        maximumTakeoffWeight, flightRangeWithMaximumWeight, fuelCapacity);
    this.passengerSeats = passengerSeats;
    this.cockpitCrew = cockpitCrew;
  }

  public int getMaxNumberPeopleOnBoard() {
    return getNumberOfCabinCrew() + this.cockpitCrew + passengerSeats;
  }

  public int getPassengerSeats() {
    return passengerSeats;
  }

  public int getCockpitCrew() {
    return cockpitCrew;
  }

  @Override
  public String toString() {
    return "PassengerAirplane{" + super.toString() +
        " Seats=" + passengerSeats +
        ", Cockpit Crew=" + cockpitCrew +
        "} "+"\n\n";
  }
}
