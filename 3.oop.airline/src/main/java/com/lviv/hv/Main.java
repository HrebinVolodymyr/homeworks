package com.lviv.hv;

import static com.lviv.hv.Printer.printEror;
import static com.lviv.hv.Printer.printHead;
import static com.lviv.hv.Printer.printMenu;
import static com.lviv.hv.Printer.println;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class Main {

  public static void main(String... args) {

    Main main = new Main();
    main.go();
  }

  void go() {
    Airline airline = null;
    airline = new Airline("Air France", "c.Lviv, str. Balabana 3");
    airline.addAirplane(
        new PassengerAirplane("Boing 747-100", "UA-JTR", 2,
            905, 110, 152900, 320000,
            10800, 17000,
            400, 5));
    airline.addAirplane(
        new PassengerAirplane("Airbus A340-200", "UA-KWY", 2,
            870, 163, 126000, 275000,
            12400, 21500,
            420, 5));
    airline.addAirplane(
        new СargoAirplane("Antonov 225:Mriya", "UA-KTN", 6,
            800, 600, 250000, 640000,
            4500, 40000,
            6));

    AirlineManager airlineManager = new AirlineManager(airline);
    printHead();
    while (true) {
      String data;
      while (true) {
        printMenu();
        data = readLine();
        if (data.equals("q")) {
          System.exit(0);
        }
        try {
          switch (Integer.parseInt(data)) {
            case 1:
              println("Total cargo capacity: "+airlineManager.getTotalCargoCapacity()+" m3");
              break;
            case 2:
              println("Total carrying capacity: "+airlineManager.getTotalCarryingCapacity()+" kg");
              break;
            case 3:
              List<Airplane> airplanes = airline.getAirplanes();
              airlineManager.sortByFlightRange(airplanes);
              println(airplanes);
              break;
            case 4:
              try {
                String str = readLine();
                String[] intervalProperties = str.split(";");
                double startInterval = Double.parseDouble(intervalProperties[0].trim());
                double endInterval = Double.parseDouble(intervalProperties[1].trim());
                if (startInterval >= endInterval) {
                  throw new Exception();
                }
                println(airlineManager
                    .findByFuelConsumptionRange(airline.getAirplanes(), startInterval, endInterval)
                    .toString());
              } catch (Exception e) {
                throw new IllegalArgumentException();
              }
              break;
            default:
              throw new IllegalArgumentException();

          }
        } catch (IllegalArgumentException e) {
          printEror("Invalid command. Try egan.");
          continue;
        }
        break;
      }
    }
  }

  private String readLine() {
    String line = null;
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    try {
      line = br.readLine();
    } catch (IOException e) {
      System.out.println("IOException");
    }
    return line;
  }

  void init(Airline airline) {

  }
}
