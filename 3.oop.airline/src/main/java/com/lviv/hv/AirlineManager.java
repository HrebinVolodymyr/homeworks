package com.lviv.hv;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AirlineManager {

  private Airline airline;

  AirlineManager(Airline airline) {
    this.airline = airline;
  }

  public int getTotalCarryingCapacity() {
    List<Airplane> airplanes = airline.getAirplanes();
    int totalCarryingCapacity = 0;
    for (Airplane airplain : airplanes) {
      totalCarryingCapacity += airplain.getCarryingCapacity();
    }
    return totalCarryingCapacity;
  }

  public int getTotalCargoCapacity() {
    List<Airplane> airplanes = airline.getAirplanes();
    int totalCargoCapacity = 0;
    for (Airplane airplane : airplanes) {
      totalCargoCapacity += airplane.getCargoCapacity();
    }
    return totalCargoCapacity;
  }

  public void sortByFlightRange(List<Airplane> airplanes) {
    Collections.sort(airplanes, new Comparator<Airplane>() {
      public int compare(Airplane airplane1, Airplane airplane2) {
        int first = airplane1.getFlightRangeWithMaximumWeight();
        int second = airplane2.getFlightRangeWithMaximumWeight();
        if (first == second) {
          return 0;
        }
        return first < second ? -1 : 1;
      }
    });

  }

  public List<Airplane> findByFuelConsumptionRange(List<Airplane> airplanes, double from, double to) {
    List<Airplane> result = new ArrayList<Airplane>();
    for (Airplane airplane : airplanes) {
      if (airplane.getMaxFuelConsumption() >= from && airplane.getMaxFuelConsumption() <= to) {
        result.add(airplane);
      }
    }
    return result;
  }


}
