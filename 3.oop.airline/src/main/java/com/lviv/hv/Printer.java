/*
 * Copyright (c) 2018, EPAM.
 */
package com.lviv.hv;

import java.util.Arrays;
import java.util.List;

/**
 * Class {@code Printer} forms and prints text in the console.
 *
 * @author Hrebin Vladimir
 * @version ver.1.0
 */
class Printer {

  static void printHead() {
    String str = new String(new char[39]).replace('\0', '*');
    System.out.println();
    System.out.println(str + " A I R L I N E " + str);
  }

  static void printDelimiter() {
    System.out.println(new String(new char[48]).replace('\0', '-'));
  }

  static void println(long[] array) {
    System.out.println(Arrays.toString(array));
  }

  static void println(List number) {
    System.out.println(number);
  }

  static void println(long number) {
    System.out.println(number);
  }

  static void println(double number) {
    System.out.println(number);
  }

  static void println(String str) {
    System.out.println();
    System.out.println(str);
  }

  static void printMassage(String str) {
    System.out.println();
    System.out.print(str);
  }

  static void printEror(String str) {
    printDelimiter();
    System.out.println(String.format("%-20s", "ERROR: " + str));
  }

  static void printMenu() {
    printDelimiter();
    System.out.println("For get total carrying capacity:                   1 + ENTER ");
    System.out.println("For get total cargo capacity:                      2 + ENTER");
    System.out.println("Display all airplains sorting by flight range:     3 + ENTER");
    System.out.println("Find airplaines by fuel consumption range:         4 + ENTER  then enter the interval (for example: 1.5;4.5)");
    printDelimiter();
  }

}
