package com.lviv.hv;

public abstract class Airplane {

  private String name;
  private String tailNumber;
  private int numberOfCabinCrew;
  private int cruiseSpeed;
  private int cargoCapacity;
  private int operatingEmptyWeight;
  private int maxTakeoffWeight;
  private int flightRangeWithMaximumWeight;
  private double fuelCapacity;

  public Airplane(String name, String tailNumber, int numberOfCabinCrew, int cruiseSpeed,
      int cargoCapacity, int operatingEmptyWeight, int maxTakeoffWeight,
      int flightRangeWithMaximumWeight, double fuelCapacity) {
    this.name = name;
    this.tailNumber = tailNumber;
    this.numberOfCabinCrew = numberOfCabinCrew;
    this.cruiseSpeed = cruiseSpeed;
    this.cargoCapacity = cargoCapacity;
    this.operatingEmptyWeight = operatingEmptyWeight;
    this.maxTakeoffWeight = maxTakeoffWeight;
    this.flightRangeWithMaximumWeight = flightRangeWithMaximumWeight;
    this.fuelCapacity = fuelCapacity;
  }

  public abstract int getMaxNumberPeopleOnBoard();

  public double getMaxFuelConsumption() {
    return fuelCapacity / flightRangeWithMaximumWeight;
  }

  public int getCarryingCapacity() {
    return (int) (maxTakeoffWeight - (operatingEmptyWeight + fuelCapacity * 0.8));
  }

  public String getName() {
    return name;
  }

  public String getTailNumber() {
    return tailNumber;
  }

  public int getNumberOfCabinCrew() {
    return numberOfCabinCrew;
  }

  public int getCruiseSpeed() {
    return cruiseSpeed;
  }

  public double getFuelCapacity() {
    return fuelCapacity;
  }

  public int getMaxTakeoffWeight() {
    return maxTakeoffWeight;
  }

  public int getFlightRangeWithMaximumWeight() {
    return flightRangeWithMaximumWeight;
  }

  public int getCargoCapacity() {
    return cargoCapacity;
  }

  public int getOperatingEmptyWeight() {
    return operatingEmptyWeight;
  }

  @Override
  public String toString() {
    return  name + '\'' +
        ", TN='" + tailNumber + '\'' +
        ", CabinCrew=" + numberOfCabinCrew +
        ", Speed=" + cruiseSpeed +
        ", Cargo Capacity=" + cargoCapacity  +
        ", Weight=" + maxTakeoffWeight +
        ", Flight Range=" + flightRangeWithMaximumWeight +
        ", Fuel Capacity=" + fuelCapacity;
  }
}
