package com.lviv.hv;

public class СargoAirplane extends Airplane {

  private int serviceСrew;

  public СargoAirplane(String name, String tailNumber, int numberOfCabinCrew, int cruiseSpeed,
      int cargoCapacity, int operatingEmptyWeight, int maximumTakeoffWeight,
      int flightRangeWithMaximumWeight, double fuelCapacity, int serviceСrew) {
    super(name, tailNumber, numberOfCabinCrew, cruiseSpeed, cargoCapacity, operatingEmptyWeight,
        maximumTakeoffWeight, flightRangeWithMaximumWeight, fuelCapacity);
    this.serviceСrew = serviceСrew;
  }

  public int getMaxNumberPeopleOnBoard() {
    return getNumberOfCabinCrew() + this.serviceСrew;
  }

  public int getServiceСrew() {
    return serviceСrew;
  }

  @Override
  public String toString() {
    return "\n"+"СargoAirplane{" + super.toString() +
        " Service Сrew=" + serviceСrew +
        "} "+"\n\n";
  }
}
