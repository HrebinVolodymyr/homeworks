package com.lviv.hv;

import java.util.ArrayList;
import java.util.List;

public class Airline {

  private String name;
  private String address;
  private List<Airplane> airplanes = new ArrayList<Airplane>();

  public Airline(String name, String address) {
    this.name = name;
    this.address = address;
  }

  public Airline(String name, String address, List<Airplane> airplanes) {
    this.name = name;
    this.address = address;
    this.airplanes = airplanes;
  }

  public void addAirplane(Airplane airplane) {
    this.airplanes.add(airplane);
  }

  public String getName() {
    return name;
  }

  public String getAddress() {
    return address;
  }

  public List<Airplane> getAirplanes() {
    return airplanes;
  }

  @Override
  public String toString() {
    return "Airline{" +
        "name='" + name + '\'' +
        ", address='" + address + '\'' +
        '}';
  }
}
