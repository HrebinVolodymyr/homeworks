<%@page pageEncoding="UTF-8" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">

<fmt:setBundle basename="/message/message"/>

<html>
<head>
	<title>Title</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
<div align="center" >
	<a href="<c:url value="/"/>">HOME</a>
</div>
<div align="center">
	<div id="content" align="right" style="width: 240px;">
		<h2 align="center">Edit department</h2>
		<form action="update" method="post">

			<label>deptNo</label>
			<input type="text" name="deptNo" value="${entity.deptNo}">
			<br><br>

			<label>deptName</label>
			<input type="text" name="deptName" value="${entity.deptName}">
			<br><br>

			<label>location</label>
			<input type="text" name="location" value="${entity.location}">
			<br><br>

			<input type="submit" value="Submit">
		</form>
	</div>
</div>

<br>
<div align="center">
	${error}
</div>
</body>
</html>

