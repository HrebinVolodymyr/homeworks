<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div align="center">
    <a href="<c:url value="/"/>">HOME</a>
</div>
<div align="center">
    <table border="1" cellpadding="5">
        <caption><h2 align="center"><a href="<c:url value="/employee"/>">Table: Employee</a></h2></caption>
        <tr>
            <th>ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>deptNo</th>
            <th>Delete</th>
            <th>Update</th>
        </tr>
        <c:forEach var="employeeEntity" items="${table}">
            <tr>
                <td><c:out value="${employeeEntity.empNo}"/></td>
                <td><c:out value="${employeeEntity.empFirstName}"/></td>
                <td><c:out value="${employeeEntity.epmLastName}"/></td>
                <td><c:out value="${employeeEntity.department.deptNo}"/></td>
                <td><a href="<c:url value="/employee/delete/${employeeEntity.empNo}"/>">DELETE</a></td>
                <td><a href="<c:url value="/employee/edit/${employeeEntity.empNo}"/>">UPDATE</a></td>
            </tr>
        </c:forEach>
        <tr>
            <td colspan="6"><a href="<c:url value="/employee/add"/>">ADD</a></td>
        </tr>
        <tr>
            <td colspan="3">
                <form action="/employeeFindById">
                    <input type="text" class="search-text" name="id"
                           value="${searchString}" placeholder="Find Employee by ID"/>
                    <input type="submit" class="button" value="Find"/>
                </form>
            </td>
            <td colspan="3">
                <form action="/employeeFindByName">
                    <input type="text" class="search-text" name="name"
                           value="${searchString}" placeholder="Find Employee by Name"/>
                    <input type="submit" class="button" value="Find"/>
                </form>
            </td>
        </tr>
    </table>
</div>
<h4 align="center"><a href="<c:url value="/tables"/>">Select all table</a><br></h4>
<div align="center">
    ${error}
</div>
