package com.lviv.hv.service.impl;

import com.lviv.hv.model.EmployeeEntity;
import com.lviv.hv.service.EmployeeService;
import com.lviv.hv.service.ServiceException;
import java.sql.SQLException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class EmployeeServiceImpl implements EmployeeService {

  @PersistenceContext
  EntityManager em;

  @Override
  @Transactional
  public List<EmployeeEntity> findAll() throws ServiceException {
    return em.createQuery("SELECT e FROM EmployeeEntity e", EmployeeEntity.class)
        .getResultList();
  }

  @Override
  @Transactional
  public EmployeeEntity findById(Integer id) throws ServiceException {
    return em.find(EmployeeEntity.class, id);
  }

  @Override
  @Transactional
  public void create(EmployeeEntity entity) throws ServiceException {
    try {
      em.persist(entity);
    } catch (Exception e) {
      throw new ServiceException(e.getMessage());
    }
  }

  @Override
  @Transactional
  public void update(EmployeeEntity entity) throws ServiceException {
    em.merge(entity);
  }

  @Override
  @Transactional
  public void delete(Integer id) throws ServiceException {
    em.remove(em.getReference(EmployeeEntity.class, id));
  }

  @Override
  @Transactional
  public List<EmployeeEntity> findByName(String name) throws ServiceException {
    return em.createQuery("SELECT e FROM EmployeeEntity e WHERE e.empFirstName = :name", EmployeeEntity.class)
        .setParameter("name", name)
        .getResultList();
  }

}
