package com.lviv.hv.service;

import com.lviv.hv.model.EmployeeEntity;
import java.sql.SQLException;
import java.util.List;

public interface EmployeeService extends GeneralService<EmployeeEntity, Integer> {

  public List<EmployeeEntity> findByName(String name) throws ServiceException;

}
