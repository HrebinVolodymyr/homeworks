package com.lviv.hv.service;

import java.util.List;

public interface GeneralService<T, ID> {

  List<T> findAll() throws ServiceException;

  T findById(ID id) throws ServiceException;

  void create(T entity) throws ServiceException;

  void update(T entity) throws ServiceException;

  void delete(ID id) throws ServiceException;
}
