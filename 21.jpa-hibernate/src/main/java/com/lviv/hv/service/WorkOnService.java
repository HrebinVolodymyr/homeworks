package com.lviv.hv.service;

import com.lviv.hv.model.PK_WorksOn;
import com.lviv.hv.model.WorksOnEntity;

public interface WorkOnService extends GeneralService<WorksOnEntity, PK_WorksOn> {

}
