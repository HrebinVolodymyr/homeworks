package com.lviv.hv.service.impl;

import com.lviv.hv.model.ProjectEntity;
import com.lviv.hv.service.ProjectService;
import com.lviv.hv.service.ServiceException;
import java.sql.SQLException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProjectServiceImpl implements ProjectService {

  @PersistenceContext
  EntityManager em;

  @Override
  @Transactional
  public List<ProjectEntity> findAll() throws ServiceException {
    return em.createQuery("SELECT p FROM ProjectEntity p", ProjectEntity.class)
        .getResultList();
  }

  @Override
  @Transactional
  public ProjectEntity findById(String id) throws ServiceException {
    return em.find(ProjectEntity.class, id);
  }

  @Override
  @Transactional
  public void create(ProjectEntity entity) throws ServiceException {
    try {
      em.persist(entity);
    } catch (Exception e) {
      throw new ServiceException(e.getMessage());
    }
  }

  @Override
  @Transactional
  public void update(ProjectEntity entity) throws ServiceException {
    em.merge(entity);
  }

  @Override
  @Transactional
  public void delete(String id) throws ServiceException {
    em.remove(em.getReference(ProjectEntity.class, id));
  }
}

