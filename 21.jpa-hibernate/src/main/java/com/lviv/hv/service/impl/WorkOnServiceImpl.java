package com.lviv.hv.service.impl;

import com.lviv.hv.model.PK_WorksOn;
import com.lviv.hv.model.WorksOnEntity;
import com.lviv.hv.service.ServiceException;
import com.lviv.hv.service.WorkOnService;
import java.sql.SQLException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class WorkOnServiceImpl implements WorkOnService {

  @PersistenceContext
  EntityManager em;

  @Override
  @Transactional
  public List<WorksOnEntity> findAll() throws ServiceException {
    return em.createQuery("SELECT w FROM WorksOnEntity w", WorksOnEntity.class)
        .getResultList();
  }

  @Override
  @Transactional
  public WorksOnEntity findById(PK_WorksOn id) throws ServiceException {
    return em.find(WorksOnEntity.class, id);
  }

  @Override
  @Transactional
  public void create(WorksOnEntity entity) throws ServiceException {
    try {
      em.persist(entity);
    } catch (Exception e) {
      throw new ServiceException(e.getMessage());
    }
  }

  @Override
  @Transactional
  public void update(WorksOnEntity entity) throws ServiceException {
    PK_WorksOn id = new PK_WorksOn();
    id.setEmpNo(entity.getEmpNo());
    id.setProjectNo(entity.getProjectNo());
    em.merge(entity);
  }

  @Override
  @Transactional
  public void delete(PK_WorksOn id) throws ServiceException {
    em.remove(em.getReference(WorksOnEntity.class, id));
  }
}
