package com.lviv.hv.service.impl;

import com.lviv.hv.model.DepartmentEntity;
import com.lviv.hv.model.EmployeeEntity;
import com.lviv.hv.service.DepartmentService;
import com.lviv.hv.service.ServiceException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DepartmentServiceImpl implements DepartmentService {

  @PersistenceContext
  EntityManager em;

  @Override
  @Transactional
  public List<DepartmentEntity> findAll() throws ServiceException {
    return em.createQuery("SELECT d FROM DepartmentEntity d", DepartmentEntity.class)
        .getResultList();
  }

  @Override
  @Transactional
  public DepartmentEntity findById(String id) throws ServiceException {
    return em.find(DepartmentEntity.class, id);
  }

  @Override
  @Transactional
  public void create(DepartmentEntity entity) throws ServiceException {
    em.persist(entity);
  }

  @Override
  @Transactional
  public void update(DepartmentEntity entity) throws ServiceException {
    em.merge(entity);
  }

  @Override
  @Transactional
  public void delete(String id) throws ServiceException {
    em.remove(em.getReference(DepartmentEntity.class, id));
  }

  @Override
  @Transactional
  public void deleteWithMoveOfEmployees(String idDeleted, String idMoveTo) throws ServiceException {
    DepartmentEntity departmentDeleted = em.find(DepartmentEntity.class, idDeleted);
    DepartmentEntity departmentMoveTo = em.find(DepartmentEntity.class, idMoveTo);

    if (departmentDeleted == null && departmentMoveTo == null) {
      throw new ServiceException("Department not found");
    }

    List<EmployeeEntity> employees =
        em.createQuery("SELECT e FROM EmployeeEntity e WHERE e.department = :departmentDeleted",
            EmployeeEntity.class)
            .setParameter("departmentDeleted", departmentDeleted)
            .getResultList();

    for (EmployeeEntity entity : employees) {
      entity.setDepartment(departmentMoveTo);
      em.merge(entity);
    }

    em.remove(departmentDeleted);
  }
}
