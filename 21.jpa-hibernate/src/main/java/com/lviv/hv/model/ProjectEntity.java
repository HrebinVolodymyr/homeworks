package com.lviv.hv.model;

import java.util.Collection;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "project", schema = "sample")
public class ProjectEntity {

  private String projectNo;
  private String projectName;
  private Integer budget;
  private String description;
  private Set<WorksOnEntity> worksOn;

  public ProjectEntity() {
    super();
  }

  public ProjectEntity(String projectNo, String projectName, Integer budget) {
    this.projectNo = projectNo;
    this.projectName = projectName;
    this.budget = budget;
  }

  @Id
  @Column(name = "project_no", nullable = false, length = 10)
  public String getProjectNo() {
    return projectNo;
  }

  public void setProjectNo(String projectNo) {
    this.projectNo = projectNo;
  }

  @Basic
  @Column(name = "project_name", nullable = true, length = 45)
  public String getProjectName() {
    return projectName;
  }

  public void setProjectName(String projectName) {
    this.projectName = projectName;
  }

  @Basic
  @Column(name = "budget", nullable = true)
  public Integer getBudget() {
    return budget;
  }

  public void setBudget(Integer budget) {
    this.budget = budget;
  }

  @Basic
  @Column(name = "description", nullable = true, length = -1)
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ProjectEntity that = (ProjectEntity) o;
    return Objects.equals(projectNo, that.projectNo) &&
        Objects.equals(projectName, that.projectName) &&
        Objects.equals(budget, that.budget) &&
        Objects.equals(description, that.description);
  }

  @Override
  public int hashCode() {

    return Objects.hash(projectNo, projectName, budget, description);
  }

  @OneToMany(mappedBy = "project")
  public Set<WorksOnEntity> getWorksOn() {
    return worksOn;
  }

  public void setWorksOn(Set<WorksOnEntity> worksOn) {
    this.worksOn = worksOn;
  }
}
