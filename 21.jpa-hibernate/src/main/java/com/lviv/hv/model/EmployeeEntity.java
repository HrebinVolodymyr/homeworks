package com.lviv.hv.model;

import java.util.Objects;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "employee", schema = "sample")
public class EmployeeEntity {

  public EmployeeEntity() {
    super();
  }

  public EmployeeEntity(Integer empNo, String empFirstName, String epmLastName,
      DepartmentEntity department) {
    this.empNo = empNo;
    this.empFirstName = empFirstName;
    this.epmLastName = epmLastName;
    this.department = department;
  }
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "emp_no", nullable = false)
  private Integer empNo;

  @Basic
  @Column(name = "emp_fname", nullable = true, length = 45)
  private String empFirstName;

  @Basic
  @Column(name = "emp_lname", nullable = true, length = 45)
  private String epmLastName;

  @ManyToOne
  @JoinColumn(name = "dept_no")
  private DepartmentEntity department;

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "account_id", referencedColumnName = "id", nullable = false)
  private AccountEntity account;

  @OneToMany(mappedBy = "employee")
  private Set<WorksOnEntity> worksOn;


  public Integer getEmpNo() {
    return empNo;
  }

  public void setEmpNo(Integer empNo) {
    this.empNo = empNo;
  }


  public String getEmpFirstName() {
    return empFirstName;
  }

  public void setEmpFirstName(String empFname) {
    this.empFirstName = empFname;
  }


  public String getEpmLastName() {
    return epmLastName;
  }

  public void setEpmLastName(String empLname) {
    this.epmLastName = empLname;
  }


  public DepartmentEntity getDepartment() {
    return department;
  }

  public void setDepartment(DepartmentEntity department) {
    this.department = department;
  }


  public AccountEntity getAccount() {
    return account;
  }

  public void setAccount(AccountEntity account) {
    this.account = account;
  }


  public Set<WorksOnEntity> getWorksOn() {
    return worksOn;
  }

  public void setWorksOn(Set<WorksOnEntity> worksOn) {
    this.worksOn = worksOn;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EmployeeEntity that = (EmployeeEntity) o;
    return Objects.equals(empNo, that.empNo) &&
        Objects.equals(empFirstName, that.empFirstName) &&
        Objects.equals(epmLastName, that.epmLastName);
  }

  @Override
  public int hashCode() {

    return Objects.hash(empNo, empFirstName, epmLastName);
  }
}
