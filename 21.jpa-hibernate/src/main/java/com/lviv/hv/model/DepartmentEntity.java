package com.lviv.hv.model;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "department", schema = "sample")
public class DepartmentEntity {

  private String deptNo;
  private String deptName;
  private String location;
  private Set<EmployeeEntity> employees = new LinkedHashSet<>();

  public DepartmentEntity() {
    super();
  }

  public DepartmentEntity(String deptNo, String deptName, String location) {
    this.deptNo = deptNo;
    this.deptName = deptName;
    this.location = location;
  }

  @Id
  @Column(name = "dept_no", nullable = false, length = 5)
  public String getDeptNo() {
    return deptNo;
  }

  public void setDeptNo(String deptNo) {
    this.deptNo = deptNo;
  }

  @Basic
  @Column(name = "dept_name", nullable = false, length = 15)
  public String getDeptName() {
    return deptName;
  }

  public void setDeptName(String deptName) {
    this.deptName = deptName;
  }

  @Basic
  @Column(name = "location", nullable = true, length = 45)
  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DepartmentEntity that = (DepartmentEntity) o;
    return Objects.equals(deptNo, that.deptNo) &&
        Objects.equals(deptName, that.deptName) &&
        Objects.equals(location, that.location);
  }

  @Override
  public int hashCode() {
    return Objects.hash(deptNo, deptName, location);
  }

  @OneToMany(mappedBy = "department")
  public Set<EmployeeEntity> getEmployees() {
   return employees;
  }

  public void setEmployees(Set<EmployeeEntity> employees) {
    this.employees = employees;
  }
}
