package com.lviv.hv.model;

import java.util.Objects;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "sql_grant", schema = "sample")
public class SqlGrantEntity {

  private Integer id;
  private String title;
  private Set<AccountEntity> accounts;

  @Id
  @Column(name = "id", nullable = false)
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Basic
  @Column(name = "title", nullable = false)
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SqlGrantEntity that = (SqlGrantEntity) o;
    return Objects.equals(id, that.id) &&
        Objects.equals(title, that.title);
  }

  @Override
  public int hashCode() {

    return Objects.hash(id, title);
  }

  @ManyToMany(mappedBy = "sqlGrants")
  public Set<AccountEntity> getAccounts() {
    return accounts;
  }

  public void setAccounts(Set<AccountEntity> accounts) {
    this.accounts = accounts;
  }
}
