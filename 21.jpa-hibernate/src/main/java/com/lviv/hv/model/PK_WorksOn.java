package com.lviv.hv.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;
import javax.persistence.Table;

//@Table(name = "works_on", schema = "sample")
public class PK_WorksOn implements Serializable {

  private Integer empNo;
  private String projectNo;

  public PK_WorksOn() {
    super();
  }

  public PK_WorksOn(Integer empNo, String projectNo) {
    this.empNo = empNo;
    this.projectNo = projectNo;
  }

  //@Column(name = "emp_no", nullable = false)
  public Integer getEmpNo() {
    return empNo;
  }

  public void setEmpNo(Integer empNo) {
    this.empNo = empNo;
  }

  //@Column(name = "project_no", nullable = false, length = 10)
  public String getProjectNo() {
    return projectNo;
  }

  public void setProjectNo(String projectNo) {
    this.projectNo = projectNo;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PK_WorksOn that = (PK_WorksOn) o;
    return Objects.equals(empNo, that.empNo) &&
        Objects.equals(projectNo, that.projectNo);
  }

  @Override
  public int hashCode() {
    return Objects.hash(empNo, projectNo);
  }
}
