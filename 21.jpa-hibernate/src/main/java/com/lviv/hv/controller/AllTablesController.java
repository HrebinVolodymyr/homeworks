package com.lviv.hv.controller;

import com.lviv.hv.model.DepartmentEntity;
import com.lviv.hv.model.EmployeeEntity;
import com.lviv.hv.model.ProjectEntity;
import com.lviv.hv.model.WorksOnEntity;
import com.lviv.hv.service.DepartmentService;
import com.lviv.hv.service.EmployeeService;
import com.lviv.hv.service.ProjectService;
import com.lviv.hv.service.ServiceException;
import com.lviv.hv.service.WorkOnService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class AllTablesController {

  @Autowired
  DepartmentService departmentService;
  @Autowired
  EmployeeService employeeService;
  @Autowired
  ProjectService projectService;
  @Autowired
  WorkOnService workOnService;

  @RequestMapping(value = "/tables", method = RequestMethod.GET)
  public String showAllTables(ModelMap model) {
    Map<String, List> tables = new HashMap<>();
    try {

      List<DepartmentEntity> departments = departmentService.findAll();
      List<EmployeeEntity> employees = employeeService.findAll();
      List<ProjectEntity> projects = projectService.findAll();
      List<WorksOnEntity> works = workOnService.findAll();

      tables.put("departments", departments);
      tables.put("employees", employees);
      tables.put("projects", projects);
      tables.put("works", works);
    } catch (Exception e) {
      e.printStackTrace();
    }
    model.addAttribute("tables", tables);
    return "tables";
  }
}