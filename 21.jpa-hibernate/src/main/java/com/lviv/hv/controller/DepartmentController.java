package com.lviv.hv.controller;

import com.lviv.hv.model.DepartmentEntity;
import com.lviv.hv.service.DepartmentService;
import com.lviv.hv.service.ServiceException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class DepartmentController {

  @Autowired
  DepartmentService departmentService;

  @RequestMapping(value = "/department", method = RequestMethod.GET)
  public String showAllDepartment(ModelMap model) {
    try {
      model.addAttribute("table", departmentService.findAll());
    } catch (Exception e) {
      e.printStackTrace();
    }
    return "departments";
  }

  @RequestMapping(value = "/departmentFindById", method = RequestMethod.GET)
  public String findById(
      @RequestParam("id") String id,
      ModelMap model) {
    try {
      if (id != null) {
        List<DepartmentEntity> list = new ArrayList<DepartmentEntity>();
        list.add(departmentService.findById(id));
        model.addAttribute("table", list);
        return "departments";
      }
      model.addAttribute("table", departmentService.findAll());
    } catch (Exception e) {
      e.printStackTrace();
    }
    return "departments";
  }

  @RequestMapping(value = "/deleteDepartmentMoveEmployees", method = RequestMethod.GET)
  public String deleteDepartmentMoveEmployees(
      @RequestParam("idDeleted") String idDeleted,
      @RequestParam("idMoveTo") String idMoveTo,
      ModelMap model) {
    try {
      departmentService.deleteWithMoveOfEmployees(idDeleted, idMoveTo);
      model.addAttribute("table", departmentService.findAll());
    } catch (Exception e) {
      model.addAttribute("table", departmentService.findAll());
      model.addAttribute("error", "ERROR: " + e.getMessage());
      e.printStackTrace();
    }
    return "departments";
  }

  @RequestMapping(value = "/department/delete/{id}", method = RequestMethod.GET)
  public String deleteDepartment(@PathVariable("id") String deptNo, ModelMap model) {
    try {
      departmentService.delete(deptNo);
      model.addAttribute("table", departmentService.findAll());
    } catch (Exception e) {
      try {
        model.addAttribute("table", departmentService.findAll());
        model.addAttribute("error", "ERROR: " + ExceptionUtils.getRootCause(e).getMessage());
      } catch (Exception s) {
        s.printStackTrace();
      }
    }
    return "departments";
  }

  @RequestMapping(value = "/department/add", method = RequestMethod.GET)
  public String add(ModelMap model) {
    return "addDepartment";
  }

  @RequestMapping(value = "/department/add", method = RequestMethod.POST)
  public String addDepartment(
      @RequestParam("deptNo") String deptNo,
      @RequestParam("deptName") String deptName,
      @RequestParam("location") String location,
      ModelMap model) {
    DepartmentEntity entity = new DepartmentEntity(deptNo, deptName, location);
    try {
      departmentService.create(entity);
    } catch (Exception e) {
      model.addAttribute("error", "ERROR: " + ExceptionUtils.getRootCause(e).getMessage());
      return "addDepartment";
    }
    return "redirect:/department";
  }

  @RequestMapping(value = "/department/edit/{id}", method = RequestMethod.GET)
  public String edit(@PathVariable("id") String deptNo, ModelMap model) {
    try {
      model.addAttribute("entity", departmentService.findById(deptNo));
    } catch (Exception e) {
      model.addAttribute("error", "ERROR: " + ExceptionUtils.getRootCause(e).getMessage());
      return "editDepartment";
    }
    return "editDepartment";
  }

  @RequestMapping(value = "/department/edit/update", method = RequestMethod.POST)
  public String editDepartment(
      @RequestParam("deptNo") String deptNo,
      @RequestParam("deptName") String deptName,
      @RequestParam("location") String location,
      ModelMap model) {
    DepartmentEntity entity = new DepartmentEntity(deptNo, deptName, location);
    try {
      departmentService.update(entity);
    } catch (Exception e) {
      model.addAttribute("error", "ERROR: " + ExceptionUtils.getRootCause(e).getMessage());
      return "redirect:/department";
    }
    return "redirect:/department";
  }
}
