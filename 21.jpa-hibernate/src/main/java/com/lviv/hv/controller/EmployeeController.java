package com.lviv.hv.controller;

import com.lviv.hv.model.DepartmentEntity;
import com.lviv.hv.model.EmployeeEntity;
import com.lviv.hv.service.DepartmentService;
import com.lviv.hv.service.EmployeeService;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class EmployeeController {

  @Autowired
  EmployeeService  employeeService;
  @Autowired
  DepartmentService departmentService;

  @RequestMapping(value = "/employee", method = RequestMethod.GET)
  public String showAllEmployee(ModelMap model) {
    try {
      model.addAttribute("table", employeeService.findAll());
    } catch (Exception e) {
      e.printStackTrace();
    }
    return "employees";
  }

  @RequestMapping(value = "/employeeFindById", method = RequestMethod.GET)
  public String findById(
      @RequestParam("id") Integer id,
      ModelMap model) {
    try {
      if (id != null) {
        List<EmployeeEntity> list = new ArrayList<EmployeeEntity>();
        list.add(employeeService.findById(id));
        model.addAttribute("table", list);
        return "employees";
      }
      model.addAttribute("table", employeeService.findAll());
    } catch (Exception e) {
      e.printStackTrace();
    }
    return "employees";
  }

  @RequestMapping(value = "/employeeFindByName", method = RequestMethod.GET)
  public String findByName(
      @RequestParam("name") String name,
      ModelMap model) {
    try {
      if (name != null) {
        List<EmployeeEntity> list = new ArrayList<EmployeeEntity>();
        model.addAttribute("table", employeeService.findByName(name));
        return "employees";
      }
      model.addAttribute("table", employeeService.findAll());
    } catch (Exception e) {
      e.printStackTrace();
    }
    return "employees";
  }

  @RequestMapping(value = "/employee/delete/{id}", method = RequestMethod.GET)
  public String deleteEmployee(@PathVariable("id") Integer employeeId, ModelMap model) {
    try {
      employeeService.delete(employeeId);
      model.addAttribute("table", employeeService.findAll());
    } catch (Exception e) {
      try {
        model.addAttribute("table", employeeService.findAll());
        model.addAttribute("error", "ERROR: " + ExceptionUtils.getRootCause(e).getMessage());
      } catch (Exception s) {
        s.printStackTrace();
      }
    }
    return "employees";
  }

  @RequestMapping(value = "/employee/add", method = RequestMethod.GET)
  public String add(ModelMap model) {
    return "addEmployee";
  }

  @RequestMapping(value = "/employee/add", method = RequestMethod.POST)
  public String addDepartment(
      @RequestParam("id") Integer id,
      @RequestParam("empFirstName") String empFirstName,
      @RequestParam("epmLastName") String epmLastName,
      @RequestParam("deptNo") String deptNo,
      ModelMap model) {
    try {
      DepartmentEntity department = departmentService.findById(deptNo);
      EmployeeEntity entity = new EmployeeEntity(id, empFirstName, epmLastName, department);
      employeeService.create(entity);
    } catch (Exception e) {
      model.addAttribute("error", "ERROR: " + ExceptionUtils.getRootCause(e).getMessage());
    }
    return "redirect:/employee";
  }

  @RequestMapping(value = "/employee/edit/{id}", method = RequestMethod.GET)
  public String edit(@PathVariable("id") Integer employeeId, ModelMap model) {
    try {
      model.addAttribute("entity", employeeService.findById(employeeId));
    } catch (Exception e) {
      model.addAttribute("error", "ERROR: " + ExceptionUtils.getRootCause(e).getMessage());
      return "editEmployee";
    }
    return "editEmployee";
  }

  @RequestMapping(value = "/employee/edit/update", method = RequestMethod.POST)
  public String updateDepartment(
      @RequestParam("id") Integer id,
      @RequestParam("empFirstName") String empFirstName,
      @RequestParam("epmLastName") String epmLastName,
      @RequestParam("deptNo") String deptNo,
      ModelMap model) {
    try {
      EmployeeEntity e = employeeService.findById(id);
      e.setEmpFirstName(empFirstName);
      e.setEpmLastName(epmLastName);
      e.setDepartment(departmentService.findById(deptNo));
      employeeService.update(e);
    } catch (Exception e) {
      model.addAttribute("error", "ERROR: " + ExceptionUtils.getRootCause(e).getMessage());
      return "redirect:/employee";
    }
    return "redirect:/employee";
  }

}
