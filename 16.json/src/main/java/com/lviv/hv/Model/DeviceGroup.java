package com.lviv.hv.Model;

public class DeviceGroup {

  private int id;
  private String name;

  public DeviceGroup() {
  }

  public DeviceGroup(int id, String name) {
    this.id = id;
    this.name = name;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return "DeviceGroup{" +
        "id=" + id +
        ", name='" + name + '\'' +
        '}';
  }
}
