package com.lviv.hv;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lviv.hv.Model.Device;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class JsonParser {

  public List parseJSON(File jsonFile) {
    ObjectMapper objectMapper = new ObjectMapper();
    Device[] devices = new Device[0];
    try {
      devices = objectMapper.readValue(jsonFile, Device[].class);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return Arrays.asList(devices);
  }

}
