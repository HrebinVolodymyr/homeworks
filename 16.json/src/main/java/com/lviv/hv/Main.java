package com.lviv.hv;


import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.lviv.hv.Model.Device;
import com.lviv.hv.Model.DeviceGroup;
import com.lviv.hv.Model.Port;
import com.lviv.hv.Model.Type;
import com.lviv.hv.comparator.DeviceComparator;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;

public class Main {

  public static void main(String... args) throws IOException, ProcessingException {
    //Convert object to json and write into file.
    writeJsonIntoFile();

    File schemaFile = new File("src/main/resources/devicesScheme.json");
    File jsonFile = new File("src/main/resources/devices.json");

    JsonNode schemaNode = JsonLoader.fromFile(schemaFile);
    JsonNode jsonNode = JsonLoader.fromFile(jsonFile);

    JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
    JsonSchema schema = factory.getJsonSchema(schemaNode);

    ProcessingReport report;
    report = schema.validate(jsonNode);
    System.out.println("--- BEGIN MESSAGES ---");
    System.out.println(report);

    if (report.isSuccess()) {
      JsonParser parser = new JsonParser();
      List devices = parser.parseJSON(jsonFile);
      Collections.sort(devices, new DeviceComparator());
      printList(devices);
    }
  }

  public static void writeJsonIntoFile() {
    DeviceGroup monitor = new DeviceGroup(1, "Monitor");
    Port vga = new Port("VGA");
    Port hdmi = new Port("HDMI");

    Type type = new Type();
    type.setDeviceGroup(monitor);
    type.setCooler(false);
    type.setPeripheral(true);
    type.setEnergyConsumption(25);
    type.addPort(vga);
    type.addPort(hdmi);

    Device device = new Device();
    device.setDeviceId(1);
    device.setName("Samsung S24F350F");
    device.setOrigin("China");
    device.setPrice(4099);
    device.setType(type);
    device.setCritical(true);

    JsonObject obj = new JsonObject();
    Gson gson = new Gson();
    String json = gson.toJson(device);
    System.out.println(json);
    try (PrintWriter writer = new PrintWriter("src/main/resources/newJson.json")) {
      writer.print(json);
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
  }

  private static void printList(List<Object> list) {
    System.out.println("JSON");
    for (Object object : list) {
      System.out.println(object);
    }
  }
}
