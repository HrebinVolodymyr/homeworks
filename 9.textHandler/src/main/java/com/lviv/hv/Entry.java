package com.lviv.hv;

public class Entry {
    int count;
    String text;

    public Entry(int count, String text) {
        this.count = count;
        this.text = text;
    }

    public int getCount() {
        return count;
    }

    public String getText() {
        return text;
    }
}
