package com.lviv.hv;

import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ComparatorConsonant implements Comparator {

  @Override
  public int compare(Object o1, Object o2) {
    return getFirstConsonant((String) o1).compareTo(getFirstConsonant((String) o2));
  }

  private Character getFirstConsonant(String string) {
    Pattern p = Pattern.compile("[^auoeiAUOEI].*");
    Matcher m = p.matcher(string);
    while (m.find()) {
      return m.group().charAt(0);

    }
    return 'a';
  }

}
