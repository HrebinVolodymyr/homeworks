package com.lviv.hv;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Book {
    private List<Paragraph> paragraphs = new ArrayList<>();

    public Book(String text) {
        parseText(text);
    }

    public Book parseText(String text) {
        String[] textParagraphs = text.split("\n");
        Arrays.stream(textParagraphs).forEach(string -> paragraphs.add(new Paragraph(string)));
        return this;
    }

    public List<Paragraph> getParagraphs() {
        return paragraphs;
    }

    public String toString() {
        String text = "";
        for (Paragraph paragraph : paragraphs) {
            text = text + paragraph.toString() + "\n";
        }
        return text;
    }
}
