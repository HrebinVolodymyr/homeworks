package com.lviv.hv;

public enum SentenceType {
    STORYTELLING,
     QUESTIONING,
     INDUCTIVE,
    INDEFINITE
}
