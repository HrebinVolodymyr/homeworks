package com.lviv.hv;

import java.util.*;
import java.util.stream.Collectors;

public class Main {

    public static void main(String... args) {
        String s = "Anyone who reads Old and Middle English literary texts will be familiar with the "
                + "mid-brown volumes of the EETS, with the symbol of Alfred's jewel embossed on the front "
                + "cover. Most of the works attributed to King Alfred or to Aelfric, along with some of "
                + "those by bishop Wulfstan and much anonymous prose and verse from the pre-Conquest "
                + "period, are to be found within the Society's three series; all of the surviving "
                + "medieval drama, most of the Middle English romances, much religious and secular prose "
                + "and verse including the English works of John Gower, Thomas Hoccleve and most of "
                + "Caxton's prints all find their place in the publications. Without EETS editions, "
                + "study of medieval English texts would hardly be possible.\n"
                + "As its name states, EETS was begun as a 'club', and it retains certain features of "
                + "that even now. It has no physical location, or even office, no paid staff or editors, "
                + "but books in the Original Series are published in the first place to satisfy "
                + "subscriptions paid by individuals or institutions. This means that there is need "
                + "for a regular sequence of new editions, normally one or two per year; achieving that "
                + "sequence can pose problems for the Editorial Secretary, who may have too few or too "
                + "many texts ready for publication at any one time. Details on a separate sheet explain "
                + "how individual (but not institutional) members can choose to take certain back volumes "
                + "in place of the newly published volumes against their subscriptions. On the same sheet "
                + "are given details about the very advantageous discount available to individual members "
                + "on all back numbers. In 1970 a Supplementary Series was begun, a series which only "
                + "appears occasionally (it currently has 24 volumes within it); some of these are new "
                + "editions of texts earlier appearing in the main series. Again these volumes are "
                + "available at publication and later at a substantial discount to members. All these "
                + "advantages can only be obtained through the Membership Secretary (the books are sent "
                + "by post); they are not available through bookshops, and such bookstores as carry EETS "
                + "books have only a very limited selection of the many published.\n"
                + "Editors, who receive no royalties or expenses and who are only very rarely commissioned "
                + "by the Society, are encouraged to approach the Editorial Secretary with a detailed "
                + "proposal of the text they wish to suggest to the Society early in their work; interest "
                + "may be expressed at that point, but before any text is accepted for publication the "
                + "final typescript must be approved by the Council (a body of some twenty scholars), "
                + "and then assigned a place in the printing schedule. The Society now has a stylesheet "
                + "to guide editors in the layout and conventions acceptable within its series. No "
                + "prescriptive set of editorial principles is laid down, but it is usually expected that "
                + "the evidence of all relevant medieval copies of the text(s) in question will have been "
                + "considered, and that the texts edited will be complete whatever their length. Editions "
                + "are directed at a scholarly readership rather than a popular one; though they normally "
                + "provide a glossary and notes, no translation is provided.\n"
                + "EETS was founded in 1864 by Frederick James Furnivall, with the help of Richard Morris, "
                + "Walter Skeat, and others, to bring the mass of unprinted Early English literature "
                + "within the reach of students. It was also intended to provide accurate texts from "
                + "which the New (later Oxford) English Dictionary could quote; the ongoing work on the "
                + "revision of that Dictionary is still heavily dependent on the Society's editions, as "
                + "are the Middle English Dictionary and the Toronto Dictionary of Old English. In 1867 "
                + "an Extra Series was started, intended to contain texts already printed but not in "
                + "satisfactory or readily obtainable editions; this series was discontinued in 1921, "
                + "and from then on all the Society's editions, apart from the handful in the "
                + "Supplementary Series described above, were listed and numbered as part of the "
                + "Original Series? In all the Society has now published some 475 volumes; all except "
                + "for a very small number (mostly of editions superseded within the series) are "
                + "available in print. The early history of the Society is only traceable in outline: "
                + "no details about nineteenth-century membership are available, and the secretarial "
                + "records of the early twentieth century were largely lost during the second world war. "
                + "By the 1950s a very large number of the Society's editions were out of print, and "
                + "finances allowed for only a very limited reprinting programme. Around 1970 an "
                + "advantageous arrangement was agreed with an American reprint firm to make almost all "
                + "the volumes available once more whilst maintaining the membership discounts. Though "
                + "this arrangement was superseded towards the end of the twentieth century and the cost "
                + "of reprinting has reverted to the Society, as a result of the effort then it has proved "
                + "possible to keep the bulk of the list in print?\n"
                + "Many comparable societies, with different areas of interest, were founded in the "
                + "nineteenth century (several of them also by Furnivall); not all have survived, and few "
                + "have produced as many valuable volumes as EETS. The Society's success continues to "
                + "depend very heavily on the loyalty of members, and especially on the energy and "
                + "devotion of a series of scholars who have been involved with the administration of "
                + "the Society - the amount of time and effort spent by those who over the years have "
                + "filled the role of Editorial Secretary is immeasurable. Plans for publications for the "
                + "coming years are well in hand: there are a number of important texts which should be "
                + "published within the next five years. At present, notably because of the efforts of a "
                + "series of Executive and Membership Secretaries, the Society's finances are in "
                + "reasonable shape; but certain trends give concern to the Council. The Society's "
                + "continuance is dependent on two factors: the first is obviously the supply of "
                + "scholarly editions suitable to be included in its series; the second is on the "
                + "maintenance of subscriptions and sales of volumes at a level which will cover the "
                + "printing and distribution costs of the new and reprinted books. The normal copyright "
                + "laws cover the Society's volumes. All enquiries about large scale reproduction, whether "
                + "by photocopying or on the internet, should be directed to the Executive Secretary in "
                + "the first instance. The Society's continued usefulness depends on its editors and on "
                + "its ability to maintain its (re)printing programme - and that depends on those who "
                + "traditionally have become members of the Society. We hope you will maintain your "
                + "membership, and will encourage both the libraries you use and also other individuals "
                + "to join. Membership conveys many benefits for you, and for the wider academic community "
                + "concerned for the understanding of medieval text?";

        Book book = new Book(s);

        //1.    Знайти найбільшу кількість речень тексту, в яких є однакові слова.
//        method1(book);

        //2.	Вивести всі речення заданого тексту у порядку зростання кількості слів у кожному з них.
        method2(book);

        //3.	Знайти таке слово у першому реченні, якого немає ні в одному з інших речень.
//    method3(book);

        //4.	У всіх запитальних реченнях тексту знайти і надрукувати без повторів слова заданої довжини.
//    method4(book, 3);

        //5.	У кожному реченні тексту поміняти місцями перше слово, що починається на голосну букву
        //    з найдовшим словом.
//    method5(book);

        //6.	Надрукувати слова тексту в алфавітному порядку по першій букві. Слова, що починаються
        //    з нової букви, друкувати з абзацного відступу.
//    method6(book);

        //7.	Відсортувати слова тексту за зростанням відсотку голосних букв (співвідношення
        //    кількості голосних до загальної кількості букв у слові).
//    method7(book);

        //8.	Слова тексту, що починаються з голосних букв, відсортувати в алфавітному порядку по
        //    першій приголосній букві слова.
//    method8(book);
    }

    public static void method1(Book book) {
        Map<SentenceElement, List<Sentence>> sentenceCount = new HashMap<>();
        List<SentenceElement> allWords = new ArrayList<>();
        for (Paragraph paragraph : book.getParagraphs()) {
            for (Sentence sentence : paragraph.getSentences()) {
                allWords.addAll(sentence.getWords());
            }
        }
        List<SentenceElement> uniqueWords = allWords.stream().distinct().collect(Collectors.toList());
        for (SentenceElement word : uniqueWords) {
            sentenceCount.put(word, new ArrayList<Sentence>());
            for (Paragraph paragraph : book.getParagraphs()) {
                for (Sentence sentence : paragraph.getSentences()) {
                    if (sentence.hasSentenceElement(word)) {
                        sentenceCount.get(word).add(sentence);
                    }
                }
            }
        }
        Map<SentenceElement, List<Sentence>> sentenceCountSort = sortByValue(sentenceCount);
        for (SentenceElement element : sentenceCountSort.keySet()) {
            System.out.println("\u001B[31m"+element.toString() + " : " + sentenceCountSort.get(element).size()+"\u001B[0m");
            for (Sentence sentence :sentenceCountSort.get(element)) {
                System.out.println(sentence);
            }
        }
        System.out.println();

    }

    public static void method2(Book book) {
        List<Entry> entries = new ArrayList<>();
        for (Paragraph paragraph : book.getParagraphs()) {
            for (Sentence sentence : paragraph.getSentences()) {
                entries.add(new Entry(sentence.getWordsCount(), sentence.toString()));
            }
        }
//        Collections.sort(entries, new Comparator<Entry>() {
//            @Override
//            public int compare(Entry o1, Entry o2) {
//                return o2.getCount() - o1.getCount();
//            }
//        });

        Collections.sort(entries, (o1, o2) -> o2.getCount() - o1.getCount());

        for (Entry entry : entries) {
            System.out.println(entry.count + " : " + entry.text);
        }
    }

    public static void method3(Book book) {
        Paragraph firstParagraph = book.getParagraphs().get(0);
        Sentence firstSentence = firstParagraph.getSentences().get(0);
        List<SentenceElement> words = firstSentence.getWords();
        List<Paragraph> paragraphs = book.getParagraphs();
        List<SentenceElement> find = new ArrayList<>();
        for (int paragraph = 0; paragraph < paragraphs.size(); paragraph++) {
            List<Sentence> sentences = paragraphs.get(paragraph).getSentences();
            for (int sentenceIndex = 0; sentenceIndex < sentences.size(); sentenceIndex++) {
                if ((paragraph == 0) && (sentenceIndex == 0)) {
                    continue;
                }
                for (SentenceElement word : words) {
                    if (sentences.get(sentenceIndex).hasSentenceElement(word)) {
                        find.add(word);
                    }
                }
            }
        }
        words.removeAll(find);
        System.out.println(words);
    }

    public static void method4(Book book, int length) {
        for (Paragraph paragraph : book.getParagraphs()) {
            for (Sentence sentence : paragraph.getSentences()) {
                if (sentence.getType() == SentenceType.QUESTIONING) {
                    System.out.println(sentence);
                    List<SentenceElement> elements = sentence.getWords();
                    List<String> words = new ArrayList<>();
                    for (SentenceElement element : elements) {
                        if (element.toString().length() == length) {
                            words.add(element.toString());
                        }

                    }
                    System.out.println(words.stream().distinct().collect(Collectors.toList()));
                    System.out.println();
                }
            }
        }
    }

    public static void method5(Book book) {
        for (Paragraph paragraph : book.getParagraphs()) {
            for (Sentence sentence : paragraph.getSentences()) {
                List<SentenceElement> sentenceElements = sentence.getSentenceElements();
                int firstWordIndex = 0;
                SentenceElement firstWord = null;
                for (int index = 0; index < sentenceElements.size(); index++) {
                    if (sentenceElements.get(index).getType() == ElementType.WORD) {
                        firstWord = sentenceElements.get(index);
                        firstWordIndex = index;
                        break;
                    }
                }
                System.out.println(firstWord.toString());
                if (firstWord.toString().matches("^[auoeiAUOEI]\\w+")) {
                    int wordMaxLengthIndex = 0;
                    SentenceElement wordMaxLength = sentenceElements.get(0);
                    for (int index = 0; index < sentenceElements.size(); index++) {
                        if (wordMaxLength.toString().length() < sentenceElements.get(index).toString()
                                .length()) {
                            wordMaxLengthIndex = index;
                            wordMaxLength = sentenceElements.get(index);
                        }
                    }
                    SentenceElement tmp = wordMaxLength;
                    sentenceElements.set(wordMaxLengthIndex, firstWord);
                    sentenceElements.set(firstWordIndex, tmp);
                }
            }
        }
        System.out.println(book.toString());
    }

    public static void method6(Book book) {
        List<String> words = new ArrayList<>();
        for (Paragraph paragraph : book.getParagraphs()) {
            for (Sentence sentence : paragraph.getSentences()) {
                List<SentenceElement> sentenceWords = sentence.getWords();
                sentenceWords.stream().forEach(word -> words.add(word.toString().toLowerCase()));
            }
        }
        Collections.sort(words);
        Map<Character, List<String>> wordsMap = new LinkedHashMap<>();
        for (String word : words) {
            if (wordsMap.containsKey(word.charAt(0))) {
                wordsMap.get(word.charAt(0)).add(word);
            } else {
                List<String> wordsSameLetter = new ArrayList<>();
                wordsSameLetter.add(word);
                wordsMap.put(word.charAt(0), wordsSameLetter);
            }
        }
        for (Character key : wordsMap.keySet()) {
            StringBuilder string = new StringBuilder().append(" ");
            for (String word : wordsMap.get(key)) {
                string.append(word).append(" ");
            }
            System.out.println(string);
        }
    }

    public static void method7(Book book) {
        List<String> words = new ArrayList<>();
        for (Paragraph paragraph : book.getParagraphs()) {
            for (Sentence sentence : paragraph.getSentences()) {
                List<SentenceElement> sentenceWords = sentence.getWords();
                sentenceWords.stream().forEach(word -> words.add(word.toString().toLowerCase()));
            }
        }
        Collections.sort(words, new ComparatorVowel());
        System.out.println(words);
    }

    public static void method8(Book book) {
        List<String> wordsVowel = new ArrayList<>();
        for (Paragraph paragraph : book.getParagraphs()) {
            for (Sentence sentence : paragraph.getSentences()) {
                List<SentenceElement> sentenceWords = sentence.getWords();
                sentenceWords.stream().forEach(word -> {
                    if (word.toString().matches("^[auoeiAUOEI][^\\s]*")) {
                        wordsVowel.add(word.toString().toLowerCase());
                    }
                });
            }
        }
        Collections.sort(wordsVowel, new ComparatorConsonant());
        System.out.println(wordsVowel);
    }

    private static Map<SentenceElement, List<Sentence>> sortByValue(Map<SentenceElement, List<Sentence>> unsortMap) {
        // 1. Convert Map to List of Map
        List<Map.Entry<SentenceElement, List<Sentence>>> list =
                new LinkedList<Map.Entry<SentenceElement, List<Sentence>>>(unsortMap.entrySet());
        // 2. Sort list with Collections.sort(), provide a custom Comparator
        //    Try switch the o1 o2 position for a different order
        Collections.sort(list, new Comparator<Map.Entry<SentenceElement, List<Sentence>>>() {
            public int compare(Map.Entry<SentenceElement, List<Sentence>> o1,
                               Map.Entry<SentenceElement, List<Sentence>> o2) {
                return o2.getValue().size() - o1.getValue().size();
            }
        });
        // 3. Loop the sorted list and put it into a new insertion order Map LinkedHashMap
        Map<SentenceElement, List<Sentence>> sortedMap = new LinkedHashMap<SentenceElement, List<Sentence>>();
        for (Map.Entry<SentenceElement, List<Sentence>> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
    }

}
