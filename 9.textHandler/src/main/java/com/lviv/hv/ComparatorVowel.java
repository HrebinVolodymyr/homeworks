package com.lviv.hv;

import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ComparatorVowel implements Comparator {

  @Override
  public int compare(Object o1, Object o2) {

    return percentageOfVowels((String) o1) - percentageOfVowels((String) o2);
  }

  private int percentageOfVowels(String string) {
    Pattern p = Pattern.compile("[auoeiAUOEI]");
    Matcher m = p.matcher(string);
    double count = 0;
    while (m.find()) {
      count++;
    }
    return (int) ((count / string.length()) * 100);
  }
}
