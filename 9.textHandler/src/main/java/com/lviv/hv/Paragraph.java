package com.lviv.hv;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Paragraph {
    private List<Sentence> sentences = new ArrayList<>();

    public Paragraph(String text) {
        parseParagraph(text);
    }

    public Paragraph parseParagraph(String text) {
        String[] textSentences = text.split("(?<=[\\.\\?!])\\s+");
        Arrays.stream(textSentences).forEach(string -> sentences.add(new Sentence(string)));
        return this;
    }

    public List<Sentence> getSentences() {
        return sentences;
    }

    @Override
    public String toString() {
        String text = "";
        for (Sentence sentence : sentences) {
            text = text + sentence.toString() + " ";
        }
        return text;
    }
}
