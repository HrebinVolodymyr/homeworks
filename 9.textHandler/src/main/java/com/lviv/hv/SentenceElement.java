package com.lviv.hv;

import java.util.Objects;
import java.util.regex.Pattern;

public class SentenceElement {

  private String element;

  public SentenceElement(String element) {
    this.element = element;
  }

  public ElementType getType() {
    if (Pattern.matches("\\s", element)) {
      return ElementType.DELIMITER;
    }
    if (Pattern.matches("[\\(\\)\\-\\.\\?!:;,]", element)) {
      return ElementType.PUNCTUATION_MARK;
    }
    if (Pattern.matches(".+", element)) {
      return ElementType.WORD;
    }
    return ElementType.INDEFINITE;
  }

  @Override
  public String toString() {
    return element;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SentenceElement that = (SentenceElement) o;
    return Objects.equals(element, that.element);
  }

  @Override
  public int hashCode() {

    return Objects.hash(element);
  }
}
