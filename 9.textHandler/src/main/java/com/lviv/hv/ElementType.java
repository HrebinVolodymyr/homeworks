package com.lviv.hv;

public enum ElementType {
    WORD,
    DELIMITER,
    PUNCTUATION_MARK,
    INDEFINITE
}
