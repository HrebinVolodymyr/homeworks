package com.lviv.hv;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Sentence {

  private List<SentenceElement> sentenceElements;

  public Sentence(String sentence) {
    parseSentence(sentence);
  }

  public void parseSentence(String text) {
    Pattern pattern = Pattern.compile(
        "([\\w\\.]+\\w+@[a-zA-Z_]+?\\.[a-zA-Z]{2,6})|[A-Za-z0-9]+|\\s|[\\(\\)\\-\\.\\?!:;,_]");
    Matcher matcher = pattern.matcher(text);
    List<SentenceElement> sentenceElements = new ArrayList<>();
    while (matcher.find()) {
      String element = matcher.group();
      SentenceElement sentenceElement = new SentenceElement(element);
      sentenceElements.add(sentenceElement);
    }
    this.sentenceElements = sentenceElements;
  }

  public SentenceType getType() {
    String endSentence = sentenceElements.get(sentenceElements.size()-1).toString();
    if (endSentence.equals(".")) {
      return SentenceType.STORYTELLING;
    }
    if (endSentence.equals("?")) {
      return SentenceType.QUESTIONING;
    }
    if (endSentence.equals("!")) {
      return SentenceType.INDUCTIVE;
    }
    return SentenceType.INDEFINITE;
  }

  public boolean hasSentenceElements(List<SentenceElement> elements) {
    for (SentenceElement element : elements) {
      if(hasSentenceElement(element)){
        return true;
      }
    }
    return false;
  }

  public boolean hasSentenceElement(SentenceElement element) {
    for (SentenceElement sentenceElement : sentenceElements){
      if(element.equals(sentenceElement)){
        return true;
      }
    }
    return false;
  }

  public List<SentenceElement> getWords() {
    List<SentenceElement> words = new ArrayList<>();
    for (SentenceElement sentenceElement : sentenceElements) {
      if (sentenceElement.getType() == ElementType.WORD) {
        words.add(sentenceElement);
      }
    }
    return words;
  }

  public int getWordsCount() {
    int count = 0;
    for (SentenceElement sentenceElement : sentenceElements) {
      if (sentenceElement.getType() == ElementType.WORD) {
        count++;
      }
    }
    return count;
  }

  public List<SentenceElement> getSentenceElements(){
    return sentenceElements;
  }

  public SentenceElement getElementByIndex(int index){
    return sentenceElements.get(index);
  }

//  public void setElementByIndex(Entry entry){
//    sentenceElements.set(entry.count,entry.text);
//  }

  @Override
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    for (SentenceElement sentenceElement : sentenceElements) {
      stringBuilder.append(sentenceElement.toString());
    }
    return stringBuilder.toString();
  }
  //  (\w+\.\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,6})|[A-Za-z0-9']+|\s|[\(\)\-\.\?!:;,]
  //   ([\w\.]+\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,6})|[A-Za-z0-9']+|\s|[\(\)\-\.\?!:;,_]
}
