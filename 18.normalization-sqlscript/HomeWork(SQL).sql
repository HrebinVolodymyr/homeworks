#          4 (Використ підзапитів у конструкції WHERE з викор. IN, ANY, ALL)

-- 1. БД «Комп. фірма». Знайдіть виробників, що випускають ПК, але не ноутбуки (використати операцію IN). 
-- Вивести maker.
SELECT DISTINCT maker FROM product WHERE type='PC' AND NOT maker IN(SELECT maker FROM product WHERE type='Laptop');

-- 2. БД «Комп. фірма». Знайдіть виробників, що випускають ПК, але не ноутбуки (використати ключове слово ALL). 
-- Вивести maker.
SELECT DISTINCT maker FROM product WHERE type='PC' AND maker <> ALL(SELECT maker FROM product WHERE type='Laptop');


-- 3. БД «Комп. фірма». Знайдіть виробників, що випускають ПК, але не ноутбуки (використати ключове слово ANY). 
-- Вивести maker.
SELECT DISTINCT maker FROM product WHERE type='PC' AND  NOT maker = ANY(SELECT maker FROM product WHERE type='Laptop');

-- 4. БД «Комп. фірма». Знайдіть виробників, що випускають одночасно ПК та ноутбуки (використати операцію IN).
-- Вивести maker.
SELECT DISTINCT maker FROM product WHERE type='PC' AND maker IN(SELECT maker FROM product WHERE type='Laptop');

-- 5. БД «Комп. фірма». Знайдіть виробників, що випускають одночасно ПК та ноутбуки (використати ключове слово ALL).
-- Вивести maker.
SELECT DISTINCT maker FROM product WHERE type='PC' AND NOT maker <> ALL(SELECT maker FROM product WHERE type='Laptop');

-- 6. БД «Комп. фірма». Знайдіть виробників, що випускають одночасно ПК та ноутбуки (використати ключове слово ANY).
-- Вивести maker.
SELECT DISTINCT maker FROM product WHERE type='PC' AND maker = ANY(SELECT maker FROM product WHERE type='Laptop');

-- 7. БД «Комп. фірма». Знайти тих виробників ПК, усі моделі ПК яких є у наявності в таблиці PC (використовувати вкладені
-- підзапити та оператори IN, ALL, ANY). 
-- Вивести maker
SELECT DISTINCT maker FROM product WHERE 
type='PC'
AND 
NOT maker in(SELECT DISTINCT maker FROM product WHERE not model in(SELECT model FROM pc WHERE pc.model=product.model) AND type='PC');

-- 8. БД «Кораблі». Вивести класи усіх кораблів України ('Ukraine'). Якщо у БД немає класів кораблів України, тоді вивести класи
-- для усіх наявних у БД країн. 
-- Вивести: country, class.


-- 9. БД «Кораблі». Знайдіть кораблі, «збережені для майбутніх битв», тобто такі, що були виведені з ладу в одній битві
-- ('damaged'), а потім (пізніше у часі) знову брали участь у битвах. 
-- Вивести: ship, battle, date.


-- 10. БД «Комп. фірма». Знайти наявну кількість комп’ютерів, що випущені виробником 'A'.
SELECT COUNT(*) AS count FROM pc WHERE pc.model IN(SELECT model FROM product WHERE maker='A' );

-- 11. БД «Комп. фірма». Знайти виробників ПК, моделей яких немає у продажу (тобто відсутні у таблиці PC).
SELECT DISTINCT maker FROM product WHERE not model in(SELECT model FROM pc WHERE pc.model=product.model) AND type='PC';


-- 12. БД «Комп. фірма». Знайти моделі та ціни ноутбуків, вартість яких є вищою за вартість будь-якого ПК. 
-- Вивести: model, price.
SELECT model, price FROM laptop WHERE price> ANY(SELECT price FROM pc);


#          5 (Використання підзапитів з лог. операцією EXISTS)

-- 1. БД «Комп. фірма». Знайти тих виробників ПК, усі моделі ПК яких є у наявності в таблиці PC (використовуючи операцію EXISTS). 
-- Вивести maker.
SELECT DISTINCT maker FROM product WHERE EXISTS(SELECT * FROM pc WHERE pc.model=product.model); --

-- 2. БД «Комп. фірма». Знайдіть виробників, які б випускали ПК зі швидкістю 750 МГц та вище. 
-- Виведіть: maker.
SELECT DISTINCT maker FROM product WHERE EXISTS(SELECT * FROM pc WHERE pc.model=product.model AND pc.speed>=750);

-- 3. БД «Комп. фірма». Знайдіть виробників, які б випускали одночасно ПК та ноутбуки зі швидкістю 750 МГц та вище.
-- Виведіть: maker.
SELECT DISTINCT maker FROM product WHERE 
	EXISTS(SELECT * FROM product p WHERE p.maker=product.maker AND p.type='PC')
    AND
	EXISTS(SELECT * FROM laptop WHERE laptop.model=product.model AND laptop.speed>=750);	

-- 4. БД «Комп. фірма». Знайдіть виробників принтерів, що випускають ПК з найвищою швидкістю процесора. 
-- Виведіть: maker.
SELECT DISTINCT maker FROM product WHERE 
	EXISTS(SELECT * FROM product p WHERE p.maker=product.maker AND p.type='Printer')
    AND
	EXISTS(SELECT * FROM pc WHERE pc.model=product.model AND pc.speed=(SELECT MAX(speed)FROM pc));

-- 5. За Вашингтонським міжнародним договором від початку 1922 р. заборонялося будувати лінійні кораблі водотоннажністю
-- понад 35 тис. тонн. Вкажіть кораблі, що порушили цей договір (враховувати лише кораблі з відомим спуском на воду, тобто з таблиці Ships). 
-- Виведіть: name, launched, displacement.
SELECT name, launched, displacement FROM classes JOIN ships ON classes.class=ships.class WHERE displacement>35000 AND 
	EXISTS(SELECT * FROM ships WHERE ships.class=classes.class AND launched>1922);

-- 6. БД «Кораблі». Знайдіть класи кораблів, у яких хоча б один корабель був затоплений у битвах. 
-- Вивести: class. (Назви класів кораблів визначати за таблицею Ships, якщо його там немає, тоді порівнювати чи його назва не співпадає з назвою
-- класу, тобто він є головним)  


-- 7. БД «Комп. фірма». Виведіть тих виробників ноутбуків, які також випускають і принтери.
SELECT DISTINCT maker FROM product WHERE type='Laptop' AND EXISTS(SELECT maker FROM product AS p WHERE p.maker=product.maker AND type='Printer');

-- 8. БД «Комп. фірма». Виведіть тих виробників ноутбуків, які не випускають принтери.
SELECT DISTINCT maker FROM product WHERE type='Laptop' AND NOT EXISTS(SELECT maker FROM product AS p WHERE p.maker=product.maker AND type='Printer');


#             6 (Конкатенація стрічок чи мат. обчислення чи робота з датами)

-- 1. БД «Комп. фірма». Виведіть середню ціну ноутбуків з попереднім текстом 'середня ціна = '.
SELECT CONCAT('середня ціна = ',CAST(AVG(price) AS CHAR)) avg FROM laptop;

-- 2. БД «Комп. фірма». Для таблиці PC вивести усю інформацію з коментарями у кожній комірці, наприклад, 'модель: 1121', 'ціна: 600,00' і т.д.
SELECT CONCAT('model: ',model) model, CONCAT('speed: ',speed) speed, CONCAT('ram: ',ram) ram, CONCAT('hd: ',hd) hd, CONCAT('cd: ',cd) cd, CONCAT('price: ',price) price FROM pc;

-- 3. БД «Фірма прий. вторсировини». З таблиці Income виведіть дати у такому форматі: рік.число_місяця.день, наприклад,
-- 2001.02.15 (без формату часу).
SELECT DATE_FORMAT(date, '%Y.%m.%d') date FROM income;

-- 4. БД «Кораблі». Для таблиці Outcomes виведіть дані, а заміть значень стовпця result, виведіть еквівалентні їм надписи
-- українською мовою.
SELECT ship, battle, CONCAT(IF(result='sunk','потоплений',''), IF(result='damaged','пошкоджений',''), IF(result='OK','без пошкоджень','')) AS result FROM outcomes;

-- 5. БД «Аеропорт». Для таблиці Pass_in_trip значення стовпця place розбити на два стовпця з коментарями, наприклад,
-- перший – 'ряд: 2' та другий – 'місце: a'.
SELECT trip_no, date, ID_psg, CONCAT('ряд: ',SUBSTRING(place, 1, 1)) AS `range`, CONCAT('місце: ', SUBSTRING(place, 2, 1)) AS seat FROM pass_in_trip;

-- 6. БД «Аеропорт». Вивести дані для таблиці Trip з об’єднаними значеннями двох стовпців: town_from та town_to, з
-- додатковими коментарями типу: 'from Rostov to Paris'.
SELECT trip_no, ID_comp, plane, CONCAT('from ',town_from,' to ',town_to) AS trip FROM trip;


#                 7 (Статистичні функції та робота з групами)

-- 1. БД «Комп. фірма». Знайдіть принтери, що мають найвищу ціну. Вивести: model, price.
SELECT model, price FROM printer WHERE price=(SELECT MAX(price) FROM printer);

-- 2. БД «Комп. фірма». Знайдіть ноутбуки, швидкість яких є меншою за швидкість будь-якого з ПК. Вивести: type, model, speed.
-- SELECT type, product.model, speed FROM product JOIN laptop ON product.model=laptop.model WHERE speed = ANY(SELECT speed FROM pc);
SELECT type, product.model, speed FROM product JOIN laptop ON product.model=laptop.model WHERE speed<(SELECT MIN(speed) FROM pc);

-- 3. БД «Комп. фірма». Знайдіть виробників найдешевших кольорових принтерів. Вивести: maker, price.
SELECT maker, price FROM product JOIN printer ON product.model=printer.model WHERE price = (SELECT MIN(price) FROM printer WHERE type = 'Jet') AND printer.type = 'Jet';
SELECT maker, price FROM product JOIN printer ON product.model=printer.model where printer.type = 'Jet' GROUP BY maker HAVING MIN(price) = (SELECT MIN(price) FROM printer WHERE type = 'Jet');

-- 4. БД «Комп. фірма». Знайдіть виробників, що випускають по крайній мірі дві різні моделі ПК. Вивести: maker, число
-- моделей. (Підказка: використовувати підзапити у якості обчислювальних стовпців та операцію групування)
SELECT maker, COUNT(model) as count FROM product WHERE type='PC' GROUP BY maker HAVING count>1 ; 

-- 5. БД «Комп. фірма». Знайдіть середній розмір жорсткого диску ПК (одне значення на всіх) тих виробників, які також
-- випускають і принтери. Вивести: середній розмір жорсткого диску.
SELECT AVG(hd) FROM product JOIN pc ON product.model=pc.model WHERE type='PC' AND maker IN(SELECT maker FROM product WHERE type='Printer');

-- 6. БД «Аеропорт». Визначіть кількість рейсів з міста 'London' для кожної дати таблиці Pass_in_trip. Вивести: date, число рейсів.
SELECT trip_date.date, COUNT(trip_no) AS 'count(trip)' FROM(
	SELECT DISTINCT  trip.trip_no, date FROM trip JOIN pass_in_trip ON trip.trip_no=pass_in_trip.trip_no WHERE town_from='London'
) AS trip_date GROUP BY date;

-- 7. БД «Аеропорт». Визначіть дні, коли було виконано максимальну кількість рейсів до міста 'Moscow'. Вивести: date, число рейсів.
SELECT trip_date.date, COUNT(trip_no) AS count FROM(
	SELECT DISTINCT  trip.trip_no, date FROM trip JOIN pass_in_trip ON trip.trip_no=pass_in_trip.trip_no WHERE town_to='Moscow' 
) AS trip_date GROUP BY date;

-- 8. БД «Кораблі». Для кожної країни визначити рік, у якому було спущено на воду максимальна кількість її кораблів. У випадку,
-- якщо виявиться декілька таких років, тоді взяти мінімальний з них. Вивести: country, кількість кораблів, рік.


-- 9. БД «Кораблі». Вкажіть битви, у яких брало участь по крайній мірі два корабля однієї і тієї ж країни (Вибір країни
-- здійснювати через таблицю Ships, а назви кораблів для таблиці Outcomes, що відсутні у таблиці Ships, не брати до уваги).
-- Вивести: назву битви, країну, кількість кораблів.



#         8 (Підзапити у якості обчислювальних стовпців)

-- 1. БД «Комп. фірма». Для таблиці Product отримати підсумковий набір у вигляді таблиці зі стовпцями maker, pc, laptop та printer,
-- в якій для кожного виробника необхідно вказати кількість продукції, що ним випускається, тобто наявну загальну
-- кількість продукції у таблицях, відповідно, PC, Laptop та Printer. (Підказка: використовувати підзапити у якості обчислювальних стовпців)
SELECT maker, SUM((SELECT COUNT(*) FROM pc WHERE product.model=pc.model )) AS PC, 
	SUM((SELECT COUNT(*) FROM laptop WHERE product.model=laptop.model )) AS Laptop,
    SUM((SELECT COUNT(*) FROM printer WHERE product.model=printer.model )) AS Printer
	FROM product group by maker;

-- 2. БД «Комп. фірма». Для кожного виробника знайдіть середній розмір екрану для ноутбуків, що ним випускається. 
-- Вивести: maker, середній розмір екрану. (Підказка: використовувати підзапити у якості обчислювальних стовпців)
SELECT maker,  AVG(screen) FROM product JOIN laptop ON product.model=laptop.model group by maker;

-- 3. БД «Комп. фірма». Знайдіть максимальну ціну ПК, що випускаються кожним виробником. 
-- Вивести: maker, максимальна ціна. (Підказка: використовувати підзапити у якості обчислювальних стовпців)
SELECT DISTINCT maker, (SELECT MAX(price) FROM product JOIN pc ON product.model=pc.model GROUP BY product.maker HAVING product.maker=p.maker) AS max_price FROM product p WHERE type='PC';

-- 4. БД «Комп. фірма». Знайдіть мінімальну ціну ПК, що випускаються кожним виробником. 
-- Вивести: maker, максимальна ціна. (Підказка: використовувати підзапити у якості обчислювальних стовпців)
SELECT DISTINCT maker, (SELECT MIN(price) FROM product JOIN pc ON product.model=pc.model GROUP BY product.maker HAVING product.maker=p.maker) AS min_price FROM product p WHERE type='PC';

-- 5. БД «Комп. фірма». Для кожного значення швидкості ПК, що перевищує 600 МГц, визначіть середню ціну ПК з такою ж швидкістю. 
-- Вивести: speed, середня ціна. (Підказка: використовувати підзапити у якості обчислювальних стовпців)
-- SELECT speed, AVG(price) FROM product JOIN pc ON product.model=pc.model WHERE speed>600 group by speed;
SELECT DISTINCT speed, (SELECT AVG(price) FROM  pc  WHERE speed>600 GROUP BY speed HAVING pc.speed=p.speed) AS 'AVG(price)' FROM pc AS p WHERE speed>600 ;

-- 6. БД «Комп. фірма». Знайдіть середній розмір жорсткого диску ПК кожного з тих виробників, які випускають також і принтери. 
-- Вивести: maker, середній розмір жорсткого диску. (Підказка: використовувати підзапити у якості обчислювальних стовпців)
SELECT AVG(hd) FROM(
	SELECT hd FROM product JOIN pc ON product.model=pc.model WHERE type='PC' AND maker IN(SELECT maker FROM product WHERE type='Printer')
) AS p;
    
-- 7. БД «Кораблі». Вкажіть назву, водотоннажність та число гармат кораблів, що брали участь у битві при 'Guadalcanal'.
 -- Вивести: ship, displacement, numGuns. (Підказка: використовувати підзапити у якості обчислювальних стовпців)
 SELECT ship, 
 (SELECT numGuns FROM ships JOIN classes ON ships.class=classes.class WHERE ships.name = ship) AS numGuns,
 (SELECT displacement FROM ships JOIN classes ON ships.class=classes.class WHERE ships.name = ship) AS displacement
 FROM outcomes WHERE battle='Guadalcanal'; 
 
-- 8. БД «Кораблі». Вкажіть назву, країну та число гармат кораблів, що були пошкоджені у битвах. 
-- Вивести: ship, country, numGuns. (Підказка: використовувати підзапити у якості обчислювальних стовпців)
SELECT ship, 
 (SELECT numGuns FROM ships JOIN classes ON ships.class=classes.class WHERE ships.name = ship) AS numGuns,
 (SELECT country FROM ships JOIN classes ON ships.class=classes.class WHERE ships.name = ship) AS country
 FROM outcomes WHERE result='damaged' AND EXISTS(SELECT * FROM ships WHERE name=outcomes.ship); 


#                   9 (Оператор CASE)

-- 1. БД «Комп. фірма». Для таблиці Product отримати підсумковий набір у вигляді таблиці зі стовпцями maker, pc, в якій для
-- кожного виробника необхідно вказати, чи виробляє він ('yes'), чи ні ('no') відповідний тип продукції. У першому випадку
-- ('yes') додатково вказати поруч у круглих дужках загальну кількість наявної (тобто, що знаходиться у таблиці PC)
-- продукції, наприклад, 'yes(2)'. (Підказка: використовувати підзапити у якості обчислювальних стовпців та оператор CASE)
SELECT maker ,
	CASE 
		WHEN exists(select * FROM product where product.maker=p.maker AND product.type='PC') THEN CONCAT('YES(',(SELECT COUNT(*) FROM product WHERE product.maker=p.maker AND product.type='PC'),')')
		WHEN not exists(select * FROM product where product.maker=p.maker AND product.type='PC') THEN 'NO'
END as ps
FROM (SELECT DISTINCT maker FROM product) as p;

-- 2. БД «Фірма прий. вторсировини». Приймаючи, що прихід та розхід грошей на кожному пункті прийому фіксується не
-- частіше одного разу на день (лише таблиці Income_o та Outcome_o), написати запит з такими вихідними даними: point
-- (пункт), date (дата), inc (прихід), out (розхід). (Підказка: використовувати зовнішнє з’єднання та оператор CASE)


-- 3. БД «Кораблі». Визначити назви усіх кораблів з таблиці Ships, які задовольняють, у крайньому випадку, комбінації будь-яких
-- чотирьох критеріїв з наступного списку: numGuns=8, bore=15, displacement=32000, type='bb', country='USA', launched=1915,
-- class='Kongo'. Вивести: name, numGuns, bore, displacement, type, country, launched, class. (Підказка: використати для
-- перевірки умов оператор CASE)


-- 4. БД «Аеропорт». Для кожного рейсу (таблиця Trip) визначити тривалість його польоту. Вивести: trip_no, назва компанії,
-- plane, town_from, town_to, тривалість польоту. (Підказка: використати для перевірки умов оператор CASE)


-- 5. БД «Фірма прий. вторсировини». Визначіть лідера за сумою виплат у змаганні між кожною парою пунктів з однаковими
-- номерами із двох різних таблиць – Outcome та Outcome_o – на кожний день, коли здійснювався прийом вторинної сировини
-- хоча б на одному з них. Вивести: Номер пункту, дата, текст: –'once a day', якщо сума виплат є більшою у фірми зі звітністю
-- один раз на день; – 'more than once a day', якщо – у фірми зі звітністю декілька разів на день; – 'both', якщо сума виплат є
-- однаковою. (Підказка: для з’єднання таблиць використовувати повне зовнішнє з’єднання, а для перевірки
-- умов оператор CASE; для пунктів що не мають у певні дні видачі грошей – необхідно обробляти NULL-значення за
-- допомогою перевірки умови IS [NOT] NULL)



#                      10 (Об’єднання UNION)


-- 1. БД «Комп. фірма». Знайдіть номера моделей та ціни усіх продуктів (будь-якого типу), що випущені виробником 'B'.
-- Вивести: maker, model, type, price. (Підказка: використовувати оператор UNION)
SELECT maker, product.model, product.type, price FROM product JOIN pc ON product.model=pc.model WHERE product.maker='B'
UNION ALL
SELECT maker, product.model, product.type, price FROM product JOIN laptop ON product.model=laptop.model WHERE product.maker='B'
UNION ALL
SELECT maker, product.model, product.type, price FROM product JOIN printer ON product.model=printer.model WHERE product.maker='B';

-- 2. БД «Комп. фірма». Для кожної моделі продукції з усієї БД виведіть її найвищу ціну. Вивести: type, model, максимальна
-- ціна. (Підказка: використовувати оператор UNION)
SELECT product.type, product.model, MAX(price) FROM product LEFT JOIN printer ON product.model=printer.model WHERE product.type='Printer' GROUP BY product.model
UNION
SELECT product.type, product.model, MAX(price) FROM product LEFT JOIN pc ON product.model=pc.model WHERE product.type='PC' GROUP BY product.model
UNION
SELECT product.type, product.model, MAX(price) FROM product LEFT JOIN laptop ON product.model=laptop.model WHERE product.type='Laptop' GROUP BY product.model;

-- 3. БД «Комп. фірма». Знайдіть середню ціну ПК та ноутбуків, що випущені виробником 'A'. Вивести: одна загальна середня ціна.
-- (Підказка: використовувати оператор UNION)
SELECT AVG(price) 
FROM (
	SELECT price FROM pc WHERE model = ANY(SELECT model FROM product WHERE maker='A' AND type='PC')
	UNION
	SELECT price FROM laptop WHERE model = ANY(SELECT model FROM product WHERE maker='A' AND type='Laptop')
) AS all_price;

-- 4. БД «Кораблі». Перерахуйте назви головних кораблів, що є наявними у БД (врахувати також і кораблі з таблиці Outcomes). 
-- Вивести: назва корабля, class. (Підказка: використовувати оператор UNION та операцію EXISTS)
SELECT name, class FROM ships
UNION
SELECT ship AS name, (ship) AS class FROM outcomes WHERE  EXISTS(SELECT * FROM classes WHERE class=ship);

-- 5. БД «Кораблі». Знайдіть класи, у яких входить лише один корабель з усієї БД (врахувати також кораблі у таблиці
-- Outcomes, яких немає у таблиці Ships). Вивести: class. (Підказка: використовувати оператор UNION та операцію EXISTS)
SELECT class, COUNT(name) AS count FROM(
	SELECT name, class FROM ships
	UNION
	SELECT ship AS name, (ship) AS class FROM outcomes WHERE  EXISTS(SELECT * FROM classes WHERE class=ship)
) AS ships GROUP BY class HAVING count=1;

-- 6. БД «Кораблі». Для кожного класу порахувати кількість кораблів, що входить до нього (врахувати також кораблі у
-- таблиці Outcomes, яких немає у таблиці Ships). Вивести: class, кількість кораблів у класі. (Підказка: використовувати
-- оператор UNION та операцію EXISTS)
SELECT class, COUNT(name) AS count FROM(
	SELECT name, class FROM ships
	UNION
	SELECT ship AS name, (ship) AS class FROM outcomes WHERE  EXISTS(SELECT * FROM classes WHERE class=ship)
) AS ships GROUP BY class;

-- 7. БД «Кораблі». Знайдіть класи, в які входять лише один або два кораблі з цілої БД (врахувати також кораблі у таблиці
-- Outcomes, яких немає у таблиці Ships). Вивести: class, кількість кораблів у класі. (Підказка: використовувати
-- оператор UNION та операцію EXISTS)
SELECT class, COUNT(name) AS count FROM(
	SELECT name, class FROM ships
	UNION
	SELECT ship AS name, (ship) AS class FROM outcomes WHERE  EXISTS(SELECT * FROM classes WHERE class=ship)
) AS ships GROUP BY class HAVING count IN(1,2);

-- 8. БД «Кораблі». Знайдіть назви усіх кораблів з БД, про які можна однозначно сказати, що вони були спущені на воду до
-- 1942 р. Вивести: назву кораблів. (Підказка: використовувати оператор UNION )
SELECT name FROM ships WHERE launched<1942
UNION
SELECT ship AS name FROM outcomes JOIN battles ON battle=name WHERE YEAR(date)<1942;

