
CREATE SCHEMA IF NOT EXISTS `record_ student_success` DEFAULT CHARACTER SET utf8 ;
USE `record_ student_success` ;

CREATE TABLE IF NOT EXISTS `record_ student_success`.`city` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `record_ student_success`.`region` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `record_ student_success`.`street` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `record_ student_success`.`address` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `house` VARCHAR(10) NOT NULL,
  `flat` VARCHAR(10) NULL DEFAULT NULL,
  `region_id` INT(11) NOT NULL,
  `city_id` INT(11) NOT NULL,
  `street_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_address_region1_idx` (`region_id` ASC),
  INDEX `fk_address_city1_idx` (`city_id` ASC),
  INDEX `fk_address_street1_idx` (`street_id` ASC),
  CONSTRAINT `fk_address_city1`
    FOREIGN KEY (`city_id`)
    REFERENCES `record_ student_success`.`city` (`id`),
  CONSTRAINT `fk_address_region1`
    FOREIGN KEY (`region_id`)
    REFERENCES `record_ student_success`.`region` (`id`),
  CONSTRAINT `fk_address_street1`
    FOREIGN KEY (`street_id`)
    REFERENCES `record_ student_success`.`street` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `record_ student_success`.`control` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` ENUM('exam', 'test') NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `record_ student_success`.`group` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `specialty` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `record_ student_success`.`student` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(30) NOT NULL,
  `middle_name` VARCHAR(30) NOT NULL,
  `surname` VARCHAR(30) NOT NULL,
  `photo` LONGBLOB NULL DEFAULT NULL,
  `autobiography` BLOB NULL DEFAULT NULL,
  `year_ birth` YEAR(4) NOT NULL,
  `year_accession` YEAR(4) NOT NULL,
  `current_rating` INT(11) NULL DEFAULT NULL,
  `scholarship_size` INT(11) NULL DEFAULT NULL,
  `address_id` INT(11) NOT NULL,
  `group_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_student_group1_idx` (`group_id` ASC),
  INDEX `fk_student_address1_idx` (`address_id` ASC),
  CONSTRAINT `fk_student_address1`
    FOREIGN KEY (`address_id`)
    REFERENCES `record_ student_success`.`address` (`id`),
  CONSTRAINT `fk_student_group1`
    FOREIGN KEY (`group_id`)
    REFERENCES `record_ student_success`.`group` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `record_ student_success`.`subject` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `record_ student_success`.`teacher` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(30) NOT NULL,
  `middle_name` VARCHAR(30) NOT NULL,
  `surname` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `record_ student_success`.`teacher_has_subject` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `teacher_id` INT(11) NOT NULL,
  `subject_id` INT(11) NOT NULL,
  `control_id` INT(11) NOT NULL,
  INDEX `fk_teacher_has_subject_subject1_idx` (`subject_id` ASC),
  INDEX `fk_teacher_has_subject_teacher1_idx` (`teacher_id` ASC),
  PRIMARY KEY (`id`),
  INDEX `fk_teacher_has_subject_control1_idx` (`control_id` ASC),
  CONSTRAINT `fk_teacher_has_subject_subject1`
    FOREIGN KEY (`subject_id`)
    REFERENCES `record_ student_success`.`subject` (`id`),
  CONSTRAINT `fk_teacher_has_subject_teacher1`
    FOREIGN KEY (`teacher_id`)
    REFERENCES `record_ student_success`.`teacher` (`id`),
  CONSTRAINT `fk_teacher_has_subject_control1`
    FOREIGN KEY (`control_id`)
    REFERENCES `record_ student_success`.`control` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `record_ student_success`.`student_achievement` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `semester` INT(11) NOT NULL,
  `module №1` INT(11) NULL DEFAULT NULL,
  `module №2` INT(11) NULL DEFAULT NULL,
  `score_100_ ball` INT(11) NULL DEFAULT NULL,
  `score_5_ball` INT(11) NULL DEFAULT NULL,
  `student_id` INT(11) NOT NULL,
  `teacher_has_subject_id` INT NOT NULL,
  INDEX `fk_student_has_subject_student1_idx` (`student_id` ASC),
  INDEX `fk_student_has_subject_teacher_has_subject1_idx` (`teacher_has_subject_id` ASC),
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_student_has_subject_student1`
    FOREIGN KEY (`student_id`)
    REFERENCES `record_ student_success`.`student` (`id`),
  CONSTRAINT `fk_student_has_subject_teacher_has_subject1`
    FOREIGN KEY (`teacher_has_subject_id`)
    REFERENCES `record_ student_success`.`teacher_has_subject` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;