
LOCK TABLES `address` WRITE;
ALTER TABLE `address` DISABLE KEYS ;
INSERT INTO `address` VALUES (1,'25','45',1,1,1),(2,'56','19',1,1,2),(3,'45','15',1,2,3),(4,'14','75',1,5,4),(5,'78','55',2,4,2),(6,'6','15',3,3,3);
ALTER TABLE `address` ENABLE KEYS ;
UNLOCK TABLES;

LOCK TABLES `city` WRITE;
 ALTER TABLE `city` DISABLE KEYS ;
INSERT INTO `city` VALUES (1,'Львів'),(2,'Стрий'),(3,'Рівне'),(4,'Луцьк'),(5,'Броди');
ALTER TABLE `city` ENABLE KEYS ;
UNLOCK TABLES;

LOCK TABLES `control` WRITE;
ALTER TABLE `control` DISABLE KEYS ;
INSERT INTO `control` VALUES (1,'exam'),(2,'test');
ALTER TABLE `control` ENABLE KEYS;
UNLOCK TABLES;

LOCK TABLES `group` WRITE;
ALTER TABLE `group` DISABLE KEYS;
INSERT INTO `group` VALUES (1,'ЕК-103','Економічна кібернетика'),(2,'ОБ-104','Облік і аудит');
ALTER TABLE `group` ENABLE KEYS;
UNLOCK TABLES;

LOCK TABLES `region` WRITE;
ALTER TABLE `region` DISABLE KEYS;
INSERT INTO `region` VALUES (1,'Львівська'),(2,'Волинська'),(3,'Рівненська');
ALTER TABLE `region` ENABLE KEYS;
UNLOCK TABLES;

LOCK TABLES `street` WRITE;
ALTER TABLE `street` DISABLE KEYS;
INSERT INTO `street` VALUES (1,'Шевченка'),(2,'Балабана'),(3,'Виговського'),(4,'Зернова');
ALTER TABLE `street` ENABLE KEYS;
UNLOCK TABLES;

LOCK TABLES `student` WRITE;
ALTER TABLE `student` DISABLE KEYS;
INSERT INTO `student` VALUES (1,'Тарас','Анатолійович','Івановський',NULL,NULL,1990,2006,NULL,NULL,1,1),(2,'Олена','Степанівна','Кіт',NULL,NULL,1990,2006,NULL,NULL,2,2),(3,'Дмитро','Анатолійович','Степанов',NULL,NULL,1990,2006,NULL,NULL,3,1),(4,'Іван','Іванович','Ільків',NULL,NULL,1989,2006,NULL,NULL,4,2),(5,'Сергій','Володимирович','Бистров',NULL,NULL,1990,2006,NULL,NULL,5,1),(6,'Анастасія','Ростиславівна','Савицька',NULL,NULL,1990,2006,NULL,NULL,6,2);
ALTER TABLE `student` ENABLE KEYS ;
UNLOCK TABLES;

LOCK TABLES `student_achievement` WRITE;
ALTER TABLE `student_achievement` DISABLE KEYS;
INSERT INTO `student_achievement` VALUES (1,1,89,NULL,NULL,NULL,1,1),(2,1,88,NULL,NULL,NULL,1,2),(3,1,87,NULL,NULL,NULL,1,3),(4,1,82,NULL,NULL,NULL,2,3),(5,1,85,NULL,NULL,NULL,2,4),(6,1,83,NULL,NULL,NULL,2,5),(7,1,86,NULL,NULL,NULL,3,1),(8,1,88,NULL,NULL,NULL,3,2),(9,1,87,NULL,NULL,NULL,3,3),(10,1,82,NULL,NULL,NULL,4,3),(11,1,85,NULL,NULL,NULL,4,4),(12,1,83,NULL,NULL,NULL,4,5),(13,1,86,NULL,NULL,NULL,5,1),(14,1,88,NULL,NULL,NULL,5,2),(15,1,87,NULL,NULL,NULL,5,3),(16,1,82,NULL,NULL,NULL,6,3),(17,1,85,NULL,NULL,NULL,6,4),(18,1,83,NULL,NULL,NULL,6,5);
ALTER TABLE `student_achievement` ENABLE KEYS;
UNLOCK TABLES;

LOCK TABLES `subject` WRITE;
ALTER TABLE `subject` DISABLE KEYS;
INSERT INTO `subject` VALUES (1,'Мережеві технології'),(2,'Вища математика'),(3,'Мікроекономіка'),(4,'Макроекономіка'),(5,'Статистика');
ALTER TABLE `subject` ENABLE KEYS;
UNLOCK TABLES;

LOCK TABLES `teacher` WRITE;
ALTER TABLE `teacher` DISABLE KEYS;
INSERT INTO `teacher` VALUES (1,'Василь','Степанович','Прошак'),(2,'Михайло','Васильович','Кремінь'),(3,'Любомир','Андрійович','Саландяк'),(4,'Вікторія ','Миколаївна','Наумець');
ALTER TABLE `teacher` ENABLE KEYS;
UNLOCK TABLES;

LOCK TABLES `teacher_has_subject` WRITE;
ALTER TABLE `teacher_has_subject` DISABLE KEYS;
INSERT INTO `teacher_has_subject` VALUES (1,1,3,1),(2,1,4,1),(3,2,2,2),(4,3,1,1),(5,4,5,2);
ALTER TABLE `teacher_has_subject` ENABLE KEYS;
UNLOCK TABLES;
