package com.lviv.hv.service.impl;

import com.lviv.hv.domain.EmployeeEntity;
import com.lviv.hv.repository.EmployeeRepository;
import com.lviv.hv.service.EmployeeService;
import com.lviv.hv.service.ServiceException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class EmployeeServiceImpl implements EmployeeService {

  @Autowired
  EmployeeRepository employeeRepository;

  @Override
  @Transactional
  public List<EmployeeEntity> findAll() throws ServiceException {
    return null;
  }

  @Override
  @Transactional
  public EmployeeEntity findById(Integer id) throws ServiceException {
    return null;
  }

  @Override
  @Transactional
  public void create(EmployeeEntity entity) throws ServiceException {

  }

  @Override
  @Transactional
  public void update(EmployeeEntity entity) throws ServiceException {

  }

  @Override
  @Transactional
  public void delete(Integer id) throws ServiceException {

  }

  @Override
  @Transactional
  public List<EmployeeEntity> findByName(String name) throws ServiceException {
    return null;
  }

}
