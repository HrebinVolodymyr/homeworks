package com.lviv.hv.service.impl;

import com.lviv.hv.domain.PK_WorksOn;
import com.lviv.hv.domain.WorksOnEntity;
import com.lviv.hv.repository.WorksOnRepository;
import com.lviv.hv.service.ServiceException;
import com.lviv.hv.service.WorkOnService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class WorkOnServiceImpl implements WorkOnService {

  @Autowired
  WorksOnRepository worksOnRepository;

  @Override
  @Transactional
  public List<WorksOnEntity> findAll() throws ServiceException {
    return null;
  }

  @Override
  @Transactional
  public WorksOnEntity findById(PK_WorksOn id) throws ServiceException {
    return null;
  }

  @Override
  @Transactional
  public void create(WorksOnEntity entity) throws ServiceException {

  }

  @Override
  @Transactional
  public void update(WorksOnEntity entity) throws ServiceException {

  }

  @Override
  @Transactional
  public void delete(PK_WorksOn id) throws ServiceException {

  }
}
