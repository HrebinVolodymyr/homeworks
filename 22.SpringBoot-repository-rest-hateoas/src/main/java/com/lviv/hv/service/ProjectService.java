package com.lviv.hv.service;

import com.lviv.hv.domain.ProjectEntity;

public interface ProjectService extends GeneralService<ProjectEntity, String> {

}
