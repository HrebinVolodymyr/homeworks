package com.lviv.hv.service;

import com.lviv.hv.domain.DepartmentEntity;

public interface DepartmentService extends GeneralService<DepartmentEntity,String> {

  public void deleteWithMoveOfEmployees(String idDeleted, String idMoveTo) throws ServiceException;

}
