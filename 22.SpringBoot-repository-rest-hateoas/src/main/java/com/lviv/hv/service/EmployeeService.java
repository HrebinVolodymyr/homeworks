package com.lviv.hv.service;

import com.lviv.hv.domain.EmployeeEntity;
import java.util.List;

public interface EmployeeService extends GeneralService<EmployeeEntity, Integer> {

  public List<EmployeeEntity> findByName(String name) throws ServiceException;

}
