package com.lviv.hv.service;

import com.lviv.hv.domain.PK_WorksOn;
import com.lviv.hv.domain.WorksOnEntity;

public interface WorkOnService extends GeneralService<WorksOnEntity, PK_WorksOn> {

}
