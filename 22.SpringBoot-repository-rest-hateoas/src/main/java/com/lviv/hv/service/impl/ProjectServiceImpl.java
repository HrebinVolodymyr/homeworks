package com.lviv.hv.service.impl;

import com.lviv.hv.domain.ProjectEntity;
import com.lviv.hv.repository.ProjectRepository;
import com.lviv.hv.service.ProjectService;
import com.lviv.hv.service.ServiceException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProjectServiceImpl implements ProjectService {

  @Autowired
  ProjectRepository projectRepository;

  @Override
  @Transactional
  public List<ProjectEntity> findAll() throws ServiceException {
    return null;
  }

  @Override
  @Transactional
  public ProjectEntity findById(String id) throws ServiceException {
    return null;
  }

  @Override
  @Transactional
  public void create(ProjectEntity entity) throws ServiceException {

  }

  @Override
  @Transactional
  public void update(ProjectEntity entity) throws ServiceException {

  }

  @Override
  @Transactional
  public void delete(String id) throws ServiceException {

  }
}

