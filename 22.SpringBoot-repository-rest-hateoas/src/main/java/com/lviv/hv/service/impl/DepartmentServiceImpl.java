package com.lviv.hv.service.impl;

import com.lviv.hv.exceptions.NoSuchDepartmentException;
import com.lviv.hv.domain.DepartmentEntity;
import com.lviv.hv.repository.DepartmentRepository;
import com.lviv.hv.service.DepartmentService;
import com.lviv.hv.service.ServiceException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DepartmentServiceImpl implements DepartmentService {

  @Autowired
  DepartmentRepository departmentRepository;

  @Override
  @Transactional
  public List<DepartmentEntity> findAll() throws ServiceException {
    return departmentRepository.findAll();
  }

  @Override
  @Transactional
  public DepartmentEntity findById(String id) throws ServiceException {
    return departmentRepository.findById(id).get();
  }

  @Override
  @Transactional
  public void create(DepartmentEntity entity) throws ServiceException {
    departmentRepository.save(entity);
  }

  @Override
  @Transactional
  public void update(DepartmentEntity entity) throws NoSuchDepartmentException {
    DepartmentEntity departmentEntity = departmentRepository.findById(entity.getDeptNo()).get();
    if (departmentEntity == null) {
      throw new NoSuchDepartmentException("Department not exist");
    }
    departmentRepository.save(entity);
  }

  @Override
  @Transactional
  public void delete(String id) throws ServiceException {
    DepartmentEntity departmentEntity = departmentRepository.findById(id).get();
    if (departmentEntity == null) {
      throw new NoSuchDepartmentException("Department not exist");
    }
    departmentRepository.deleteById(id);
  }

  @Override
  @Transactional
  public void deleteWithMoveOfEmployees(String idDeleted, String idMoveTo) throws ServiceException {

  }
}
