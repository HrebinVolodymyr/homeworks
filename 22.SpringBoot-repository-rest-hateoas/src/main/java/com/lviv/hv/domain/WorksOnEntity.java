package com.lviv.hv.domain;

import java.sql.Date;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "works_on", schema = "sample")
@IdClass(PK_WorksOn.class)
public class WorksOnEntity {

  @Id
  @Column(name = "emp_no", nullable = false)
  private Integer empNo;

  @Id
  @Column(name = "project_no", nullable = false, length = 10)
  private String projectNo;

  @Basic
  @Column(name = "job", nullable = true, length = 45)
  private String job;

  @Basic
  @Column(name = "enter_date", nullable = true)
  private Date date;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "emp_no", referencedColumnName = "emp_no", nullable = false, insertable = false, updatable = false)
  private EmployeeEntity employee;

  @ManyToOne
  @JoinColumn(name = "project_no", referencedColumnName = "project_no", nullable = false, insertable = false, updatable = false)
  private ProjectEntity project;

  public WorksOnEntity() {
    super();
  }

  public WorksOnEntity(Integer empNo, String projectNo, String job, Date date) {
    this.empNo = empNo;
    this.projectNo = projectNo;
    this.job = job;
    this.date = date;
  }

  public Integer getEmpNo() {
    return empNo;
  }

  public void setEmpNo(Integer empNo) {
    this.empNo = empNo;
  }

  public String getProjectNo() {
    return projectNo;
  }

  public void setProjectNo(String projectNo) {
    this.projectNo = projectNo;
  }

  public String getJob() {
    return job;
  }

  public void setJob(String job) {
    this.job = job;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public EmployeeEntity getEmployee() {
    return employee;
  }

  public void setEmployee(EmployeeEntity employee) {
    this.employee = employee;
  }

  public ProjectEntity getProject() {
    return project;
  }

  public void setProject(ProjectEntity project) {
    this.project = project;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WorksOnEntity that = (WorksOnEntity) o;
    return Objects.equals(empNo, that.empNo) &&
        Objects.equals(projectNo, that.projectNo) &&
        Objects.equals(job, that.job) &&
        Objects.equals(date, that.date);
  }

  @Override
  public int hashCode() {
    return Objects.hash(empNo, projectNo, job, date);
  }

}