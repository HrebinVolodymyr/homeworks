package com.lviv.hv.domain;

import java.util.Objects;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "account", schema = "sample")
public class AccountEntity {

  private Integer id;
  private String login;
  private String password;
  private EmployeeEntity employee;
  private Set<SqlGrantEntity> sqlGrants;

  @Id
  @Column(name = "id", nullable = false)
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Basic
  @Column(name = "login", nullable = false, length = 15)
  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  @Basic
  @Column(name = "password", nullable = false, length = 45)
  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AccountEntity that = (AccountEntity) o;
    return Objects.equals(id, that.id) &&
        Objects.equals(login, that.login) &&
        Objects.equals(password, that.password);
  }

  @Override
  public int hashCode() {

    return Objects.hash(id, login, password);
  }

  @OneToOne(mappedBy = "account")
  public EmployeeEntity getEmployee() {
    return employee;
  }

  public void setEmployee(EmployeeEntity employee) {
    this.employee = employee;
  }

  @ManyToMany
  @JoinTable(name = "account_has_sql_grant", schema = "sample", joinColumns = @JoinColumn(name = "account_id", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "sql_grant_id", referencedColumnName = "id", nullable = false))
  public Set<SqlGrantEntity> getSqlGrants() {
    return sqlGrants;
  }

  public void setSqlGrants(Set<SqlGrantEntity> sqlGrants) {
    this.sqlGrants = sqlGrants;
  }
}
