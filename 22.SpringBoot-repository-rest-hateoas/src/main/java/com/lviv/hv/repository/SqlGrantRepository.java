package com.lviv.hv.repository;

import com.lviv.hv.domain.SqlGrantEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SqlGrantRepository extends JpaRepository<SqlGrantEntity, Integer> {

}
