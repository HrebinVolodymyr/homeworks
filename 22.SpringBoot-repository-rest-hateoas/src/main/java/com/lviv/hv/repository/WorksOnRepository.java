package com.lviv.hv.repository;

import com.lviv.hv.domain.PK_WorksOn;
import com.lviv.hv.domain.WorksOnEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WorksOnRepository extends JpaRepository<WorksOnEntity, PK_WorksOn> {

}
