package com.lviv.hv.exceptions;

import com.lviv.hv.service.ServiceException;

public class NoSuchDepartmentException extends ServiceException {

  public NoSuchDepartmentException(String message) {
    super(message);
  }

  public NoSuchDepartmentException(String message, Throwable cause) {
    super(message, cause);
  }
}
