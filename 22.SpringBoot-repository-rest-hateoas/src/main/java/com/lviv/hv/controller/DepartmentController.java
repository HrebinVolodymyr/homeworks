package com.lviv.hv.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import com.lviv.hv.DTO.DepartmentDTO;
import com.lviv.hv.domain.DepartmentEntity;
import com.lviv.hv.service.impl.DepartmentServiceImpl;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DepartmentController {

  @Autowired
  DepartmentServiceImpl departmentService;

  @GetMapping(value = "/api/department")
  public ResponseEntity<List<DepartmentDTO>> getAllDepartments(){
    List<DepartmentEntity> departments = departmentService.findAll();
    Link link = linkTo(methodOn(DepartmentController.class).getAllDepartments()).withSelfRel();

    List<DepartmentDTO> departmentDTOS = new ArrayList<>();
    for (DepartmentEntity entity : departments) {
      Link selfLink = new Link(link.getHref() + "/" + entity.getDeptNo()).withSelfRel();
      DepartmentDTO dto = new DepartmentDTO(entity, selfLink);
      departmentDTOS.add(dto);
    }
    return new ResponseEntity<>(departmentDTOS, HttpStatus.OK);
  }

}
