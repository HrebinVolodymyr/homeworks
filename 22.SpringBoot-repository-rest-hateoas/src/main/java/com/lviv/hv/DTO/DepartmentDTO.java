package com.lviv.hv.DTO;

import com.lviv.hv.domain.DepartmentEntity;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

public class DepartmentDTO extends ResourceSupport {

  DepartmentEntity department;

  public DepartmentDTO(DepartmentEntity department, Link selfLink) {
    this.department = department;
    add(selfLink);
  }

  public String getDepartmentNo() {
    return department.getDeptNo();
  }

  public String getDepartmentName() {
    return department.getDeptName();
  }

  public String getDepartmentLocation() {
    return department.getLocation();
  }

}
