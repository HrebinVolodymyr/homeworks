DROP TRIGGER IF EXISTS fk_street_insert;
DROP TRIGGER IF EXISTS fk_street_update;
DROP TRIGGER IF EXISTS street_delete;
DROP TRIGGER IF EXISTS street_update;

DROP TRIGGER IF EXISTS fk_post_insert;
DROP TRIGGER IF EXISTS fk_post_update;
DROP TRIGGER IF EXISTS post_delete;
DROP TRIGGER IF EXISTS post_update;

DROP TRIGGER IF EXISTS pharmacy_update;
DROP TRIGGER IF EXISTS pharmacy_delete;
DROP TRIGGER IF EXISTS p_m_medicine_update;
DROP TRIGGER IF EXISTS p_m_medicine_delete;
DROP TRIGGER IF EXISTS fk_pharmacy_medicine_insert;
DROP TRIGGER IF EXISTS fk_pharmacy_medicine_update;

DROP TRIGGER IF EXISTS zone_update;
DROP TRIGGER IF EXISTS zone_delete;
DROP TRIGGER IF EXISTS m_z_medicine_update;
DROP TRIGGER IF EXISTS m_z_medicine_delete;
DROP TRIGGER IF EXISTS fk_zone_medicine_insert;
DROP TRIGGER IF EXISTS fk_zone_medicine_update;

DROP TRIGGER IF EXISTS medicine_insert_checking_ministry_code;
DROP TRIGGER IF EXISTS medicine_update_checking_ministry_code;

-- 1. Забезпечити цілісність значень для структури БД.
-- pharmacy(M:1)street
DELIMITER //
CREATE TRIGGER fk_street_insert
BEFORE INSERT
ON pharmacy FOR EACH ROW
BEGIN
IF ((new.street IS NOT NULL) AND (NOT EXISTS(SELECT * FROM street WHERE street.street=new.street)))
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'ERROR: street does not exist in the table street';
END IF;
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER fk_street_update
BEFORE UPDATE
ON pharmacy FOR EACH ROW
BEGIN
IF ((new.street IS NOT NULL) AND (NOT EXISTS(SELECT * FROM street WHERE street.street=new.street)))
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'ERROR: street does not exist in the table street';
END IF;
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER street_delete
BEFORE DELETE
ON street FOR EACH ROW
BEGIN
IF (EXISTS(SELECT * FROM pharmacy WHERE pharmacy.street=old.street))
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'ERROR:  street exists in the pharmacy table';
END IF;
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER street_update
BEFORE UPDATE
ON street FOR EACH ROW
BEGIN
IF (EXISTS(SELECT * FROM pharmacy WHERE pharmacy.street=old.street))
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'ERROR:  street exists in the pharmacy table';
END IF;
END //
DELIMITER ;

-- employee(M:1)post
DELIMITER //
CREATE TRIGGER fk_post_insert
BEFORE INSERT
ON employee FOR EACH ROW
BEGIN
IF ((new.post IS NOT NULL) AND (NOT EXISTS(SELECT * FROM post WHERE post.post=new.post)))
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'ERROR: post does not exist in the table post';
END IF;
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER fk_post_update
BEFORE UPDATE
ON employee FOR EACH ROW
BEGIN
IF ((new.post IS NOT NULL) AND (NOT EXISTS(SELECT * FROM post WHERE post.post=new.post)))
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'ERROR: post does not exist in the table post';
END IF;
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER post_delete
BEFORE DELETE
ON post FOR EACH ROW
BEGIN
IF (EXISTS(SELECT * FROM employee WHERE employee.post=old.post))
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'ERROR:  post exists in the employee table';
END IF;
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER post_update
BEFORE UPDATE
ON post FOR EACH ROW
BEGIN
IF (EXISTS(SELECT * FROM employee WHERE employee.post=old.post))
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'ERROR:  post exists in the employee table';
END IF;
END //
DELIMITER ;

-- pharmacy(M:M)medicine
DELIMITER //
CREATE TRIGGER pharmacy_update
BEFORE UPDATE
ON pharmacy FOR EACH ROW
BEGIN
IF (EXISTS(SELECT * FROM pharmacy_medicine WHERE pharmacy_medicine.pharmacy_id=old.id))
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'ERROR:  pharmacy:id exists in the pharmacy_medicine table';
END IF;
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER pharmacy_delete
BEFORE DELETE
ON pharmacy FOR EACH ROW
BEGIN
IF (EXISTS(SELECT * FROM pharmacy_medicine WHERE pharmacy_medicine.pharmacy_id=old.id))
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'ERROR:  pharmacy exists in the pharmacy_medicine table';
END IF;
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER p_m_medicine_update
BEFORE UPDATE
ON medicine FOR EACH ROW
BEGIN
IF (EXISTS(SELECT * FROM pharmacy_medicine WHERE pharmacy_medicine.medicine_id=old.id))
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'ERROR:  medicine exists in the pharmacy_medicine table';
END IF;
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER p_m_medicine_delete
BEFORE DELETE
ON medicine FOR EACH ROW
BEGIN
IF (EXISTS(SELECT * FROM pharmacy_medicine WHERE pharmacy_medicine.medicine_id=old.id))
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'ERROR:  medicine exists in the pharmacy_medicine table';
END IF;
END //
DELIMITER ;


DELIMITER //
CREATE TRIGGER fk_pharmacy_medicine_insert
BEFORE INSERT
ON pharmacy_medicine FOR EACH ROW
BEGIN
IF ((NOT EXISTS(SELECT * FROM pharmacy WHERE pharmacy.id=new.pharmacy_id)) 
	OR 
    (NOT EXISTS(SELECT * FROM medicine WHERE medicine.id=new.medicine_id)))
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'ERROR: id does not exist in the table';
END IF;
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER fk_pharmacy_medicine_update
BEFORE UPDATE
ON pharmacy_medicine FOR EACH ROW
BEGIN
IF ((NOT EXISTS(SELECT * FROM pharmacy WHERE pharmacy.id=new.pharmacy_id)) 
	OR 
    (NOT EXISTS(SELECT * FROM medicine WHERE medicine.id=new.medicine_id)))
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'ERROR: id does not exist in the table';
END IF;
END //
DELIMITER ;

-- zone(M:M)medicine
DELIMITER //
CREATE TRIGGER zone_update
BEFORE UPDATE
ON zone FOR EACH ROW
BEGIN
IF (EXISTS(SELECT * FROM medicine_zone WHERE medicine_zone.zone_id=old.id))
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'ERROR:  pharmacy:id exists in the medicine_zone table';
END IF;
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER zone_delete
BEFORE DELETE
ON zone FOR EACH ROW
BEGIN
IF (EXISTS(SELECT * FROM medicine_zone WHERE medicine_zone.zone_id=old.id))
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'ERROR:  pharmacy exists in the medicine_zone table';
END IF;
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER m_z_medicine_update
BEFORE UPDATE
ON medicine FOR EACH ROW
BEGIN
IF (EXISTS(SELECT * FROM medicine_zone WHERE medicine_zone.medicine_id=old.id))
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'ERROR:  medicine exists in the medicine_zone table';
END IF;
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER m_z_medicine_delete
BEFORE DELETE
ON medicine FOR EACH ROW
BEGIN
IF (EXISTS(SELECT * FROM medicine_zone WHERE medicine_zone.medicine_id=old.id))
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'ERROR:  medicine exists in the medicine_zone table';
END IF;
END //
DELIMITER ;


DELIMITER //
CREATE TRIGGER fk_medicine_zone_insert
BEFORE INSERT
ON medicine_zone FOR EACH ROW
BEGIN
IF ((NOT EXISTS(SELECT * FROM zone WHERE zone.id=new.zone_id)) 
	OR 
    (NOT EXISTS(SELECT * FROM medicine WHERE medicine.id=new.medicine_id)))
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'ERROR: id does not exist in the table';
END IF;
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER fk_medicine_zone_update
BEFORE UPDATE
ON medicine_zone FOR EACH ROW
BEGIN
IF ((NOT EXISTS(SELECT * FROM zone WHERE zone.id=new.zone_id)) 
	OR 
    (NOT EXISTS(SELECT * FROM medicine WHERE medicine.id=new.medicine_id)))
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'ERROR: id does not exist in the table';
END IF;
END //
DELIMITER ;

-- 2. Співробітники→ Ідентифікаційний номер не може закінчувати двома нулями;
DELIMITER //
CREATE TRIGGER employee_insert_checking_identity_number
BEFORE INSERT
ON employee FOR EACH ROW
BEGIN
IF (new.identity_number RLIKE '.*00$')
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'ERROR: identity number can not end with two zeros';
END IF;
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER employee_update_checking_identity_number
BEFORE UPDATE
ON employee FOR EACH ROW
BEGIN
IF (new.identity_number RLIKE '.*00$')
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'ERROR: identity number can not end with two zeros';
END IF;
END //
DELIMITER ;

-- 3. для Перелік лікарств→ Код міністерства забезпечити формат вводу:
-- 2 довільні букви, окрім М і П + '-' + 3 цифри + '-' + 2цифри.
DELIMITER //
CREATE TRIGGER medicine_insert_checking_ministry_code
BEFORE INSERT
ON medicine FOR EACH ROW
BEGIN
IF (NOT new.ministry_code RLIKE '^[А-ЛН-ОР-Я][А-ЛН-ОР-Я]-[0-9][0-9][0-9]-[0-9][0-9]$')
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'ERROR: incorrect ministry code format';
END IF;
END //
DELIMITER ;

DELIMITER //
CREATE TRIGGER medicine_update_checking_ministry_code
BEFORE UPDATE
ON medicine FOR EACH ROW
BEGIN
IF (NOT new.ministry_code RLIKE '^[А-ЛН-ОР-Я][А-ЛН-ОР-Я]-[0-9][0-9][0-9]-[0-9][0-9]$')
THEN SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'ERROR: incorrect ministry code format';
END IF;
END //
DELIMITER ;

-- 4. Заборонити будь-яку модифікацію даних в таблиці Посада.
DELIMITER //
CREATE TRIGGER post_update_prohibited
BEFORE UPDATE
ON post FOR EACH ROW
BEGIN
 SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'ERROR: modification of data is prohibited';
END //
DELIMITER ;