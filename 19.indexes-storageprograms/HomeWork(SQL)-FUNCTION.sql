-- 1. Для таблиці Співробітники написати функцію як буде шукати MIN
-- стовпця Трудовий стаж . Потім зробити вибірку даних (SELECT),
-- використовуючи дану функцію.
CREATE FUNCTION min_experience ()
RETURNS DECIMAL(10,1) DETERMINISTIC
RETURN (SELECT min(experience) as e FROM employee);

SELECT * FROM employee WHERE experience = min_experience();


-- 2. Написати функцію, яка витягує за ключем між таблицями
-- Співробітники та Аптечна установа об’єднане значення полів Назва та
-- Noбудинку. Потім зробити вибірку усіх даних (SELECT) з таблиці
-- Співробітники, використовуючи дану функцію.
CREATE FUNCTION concat_pharmacy (pharmacy_id int)
RETURNS CHAR(50) DETERMINISTIC
RETURN (SELECT CONCAT(name,' ',building_number ) FROM pharmacy WHERE pharmacy.id = pharmacy_id);

SELECT id, surname, name, concat_pharmacy(pharmacy_id) AS pharmacy FROM employee;