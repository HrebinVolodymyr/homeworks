
-- 1. Забезпечити параметризовану вставку нових значень у таблицю Співробітники.
DROP PROCEDURE IF EXISTS add_employee;

DELIMITER //
CREATE PROCEDURE add_employee(
IN surname VARCHAR(30),
IN name CHAR(30),
IN midle_name VARCHAR(30),
IN identity_number CHAR(10),
IN passport CHAR(10),
IN experience DECIMAL(10,1),
IN birthday DATE,
IN post VARCHAR(15),
IN pharmacy_id INT(11))
BEGIN
INSERT INTO `storedpr_db`.`employee` (`surname`, `name`, `midle_name`, `identity_number`, `passport`, `experience`, `birthday`, `post`, `pharmacy_id`) 
VALUES (surname, name, midle_name, identity_number, passport, experience, birthday, post, pharmacy_id);
END //
DELIMITER ;

CALL add_employee('tt', 'tt', 'tt', 'rrr', 'fff', 5, null, 'q', 5);