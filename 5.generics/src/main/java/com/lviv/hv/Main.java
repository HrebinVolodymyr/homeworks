package com.lviv.hv;

import com.lviv.hv.entity.AmdDroid;
import com.lviv.hv.entity.Bot;
import com.lviv.hv.entity.Droid;
import com.lviv.hv.entity.IntelDroid;
import java.util.ArrayList;
import java.util.List;

public class Main {

  public static void main(String... args) {

    AmdDroid amdDroidFirst = new AmdDroid(new Bot(), 1);
    AmdDroid amdDroidSecond = new AmdDroid(new Bot(), 2);
    IntelDroid intelDroidFirst = new IntelDroid(new Bot(), 3);
    IntelDroid intelDroidSecond = new IntelDroid(new Bot(), 4);

    List<AmdDroid> amdDroidList = new ArrayList<>();
    amdDroidList.add(amdDroidFirst);
    amdDroidList.add(amdDroidSecond);
    List<IntelDroid> intelDroidList = new ArrayList<>();
    intelDroidList.add(intelDroidFirst);
    intelDroidList.add(intelDroidSecond);

    Spaceship<Droid> spaceship = new Spaceship<>();

    spaceship.addDroid(amdDroidFirst);
    spaceship.addDroid(amdDroidSecond);
    spaceship.addDroid(intelDroidFirst);
    spaceship.addDroid(intelDroidSecond);
    //spaceship.addDroid("Hello");
    spaceship.addAllDroids(amdDroidList);
    spaceship.addAllDroids(intelDroidList);


    List<Integer> list = new ArrayList<>();
    list.add(1);
    list.add(2);
    list.add(3);
    list.add(Integer.valueOf("hello"));


  }
}
