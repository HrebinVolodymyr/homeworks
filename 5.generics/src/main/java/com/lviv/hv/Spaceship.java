package com.lviv.hv;

import com.lviv.hv.entity.Droid;
import java.util.ArrayList;
import java.util.List;

public class Spaceship<T extends Droid> {

  private ArrayList<T> droids = new ArrayList<>();

  public void addDroid(T droid) {
    droids.add(droid);
  }

  public void addAllDroids(List<? extends T> droids) {
    this.droids.addAll(droids);
  }

  public List<T> getAllDroids() {
    return droids;
  }

}
