package com.lviv.hv.entity;

import com.lviv.hv.entity.User;
import java.io.IOException;
//import java.nio.charset.StandardCharsets;
//import java.nio.file.Files;
//import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Bot extends User {

  public Bot() {
    super(getRandomName());
  }

  private static String getRandomName() {
    Random rnd = new Random();
    List<String> listOfNames = new ArrayList<String>();
    listOfNames.add("Douglas");
    listOfNames.add("Larry");
    listOfNames.add("Alfonzo");
    listOfNames.add("Santo");
    listOfNames.add("Wilford");
    listOfNames.add("Roberto");
    listOfNames.add("Xavier");
    listOfNames.add("Lloyd");
    listOfNames.add("Wilber");
    listOfNames.add("Benny");
    listOfNames.add("Clint");
    listOfNames.add("Doug");
    listOfNames.add("Oliver");
    listOfNames.add("Tyrell");
    listOfNames.add("Weston");
    int randomIndex = rnd.nextInt(14);
    return listOfNames.get(randomIndex);
  }

}
