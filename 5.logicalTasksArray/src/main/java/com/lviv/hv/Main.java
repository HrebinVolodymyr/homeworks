package com.lviv.hv;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class Main {

  public static void main(String... args) {

    int[] firstArray = new int[20];
    int[] secondArray = new int[20];

    for (int i = 0; i < 20; i++) {
      firstArray[i] = randInt(0, 50);
      secondArray[i] = randInt(10, 60);
    }

    int[] a3 = {0, 5, 7, 10, -6, 7, 11, 10, 15, 4};
    int[] a4 = {23, 8, 1, 45, 5, 10, 15, 1, 3, 20};
    int[] a5 = {1, 3, 2, 5, 1, 3, 1, 6, 5, 5, 2};

    System.out.println(Arrays.toString(getUniqueElements(a3, a4)));
    System.out.println(Arrays.toString(getUniqueElements(a4, a3)));
    System.out.println(Arrays.toString(getUniqueElementsFromTwoArrays(a3, a4)));
    System.out.println(Arrays.toString(getDuplicateElementsFromTwoArrays(a3, a4)));
    System.out.println(Arrays.toString(getLessThanTwoElements(a5)));

  }

  public static int[] getUniqueElementsFromTwoArrays(int[] first, int[] second) {
    int[] firstArrayUnique = first;
    int[] secondArrayUnique = second;
    int[] firstSecondUniqueElements = getUniqueElements(firstArrayUnique, secondArrayUnique);
    int[] secondFirstUniqueElements = getUniqueElements(secondArrayUnique, firstArrayUnique);
    return mergeArrays(firstSecondUniqueElements, secondFirstUniqueElements);
  }

  public static int[] getDuplicateElementsFromTwoArrays(int[] first, int[] second) {
    int[] uniqueElementsFromTwoArrays = getUniqueElementsFromTwoArrays(first, second);
    int[] allElementsFromTwoArrays = mergeArrays(first, second);
    int[] duplicateElementsFromTwoArrays = getUniqueElements(allElementsFromTwoArrays,
        uniqueElementsFromTwoArrays);
    return duplicateElementsFromTwoArrays;

  }

  public static int[] getUniqueElements(int[] firstArray, int[] secondArray) {
    int[] tmp = new int[firstArray.length];
    int tmpIndex = 0;
    for (int i = 0; i < firstArray.length; i++) {
      boolean unique = true;
      for (int j = 0; j < secondArray.length; j++) {
        if (firstArray[i] == secondArray[j]) {
          unique = false;
          break;
        }
      }
      if (unique) {
        tmp[tmpIndex] = firstArray[i];
        tmpIndex++;
      }
    }
    int[] result = new int[tmpIndex];
    System.arraycopy(tmp, 0, result, 0, tmpIndex);
    return result;
  }

  public static int[] mergeArrays(int[] firstArray, int[] secondArray) {
    int[] result = new int[firstArray.length + secondArray.length];
    System.arraycopy(firstArray, 0, result, 0, firstArray.length);
    System.arraycopy(secondArray, 0, result, firstArray.length, secondArray.length);
    return result;
  }

  public static int randInt(int min, int max) {
    return ThreadLocalRandom.current().nextInt(min, max + 1);
  }

  public static int[] removeDuplicates(int[] array) {
    int[] tmp = new int[array.length];
    int tmpNextIndex = 0;
    for (int index = 0; index < array.length; index++) {
      if (!hasElement(tmp, array[index], tmpNextIndex - 1)) {
        tmp[tmpNextIndex] = array[index];
        tmpNextIndex++;
      }
    }
    int[] result = new int[tmpNextIndex];
    System.arraycopy(tmp, 0, result, 0, tmpNextIndex);
    return result;
  }

  public static boolean hasElement(int[] array, int element, int toIndex) {
    for (int index = 0; index <= toIndex; index++) {
      if (array[index] == element) {
        return true;
      }
    }
    return false;
  }

  //----------------------------------------------------------------------------

  public static int[] getLessThanTwoElements(int[] array) {
    int[] tmp = new int[array.length];
    int tmpIndex = 0;
    for (int index = 0; index < array.length; index++) {
      int count = 0;
      for (int j = 0; j < array.length; j++) {
        if (array[index] == array[j]) {
          count++;
        }
      }
      if (count <= 2) {
        tmp[tmpIndex] = array[index];
        tmpIndex++;
      }
    }
    int[] result = new int[tmpIndex];
    System.arraycopy(tmp, 0, result, 0, tmpIndex);
    return result;
  }

  //----------------------------------------------------------------------------

}
