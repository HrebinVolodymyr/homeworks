<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div align="center">
    <a href="<c:url value="/"/>">HOME</a>
</div>
<div align="center">
    <table border="1" cellpadding="5">
        <caption><h2 align="center"><a href="<c:url value="/worksOn"/>">Table: Works_on</a></h2></caption>
        <tr>
            <th>empNo</th>
            <th>projectNo</th>
            <th>job</th>
            <th>date</th>
            <th>Delete</th>
            <th>Update</th>
        </tr>
        <c:forEach var="workEntity" items="${table}">
            <tr>
                <td><c:out value="${workEntity.pk.empNo}"/></td>
                <td><c:out value="${workEntity.pk.projectNo}"/></td>
                <td><c:out value="${workEntity.job}"/></td>
                <td><c:out value="${workEntity.date}"/></td>
                <td>
                    <a href="<c:url value="/worksOn/delete/${workEntity.pk.empNo}/${workEntity.pk.projectNo}"/>">DELETE</a>
                </td>
                <td><a href="<c:url value="/worksOn/edit/${workEntity.pk.empNo}/${workEntity.pk.projectNo}"/>">UPDATE</a></td>
            </tr>
        </c:forEach>
        <tr>
            <td colspan="6"><a href="<c:url value="/worksOn/add"/>">ADD</a></td>
        </tr>
        <tr>
            <td colspan="6">
                <form action="/worksOnFindById">
                    <input type="text" class="search-text" name="id"
                           value="${searchString}" placeholder="Find Work by ID"/>
                    <input type="submit" class="button" value="Find"/>
                </form>
            </td>
        </tr>
    </table>
</div>
<h4 align="center"><a href="<c:url value="/tables"/>">Select all table</a><br></h4>
<div align="center">
    ${error}
</div>
