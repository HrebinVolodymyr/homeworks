<%@page pageEncoding="UTF-8" %>
<%@page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">

<fmt:setBundle basename="/message/message"/>

<html>
<head>
    <title>Title</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
<div align="center" >
    <a href="<c:url value="/"/>">HOME</a>
</div>
<div align="center">
    <div id="content" align="right" style="width: 250px;">
        <h2 align="center">Add employee</h2>
        <form action="add" method="post">

            <label>ID</label>
            <input type="text" name="id">
            <br><br>

            <label>First Name</label>
            <input type="text" name="empFirstName">
            <br><br>

            <label>Last Name</label>
            <input type="text" name="epmLastName">
            <br><br>

            <label>deptNo</label>
            <input type="text" name="deptNo">
            <br><br>

            <input type="submit" value="Submit">
        </form>
    </div>
</div>

<br>
<div align="center">
    ${error}
</div>
</body>
</html>