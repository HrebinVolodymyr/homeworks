<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div align="center">
    <a href="<c:url value="/"/>">HOME</a>
</div>
<div align="center">
    <table border="1" cellpadding="5">
        <caption><h2 align="center"><a href="<c:url value="/department"/>">Table: Department</a></h2></caption>
        <tr>
            <th>deptNo</th>
            <th>deptName</th>
            <th>location</th>
            <th>Delete</th>
            <th>Update</th>
        </tr>
        <c:forEach var="departmentEntity" items="${table}">
            <tr>
                <td><c:out value="${departmentEntity.deptNo}"/></td>
                <td><c:out value="${departmentEntity.deptName}"/></td>
                <td><c:out value="${departmentEntity.location}"/></td>
                <td><a href="<c:url value="/department/delete/${departmentEntity.deptNo}"/>">DELETE</a></td>
                <td><a href="<c:url value="/department/edit/${departmentEntity.deptNo}"/>">UPDATE</a></td>
            </tr>
        </c:forEach>
        <tr>
            <td colspan="5"><a href="<c:url value="/department/add"/>">ADD</a></td>
        </tr>
        <tr>
            <td colspan="5">
                Delete from Department and move all employees to another department<br>
                <form action="/deleteDepartmentMoveEmployees">
                    <input type="text"  name="idDeleted" placeholder="idDeleted"/>
                    <input type="text"  name="idMoveTo" placeholder="idMoveTo"/>
                    <input type="submit" class="button" value="Delete and move"/>
                </form>
           </td>
        </tr>
        <tr>
            <td colspan="5">
                <form action="/departmentFindById">
                    <input type="text" class="search-text" name="id"
                           value="${searchString}" placeholder="Find Department by ID"/>
                    <input type="submit" class="button" value="Find"/>
                </form>
            </td>
        </tr>
    </table>
</div>
<h4 align="center"><a href="<c:url value="/tables"/>">Select all table</a><br></h4>
<div align="center">
    ${error}
</div>