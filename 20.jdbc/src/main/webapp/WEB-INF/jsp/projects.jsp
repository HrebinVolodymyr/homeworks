<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div align="center">
    <a href="<c:url value="/"/>">HOME</a>
</div>
<div align="center">
    <table border="1" cellpadding="5">
        <caption><h2 align="center"><a href="<c:url value="/project"/>">Table: Project</a></h2></caption>
        <tr>
            <th>projectNo</th>
            <th>projectName</th>
            <th>budget</th>
            <th>Delete</th>
            <th>Update</th>
        </tr>
        <c:forEach var="projectEntity" items="${table}">
            <tr>
                <td><c:out value="${projectEntity.projectNo}"/></td>
                <td><c:out value="${projectEntity.projectName}"/></td>
                <td><c:out value="${projectEntity.budget}"/></td>
                <td><a href="<c:url value="/project/delete/${projectEntity.projectNo}"/>">DELETE</a></td>
                <td><a href="<c:url value="/project/edit/${projectEntity.projectNo}"/>">UPDATE</a></td>
            </tr>
        </c:forEach>
        <tr>
            <td colspan="5"><a href="<c:url value="/project/add"/>">ADD</a></td>
        </tr>
        <tr>
            <td colspan="5">
                <form action="/projectFindById">
                    <input type="text" class="search-text" name="id"
                           value="${searchString}" placeholder="Find Project by ID"/>
                    <input type="submit" class="button" value="Find"/>
                </form>
            </td>
        </tr>
    </table>
</div>
<h4 align="center"><a href="<c:url value="/tables"/>">Select all table</a><br></h4>
<div align="center">
    ${error}
</div>