package com.lviv.hv.controller;

import com.lviv.hv.model.ProjectEntity;
import com.lviv.hv.service.ProjectService;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ProjectController {

  @RequestMapping(value = "/project", method = RequestMethod.GET)
  public String showProjects(ModelMap model) {
    ProjectService projectService = new ProjectService();
    try {
      model.addAttribute("table", projectService.findAll());
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return "projects";
  }

  @RequestMapping(value = "/projectFindById", method = RequestMethod.GET)
  public String findById(
      @RequestParam("id") String id,
      ModelMap model) {
    ProjectService projectService = new ProjectService();
    try {
      if (id != null) {
        List<ProjectEntity> list = new ArrayList<ProjectEntity>();
        list.add(projectService.findById(id));
        model.addAttribute("table", list);
        return "projects";
      }
      model.addAttribute("table", projectService.findAll());
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return "projects";
  }

  @RequestMapping(value = "/project/delete/{id}", method = RequestMethod.GET)
  public String deleteProject(@PathVariable("id") String projectId, ModelMap model) {
    ProjectService projectService = new ProjectService();
    try {
      projectService.delete(projectId);
      model.addAttribute("table", projectService.findAll());
    } catch (SQLException e) {
      try {
        model.addAttribute("table", projectService.findAll());
        model.addAttribute("error", "ERROR: " + e.getMessage());
      } catch (SQLException s) {
        s.printStackTrace();
      }
    }
    return "projects";
  }

  @RequestMapping(value = "/project/add", method = RequestMethod.GET)
  public String add(ModelMap model) {
    return "addProject";
  }

  @RequestMapping(value = "/project/add", method = RequestMethod.POST)
  public String addProject(
      @RequestParam("projectNo") String projectNo,
      @RequestParam("projectName") String projectName,
      @RequestParam("budget") Integer budget,
      ModelMap model) {
    ProjectEntity entity = new ProjectEntity(projectNo, projectName, budget);
    ProjectService projectService = new ProjectService();
    try {
      int count = projectService.create(entity);
    } catch (SQLException e) {
      model.addAttribute("error", "ERROR: " + e.getMessage());
      return "addProject";
    }
    return "redirect:/project";
  }

  @RequestMapping(value = "/project/edit/{id}", method = RequestMethod.GET)
  public String edit(@PathVariable("id") String projectId, ModelMap model) {
    ProjectService projectService = new ProjectService();
    try {
      model.addAttribute("entity", projectService.findById(projectId));
    } catch (SQLException e) {
      model.addAttribute("error", "ERROR: " + e.getMessage());
      return "projects";
    }
    return "editProject";
  }

  @RequestMapping(value = "/project/edit/update", method = RequestMethod.POST)
  public String updateProject(
      @RequestParam("projectNo") String projectNo,
      @RequestParam("projectName") String projectName,
      @RequestParam("budget") Integer budget,
      ModelMap model) {
    ProjectEntity entity = new ProjectEntity(projectNo, projectName, budget);
    ProjectService projectService = new ProjectService();
    try {
      int count = projectService.update(entity);
    } catch (SQLException e) {
      model.addAttribute("error", "ERROR: " + e.getMessage());
      return "redirect:/project";
    }
    return "redirect:/project";
  }


}
