package com.lviv.hv.controller;

import com.lviv.hv.model.DepartmentEntity;
import com.lviv.hv.service.DepartmentService;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class DepartmentController {

  @RequestMapping(value = "/department", method = RequestMethod.GET)
  public String showAllDepartment(ModelMap model) {
    DepartmentService departmentService = new DepartmentService();
    try {
      model.addAttribute("table", departmentService.findAll());
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return "departments";
  }

  @RequestMapping(value = "/departmentFindById", method = RequestMethod.GET)
  public String findById(
      @RequestParam("id") String id,
      ModelMap model) {
    DepartmentService departmentService = new DepartmentService();
    try {
      if (id != null) {
        List<DepartmentEntity> list = new ArrayList<DepartmentEntity>();
        list.add(departmentService.findById(id));
        model.addAttribute("table", list);
        return "departments";
      }
      model.addAttribute("table", departmentService.findAll());
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return "departments";
  }

  @RequestMapping(value = "/deleteDepartmentMoveEmployees", method = RequestMethod.GET)
  public String deleteDepartmentMoveEmployees(
      @RequestParam("idDeleted") String idDeleted,
      @RequestParam("idMoveTo") String idMoveTo,
      ModelMap model) {
    DepartmentService departmentService = new DepartmentService();
    try {
      int count = departmentService.deleteWithMoveOfEmployees(idDeleted, idMoveTo);
      model.addAttribute("table", departmentService.findAll());
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return "departments";
  }

  @RequestMapping(value = "/department/delete/{id}", method = RequestMethod.GET)
  public String deleteDepartment(@PathVariable("id") String deptNo, ModelMap model) {
    DepartmentService departmentService = new DepartmentService();
    try {
      departmentService.delete(deptNo);
      model.addAttribute("table", departmentService.findAll());
    } catch (SQLException e) {
      try {
        model.addAttribute("table", departmentService.findAll());
        model.addAttribute("error", "ERROR: " + e.getMessage());
      } catch (SQLException s) {
        s.printStackTrace();
      }
    }
    return "departments";
  }

  @RequestMapping(value = "/department/add", method = RequestMethod.GET)
  public String add(ModelMap model) {
    return "addDepartment";
  }

  @RequestMapping(value = "/department/add", method = RequestMethod.POST)
  public String addDepartment(
      @RequestParam("deptNo") String deptNo,
      @RequestParam("deptName") String deptName,
      @RequestParam("location") String location,
      ModelMap model) {
    DepartmentEntity entity = new DepartmentEntity(deptNo, deptName, location);
    DepartmentService departmentService = new DepartmentService();
    try {
      int count = departmentService.create(entity);
    } catch (SQLException e) {
      model.addAttribute("error", "ERROR: " + e.getMessage());
      return "addDepartment";
    }
    return "redirect:/department";
  }

  @RequestMapping(value = "/department/edit/{id}", method = RequestMethod.GET)
  public String edit(@PathVariable("id") String deptNo, ModelMap model) {
    DepartmentService departmentService = new DepartmentService();
    try {
      model.addAttribute("entity", departmentService.findById(deptNo));
    } catch (SQLException e) {
      model.addAttribute("error", "ERROR: " + e.getMessage());
      return "editDepartment";
    }
    return "editDepartment";
  }

  @RequestMapping(value = "/department/edit/update", method = RequestMethod.POST)
  public String editDepartment(
      @RequestParam("deptNo") String deptNo,
      @RequestParam("deptName") String deptName,
      @RequestParam("location") String location,
      ModelMap model) {
    DepartmentEntity entity = new DepartmentEntity(deptNo, deptName, location);
    DepartmentService departmentService = new DepartmentService();
    try {
      int count = departmentService.update(entity);
    } catch (SQLException e) {
      model.addAttribute("error", "ERROR: " + e.getMessage());
      return "redirect:/department";
    }
    return "redirect:/department";
  }
}
