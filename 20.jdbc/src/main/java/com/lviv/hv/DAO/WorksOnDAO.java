package com.lviv.hv.DAO;

import com.lviv.hv.model.PK_WorksOn;
import com.lviv.hv.model.WorksOnEntity;

public interface WorksOnDAO extends GeneralDAO<WorksOnEntity, PK_WorksOn> {
}
