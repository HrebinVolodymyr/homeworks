package com.lviv.hv.DAO;

import com.lviv.hv.model.DepartmentEntity;

public interface DepartmentDAO extends GeneralDAO<DepartmentEntity, String> {
}


