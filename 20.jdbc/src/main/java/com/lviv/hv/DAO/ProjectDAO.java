package com.lviv.hv.DAO;

import com.lviv.hv.model.ProjectEntity;

public interface ProjectDAO extends GeneralDAO<ProjectEntity, String> {
}
