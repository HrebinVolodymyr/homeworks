package com.lviv.hv.service;

import com.lviv.hv.DAO.implementation.WorksOnDaoImpl;
import com.lviv.hv.model.PK_WorksOn;
import com.lviv.hv.model.WorksOnEntity;
import java.sql.SQLException;
import java.util.List;

public class WorkOnService {
    public List<WorksOnEntity> findAll() throws SQLException {
        return new WorksOnDaoImpl().findAll();
    }

    public int create(WorksOnEntity entity) throws SQLException{
        return new WorksOnDaoImpl().create(entity);
    }

    public int update(WorksOnEntity entity) throws SQLException{
        return new WorksOnDaoImpl().update(entity);
    }

    public int delete(PK_WorksOn pk) throws SQLException{
        return new WorksOnDaoImpl().delete(pk);
    }
}
