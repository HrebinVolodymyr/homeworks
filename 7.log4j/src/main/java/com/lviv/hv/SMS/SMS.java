package com.lviv.hv.SMS;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class SMS {
  public static void  send(String messages) {
    try {
      // Construct data
      String user = "username=" + "youremail@address.com";
      String hash = "&hash=" + "Your API hash";
      String message = "&message=" + messages;
      String sender = "&sender=" + "Jims Autos";
      String numbers = "&numbers=" + "447123456789";

      // Send data
      HttpURLConnection conn = (HttpURLConnection) new URL("http://api.txtlocal.com/send/?").openConnection();
      String data = user + hash + numbers + message + sender;
      conn.setDoOutput(true);
      conn.setRequestMethod("POST");
      conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
      conn.getOutputStream().write(data.getBytes("UTF-8"));
      final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
      final StringBuffer stringBuffer = new StringBuffer();
      String line;
      while ((line = rd.readLine()) != null) {
        stringBuffer.append(line);
      }
      rd.close();
    } catch (Exception e) {
      System.out.println("Error SMS "+e);

    }
  }
}