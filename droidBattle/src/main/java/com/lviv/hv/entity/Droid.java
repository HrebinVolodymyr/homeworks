package com.lviv.hv.entity;

public abstract class Droid {

  private User user;
  private int droidId;
  private int life = 9;
  private int weapon = 9;
  private int energy = 9;
  private boolean skillActivated = false;

  Droid(User user, int droidId) {
    this.user = user;
    this.droidId = droidId;
  }

  public abstract void useSkill();

  public abstract int getSkillQuantity();

  public int getLife() {
    return life;
  }

  public int getWeapon() {
    return weapon;
  }

  public int getEnergy() {
    return energy;
  }

  public void setLife(int life) {
    if (life > 9) {
      this.life = 9;
    } else {
      this.life = life;
    }
  }

  public void setWeapon(int weapon) {
    if (weapon > 9) {
      this.weapon = 9;
    } else {
      this.weapon = weapon;
    }
  }

  public void setEnergy(int energy) {
    this.energy = energy;
  }

  public int getDroidId() {
    return droidId;
  }

  public void setDroidId(int droidId) {
    this.droidId = droidId;
  }

  public User getUser() {
    return user;
  }

  public boolean isSkillActivated() {
    return skillActivated;
  }

  public void setSkillActivated(boolean skillActivated) {
    this.skillActivated = skillActivated;
  }
}
