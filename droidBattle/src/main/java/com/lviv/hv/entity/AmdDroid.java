package com.lviv.hv.entity;

public class AmdDroid extends Droid {

  private int repair = 9;

  public AmdDroid(User user, int droidId) {
    super(user, droidId);
  }

  @Override
  public void useSkill() {
    repair = repair - 3;
    setLife(getLife() + 3);
  }

  @Override
  public int getSkillQuantity() {
    return repair;
  }

  public int getRepair() {
    return repair;
  }

  public void setRepair(int repair) {
    this.repair = repair;
  }

  @Override
  public String toString() {
    return "AmdDroid";
  }

}
