package com.lviv.hv.model;

import com.lviv.hv.entity.User;
import com.lviv.hv.view.LogicLevelView;


public class LogicLevelModel extends BaseModel {

  private LogicLevelView logicLevelView = new LogicLevelView();
  private int secreteCode = 845;
  private int attempt = 5;  //number of yours available attempts

  public LogicLevelModel(User user) {
    super(user);
  }

  @Override
  public void doAction(int[] action) {
    int answer = action[0];

    logicLevelView.draw(getUser().getName() + " write your answer and press ENTER.");
    if (attempt != 0) {
      if (answer == secreteCode) {
        logicLevelView.draw("C O D E    A C C E P T E D !                    Y O U   W O N  ! ! !");
        setLevelComplete(true);
      } else if (answer != secreteCode) {
        logicLevelView
            .draw("C O D E    I S    W R O N G!   T R Y    A G A I N  ! ! !  ATTEMPTS: " + attempt);
        attempt--;
      }
    } else {
      throw new ModelException("Y O U    L O S T!  To continue, press Enter");
    }

  }

}
