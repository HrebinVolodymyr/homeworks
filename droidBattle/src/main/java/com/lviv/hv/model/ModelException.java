package com.lviv.hv.model;

public class ModelException extends RuntimeException {

  ModelException(String message) {
    super(message);

  }

  ModelException(String message, Throwable cause) {
    super(message, cause);

  }


}
