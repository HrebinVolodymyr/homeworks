package com.lviv.hv.model;

import com.lviv.hv.entity.User;
import com.lviv.hv.view.PreviewView;

public class PreviewModel extends BaseModel {

  public PreviewModel(User user) {
    super(user);
  }

  @Override
  public void doAction(int[] action) {
    User user = getUser();
    String userName;
    userName = user.getName();

    PreviewView greeting = new PreviewView();
    greeting.draw("Hello and welcome dear " + userName +
        ", click \"ENTER\" to continue!");

    setLevelComplete(true);
  }
}
