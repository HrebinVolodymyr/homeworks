package com.lviv.hv.model;

import com.lviv.hv.entity.AmdDroid;
import com.lviv.hv.entity.Droid;
import com.lviv.hv.entity.IntelDroid;
import com.lviv.hv.entity.User;
import java.util.Random;

public abstract class BaseModel {

  private User user;
  private boolean levelComplete = false;

  BaseModel(User user) {
    this.user = user;
  }

  public abstract void doAction(int[] action);

  public boolean isLevelComplete() {
    return levelComplete;
  }

  protected void setLevelComplete(boolean levelComplete) {
    this.levelComplete = levelComplete;
  }

  protected User getUser() {
    return user;
  }

  protected int randInt(int min, int max) {
    Random rand = new Random();
    int randomNum = rand.nextInt((max - min) + 1) + min;
    return randomNum;
  }

  protected Droid getNewDroid(User user, int id) {
    if (randInt(1, 2) == 1) {
      return new IntelDroid(user, id);
    } else {
      return new AmdDroid(user, id);
    }
  }

  public int[] getRandomAction(int number, int rangeStart, int rangeEnd) {
    int[] action = new int[number];
    Random rnd = new Random();
    for (int i = 0; i < action.length - 1; i++) {
      action[i] = rnd.nextInt((rangeEnd - rangeStart) + 1) + rangeStart;
      if (i == 1) {
        action[i] = rnd.nextInt(2) + 1;

      }
    }
    return action;
  }
}
