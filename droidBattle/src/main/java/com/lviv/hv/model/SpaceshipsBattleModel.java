package com.lviv.hv.model;

import com.lviv.hv.entity.AmdDroid;
import com.lviv.hv.entity.Bot;
import com.lviv.hv.entity.Droid;
import com.lviv.hv.entity.IntelDroid;
import com.lviv.hv.entity.User;
import com.lviv.hv.view.SpaceshipsBattleView;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SpaceshipsBattleModel extends BaseModel {

  private List<Droid> friendlyTeam = new ArrayList<Droid>();
  private List<Droid> enemyTeam = new ArrayList<Droid>();
  private SpaceshipsBattleView spaceshipsBattleView = new SpaceshipsBattleView();

  public SpaceshipsBattleModel(User user) {
    super(user);
    friendlyTeam.add(getNewDroid(getUser(), 6));
    for (int i = 4; i <= 5; i++) {
      friendlyTeam.add(getNewDroid(new Bot(), i));
    }
    for (int i = 1; i <= 3; i++) {
      enemyTeam.add(getNewDroid(new Bot(), i));
    }
  }

  @Override
  public void doAction(int[] action) {
    if (action.length == 1) {
      int tmp = action[0];
      action = new int[2];
      action[0] = tmp;
      action[1] = 1;
    }
    fight(getDroidById(friendlyTeam, 6), enemyTeam, action);
    spaceshipsBattleView.draw(friendlyTeam, enemyTeam);

    for (int droidId = 4; droidId <= 5; droidId++) {
      Droid droid = getDroidById(friendlyTeam, droidId);
      if (droid != null) {
        try {
          Thread.sleep(500);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        int[] actions = new int[2];
        actions[0] = 1;
        actions[1] = getRandomId(enemyTeam);
        fight(droid, enemyTeam, actions);
        spaceshipsBattleView.draw(friendlyTeam, enemyTeam);
      }
    }
    for (int droidId = 1; droidId <= 3; droidId++) {
      Droid droid = getDroidById(enemyTeam, droidId);

      if (droid != null) {
        try {
          Thread.sleep(500);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        int[] actions = new int[2];
        actions[0] = 1;
        actions[1] = getRandomId(friendlyTeam);
        fight(droid, friendlyTeam, actions);
        spaceshipsBattleView.draw(friendlyTeam, enemyTeam);
      }
    }
    if (enemyTeam.size() == 0) {
      setLevelComplete(true);
    }
    if (friendlyTeam.size() == 0) {
      throw new ModelException("Y O U   L O S T !   To continue, press Enter");
    }
    if (getDroidById(friendlyTeam, 6) == null) {
      throw new ModelException("Y O U   L O S T !   To continue, press Enter");
    }
    if ((friendlyTeam.size() == 1) && getDroidById(friendlyTeam, 6).getWeapon() == 0) {
      throw new ModelException("Y O U   L O S T !   To continue, press Enter");
    }
  }

  protected Droid getDroidById(List<Droid> droids, int id) {
    for (Droid droid : droids) {
      if (id == droid.getDroidId()) {
        return droid;
      }
    }
    return null;
  }

  protected void fight(Droid attackingDroid, List<Droid> enemyTeam, int[] action) {

    switch (action[0]) {
      case 1:
        Droid droid = getDroidById(enemyTeam, action[1]);

        if ((droid != null) && (attackingDroid.getEnergy() > 1) && (attackingDroid.getWeapon()
            <= 5)) {
          attackingDroid.setWeapon(attackingDroid.getWeapon() + 3);
          attackingDroid.setEnergy(attackingDroid.getEnergy() - 3);
          return;
        }

        if ((droid != null) && (attackingDroid.getEnergy() > 1) && (attackingDroid.getLife()
            <= 1)) {
          attackingDroid.setLife(attackingDroid.getLife() + 3);
          attackingDroid.setEnergy(attackingDroid.getEnergy() - 3);
          return;
        }

        if ((droid != null) && (attackingDroid.getSkillQuantity() != 0) && (attackingDroid.getLife()
            <= 4)) {
          attackingDroid.useSkill();
        }

        if ((droid != null) && (attackingDroid.getWeapon() != 0)) {
          if (droid.isSkillActivated() && (attackingDroid.getWeapon() != 0)) {
            droid.setSkillActivated(false);
            attackingDroid.setWeapon(attackingDroid.getWeapon() - 1);
          } else {
            int droidLife = droid.getLife();
            if (droidLife <= 1) {
              attackingDroid.setWeapon(attackingDroid.getWeapon() - 1);
              enemyTeam.remove(droid);
            } else {
              attackingDroid.setWeapon(attackingDroid.getWeapon() - 1);
              if (attackingDroid.getUser().getName().equals("Volodymyr")) {
                droid.setLife(droid.getLife() - 3);
              } else {
                droid.setLife(droid.getLife() - 1);
              }
            }
          }
        }
        break;
      case 2:
        if (attackingDroid.getSkillQuantity() != 0) {
          attackingDroid.useSkill();
        }
        break;
      case 3:
        if ((attackingDroid.getEnergy() != 0) && (action[1] == 1)) {
          attackingDroid.setLife(attackingDroid.getLife() + 3);
          attackingDroid.setEnergy(attackingDroid.getEnergy() - 3);
        }
        if ((attackingDroid.getEnergy() != 0) && (action[1] == 2)) {
          attackingDroid.setWeapon(attackingDroid.getWeapon() + 3);
          attackingDroid.setEnergy(attackingDroid.getEnergy() - 3);
        }
        break;
    }

  }

  protected int getRandomId(List<Droid> droids) {
    int[] droidsIds = new int[droids.size()];
    for (int i = 0; i < droidsIds.length; i++) {
      droidsIds[i] = (droids.get(i)).getDroidId();
    }
    if (droidsIds.length == 0) {
      return 0;
    } else {
      return droidsIds[randInt(0, droidsIds.length - 1)];
    }

  }

}
