package com.lviv.hv.model;

import com.lviv.hv.entity.AmdDroid;
import com.lviv.hv.entity.Bot;
import com.lviv.hv.entity.Droid;
import com.lviv.hv.entity.IntelDroid;
import com.lviv.hv.entity.User;
import com.lviv.hv.view.DroidBattleView;
import java.util.ArrayList;
import java.util.List;

public class DroidBattleModel extends BaseModel {

  private List<Droid> droids = new ArrayList<Droid>();
  private DroidBattleView droidBattleView = new DroidBattleView();

  public DroidBattleModel(User user) {
    super(user);
    droids.add(new AmdDroid(getUser(), 1));
    droids.add(new IntelDroid(new Bot(), 2));
  }

  @Override
  public void doAction(int[] action) {

    Droid friendlyDroid = droids.get(0);
    Droid enemyDroid = droids.get(1);

    fight(friendlyDroid, enemyDroid, action);
    droidBattleView.draw(droids);
    if (enemyDroid.getLife() <= 1) {
      setLevelComplete(true);
    }
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    action[0] = 1;
    fight(droids.get(1), droids.get(0), action);
    droidBattleView.draw(droids);

    if (friendlyDroid.getLife() == 0) {
      throw new ModelException("You lost. To continue, press Enter");
    }
  }

  protected void fight(Droid attackingDroid, Droid enemyDroid, int[] action) {

    switch (action[0]) {
      case 1:
        if ((attackingDroid.getEnergy() > 1) && (attackingDroid.getWeapon()
            <= 5)) {
          attackingDroid.setWeapon(attackingDroid.getWeapon() + 3);
          attackingDroid.setEnergy(attackingDroid.getEnergy() - 3);
          return;
        }
        if ((attackingDroid.getEnergy() > 1) && (attackingDroid.getLife()
            <= 1)) {
          attackingDroid.setLife(attackingDroid.getLife() + 3);
          attackingDroid.setEnergy(attackingDroid.getEnergy() - 3);
          return;
        }
        if ((attackingDroid.getSkillQuantity() != 0) && (attackingDroid.getLife()
            <= 4)) {
          attackingDroid.useSkill();
        }
        if ((attackingDroid.getWeapon() != 0)) {
          if (enemyDroid.isSkillActivated() && (attackingDroid.getWeapon() != 0)) {
            enemyDroid.setSkillActivated(false);
            attackingDroid.setWeapon(attackingDroid.getWeapon() - 1);
          } else {
            int droidLife = enemyDroid.getLife();
            if (droidLife <= 0) {
              attackingDroid.setWeapon(attackingDroid.getWeapon() - 1);
            } else {
              attackingDroid.setWeapon(attackingDroid.getWeapon() - 1);
              if (attackingDroid.getUser().getName().equals("Volodymyr")) {
                enemyDroid.setLife(enemyDroid.getLife() - 2);
              } else {
                enemyDroid.setLife(enemyDroid.getLife() - 1);
              }
            }
          }
        }
        break;
      case 2:
        if (attackingDroid.getSkillQuantity() != 0) {
          attackingDroid.useSkill();
        }
        break;
      case 3:
        if ((attackingDroid.getEnergy() != 0) && (action[1] == 1)) {
          attackingDroid.setLife(attackingDroid.getLife() + 3);
          attackingDroid.setEnergy(attackingDroid.getEnergy() - 3);
        }
        if ((attackingDroid.getEnergy() != 0) && (action[1] == 2)) {
          attackingDroid.setWeapon(attackingDroid.getWeapon() + 3);
          attackingDroid.setEnergy(attackingDroid.getEnergy() - 3);
        }
        break;
      default:
        break;
    }
  }

}

