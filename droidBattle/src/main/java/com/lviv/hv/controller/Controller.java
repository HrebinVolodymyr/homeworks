package com.lviv.hv.controller;

import com.lviv.hv.model.BaseModel;
import com.lviv.hv.entity.User;
import com.lviv.hv.model.DroidBattleModel;
import com.lviv.hv.model.LogicLevelModel;
import com.lviv.hv.model.ModelException;
import com.lviv.hv.model.PreviewModel;
import com.lviv.hv.model.SpaceshipsBattleModel;
import com.lviv.hv.view.BaseView;
import com.lviv.hv.view.WelcomeScreenView;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.lang.String;

public class Controller {

  private String[] initParams;

  public Controller(String[] initParams) {
    this.initParams = initParams;
  }

  public void start() {
    BaseView view = new WelcomeScreenView();
    view.draw("Enter your name and press ENTER");
    User user = new User(readLine());

    while (true) {
      List<BaseModel> levels = new ArrayList<BaseModel>();
      levels.add(new PreviewModel(user));
      levels.add(new SpaceshipsBattleModel(user));
      levels.add(new DroidBattleModel(user));
      levels.add(new LogicLevelModel(user));
      try {
        for (BaseModel level : levels) {
          int[] action = new int[2];
          action[0] = 1;
          action[1] = 1;
          while (!level.isLevelComplete()) {
            level.doAction(action);
            action = readAction();
          }
        }
      } catch (ModelException e) {
        view.draw(e.getMessage());
        readLine();
      }
    }
  }

  private int[] readAction() {
    String data = readLine();
    data = data.trim();
    if (data.equals("q")) {
      System.exit(0);
    }

    String[] items;
    int i = 0;
    int[] action;

    try {
      items = data.split(";");
      action = new int[items.length];
      for (String str : items) {
        action[i] = Integer.parseInt(str.trim());
        i++;
      }
    } catch (Exception e) {
      action = new int[2];
      action[0] = 0;
      action[1] = 0;
      return action;
    }
    return action;
  }

  private String readLine() {
    String line = null;
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    try {
      line = br.readLine();
    } catch (IOException e) {
      System.out.println("IOException");
    }
    return line;
  }
}
