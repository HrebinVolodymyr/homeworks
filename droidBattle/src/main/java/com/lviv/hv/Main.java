package com.lviv.hv;

import com.lviv.hv.controller.Controller;

public class Main {

  public static void main(String... args) {
    Controller controller = new Controller(args);
    controller.start();
  }
}
