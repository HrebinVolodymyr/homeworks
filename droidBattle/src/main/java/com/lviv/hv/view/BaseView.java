package com.lviv.hv.view;

import com.lviv.hv.entity.Droid;
import java.util.List;

public abstract class BaseView {

  public void draw(List<Droid> friendlyTeam, List<Droid> enemyTeam) {
    throw new UnsupportedOperationException();
  }

  public void draw(List<Droid> droids) {
    throw new UnsupportedOperationException();
  }

  public void draw(String message) {
    throw new UnsupportedOperationException();
  }


  String render(String[] data) {
    StringBuilder stringBuilder = new StringBuilder();
    for (String str : data) {
      stringBuilder.append(str);
    }
    return stringBuilder.toString();
  }

  String getHeader() {
    String header = "\u001B[0;40;37m\n"
        + " \u001B[42;30m**********************************************************************"
        + "**************************************************************\u001B[0m\n"
        + " \u001B[42;30m*\u001B[40;37m\u001B[130C\u001B[42;30m*\u001B[0m\n"
        + " \u001B[42;30m*\u001B[40;37m\u001B[130C\u001B[42;30m*\u001B[0m";
    return header;
  }

  String getFooter(String message) {
    String s1 = " \u001B[42;30m*\u001B[40;37m\u001B[130C\u001B[42;30m*\u001B[0m\n"
        + " \u001B[42;30m*\u001B[40;32m*****************************************"
        + "***************************************************************************"
        + "**************\u001B[42;30m*\u001B[0m\n";
    String s2 =
        " \u001B[42;30m*\u001B[40;37m   \u001B[31mMESSAGE: " + message + "\u001B[37m\u001B[" +

            (118 - message.length())

            + "C\u001B[42;30m*\u001B[0m\n";
    String s3 = " \u001B[42;30m********************************************************"
        + "****************************************************************************\u001B[0m";
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(s1).append(s2).append(s3);
    return stringBuilder.toString();
  }

  String calculateProgress(int count) {
    StringBuilder stringBuilder = new StringBuilder();
    for (int i = 0; i < count; i++) {
      stringBuilder.append("*");
    }
    return stringBuilder.toString();
  }

  String[] combineArraysHorizontally(String[] first, String[] second) {
    String[] result = new String[first.length];
    for (int i = 0; i < first.length; i++) {
      result[i] = first[i] + second[i];
    }
    return result;
  }

  String[] combineArraysVertically(String[] first, String[] second) {
    String[] result = new String[first.length + second.length];
    System.arraycopy(first, 0, result, 0, first.length);
    System.arraycopy(second, 0, result, first.length, second.length);
    return result;
  }

}
