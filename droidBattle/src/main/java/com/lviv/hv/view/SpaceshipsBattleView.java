package com.lviv.hv.view;

import com.lviv.hv.entity.AmdDroid;
import com.lviv.hv.entity.Droid;
import com.lviv.hv.entity.IntelDroid;
import java.util.List;

public class SpaceshipsBattleView extends BaseView {


  public void draw(List<Droid> friendlyTeam, List<Droid> enemyTeam) {
    String[][] playingField = new String[6][];
    for (int i = 1; i <= 3; i++) {
      Droid droid = getDroidById(friendlyTeam, 7 - i);
      if (droid == null) {
        playingField[i - 1] = getEmptySpace();
      }
      if (droid instanceof AmdDroid) {
        playingField[i - 1] = getAMDSpaceshipLeft(droid.getLife(), droid.getWeapon(),
            droid.getSkillQuantity(), droid.getEnergy(), droid.getDroidId(),
            droid.getUser().getName());
      }
      if (droid instanceof IntelDroid) {
        playingField[i - 1] = getIntelSpaceshipLeft(droid.getLife(), droid.getWeapon(),
            droid.getSkillQuantity(), droid.getEnergy(), droid.getDroidId(),
            droid.getUser().getName());
      }
    }

    for (int i = 4; i <= 6; i++) {
      Droid droid = getDroidById(enemyTeam, 7 - i);
      if (droid == null) {
        playingField[i - 1] = getEmptySpace();
      }
      if (droid instanceof AmdDroid) {
        playingField[i - 1] = getAMDSpaceshipRight(droid.getLife(), droid.getWeapon(),
            droid.getSkillQuantity(), droid.getEnergy(), droid.getDroidId(),
            droid.getUser().getName());
      }
      if (droid instanceof IntelDroid) {
        playingField[i - 1] = getIntelSpaceshipRight(droid.getLife(), droid.getWeapon(),
            droid.getSkillQuantity(), droid.getEnergy(), droid.getDroidId(),
            droid.getUser().getName());
      }
    }
    String[] sp1 = combineArraysHorizontally(playingField[0], playingField[3]);
    String[] sp2 = combineArraysHorizontally(playingField[1], playingField[4]);
    String[] sp3 = combineArraysHorizontally(playingField[2], playingField[5]);

    String[] r1 = combineArraysVertically(sp1, sp2);
    String[] r2 = combineArraysVertically(r1, sp3);
    String[] disp = combineArraysHorizontally(r2, getSpaceStation());

    String message =
        Printer.ANSI_YELLOW + " 1;. " + Printer.ANSI_RESET + "[SHOOT;SHIP NUMBER(1,2,3)] | "
            + Printer.ANSI_YELLOW + "2" + Printer.ANSI_RESET + " [USE SKILL] | "
            + Printer.ANSI_YELLOW + "3;. " + Printer.ANSI_RESET
            + "[USE ENERGY ; 1(LIFE) 2 (WEAPON)]                               ";

    System.out.println(getHeader() + "\n" + render(disp) + "\n" + getFooter(message));
  }

  public String[] getAMDSpaceshipRight(int life, int weapon, int repair, int energy, int droidId,
      String droidName) {
    String[] amdSpaceship = new String[10];
    String emptySpace = "\u001B[45C";
    amdSpaceship[0] = emptySpace;
    amdSpaceship[1] =
        "\u001B[29C" + droidId + ":" + droidName + "\u001B[" + (14 - droidName.length()) + "C";
    amdSpaceship[2] = "\u001B[40;37m\u001B[11C\u001B[31m/-------\\ \u001B[30m***"
        + "\u001B[31m /\\/-------------\\\u001B[3C";

    amdSpaceship[3] = "\u001B[40;37m\u001B[10C\u001B[31m/\u001B[32m/ "
        + "------\\\u001B[31m\\---/\u001B[32m--\u001B[37m \u001B[32mL\u001B[37m \u001B[32m"
        + this.calculateProgress(life)
        + "\u001B["
        + (10 - life)
        + "C\u001B[31m  |\u001B[32m*\u001B[1C";

    amdSpaceship[4] = "\u001B[40;37m\u001B[9C\u001B[31m/\u001B[32m/"
        + "\u001B[31m   AMD   \u001B[32m----/\u001B[31m \u001B[1;33m|\u001B[0m "
        + "\u001B[31mW\u001B[37m \u001B[31m"
        + calculateProgress(weapon)
        + "\u001B["
        + (10 - weapon)
        + "C /\u001B[32m*\u001B[2C";

    amdSpaceship[5] = "\u001B[40;37m\u001B[8C\u001B[31m/\u001B[37m "
        + "\u001B[36m--@@@--\u001B[1;33m__\u001B[0;36m--@@---\u001B[1;33m|\u001B[0m "
        + "\u001B[36mR\u001B[37m \u001B[36m"
        + calculateProgress(repair)
        + "\u001B["
        + (10 - repair)
        + "C\u001B[31m \\\u001B[32m*\u001B[2C";

    amdSpaceship[6] = "\u001B[40;37m\u001B[8C\u001B[31m\\\u001B[1;33m\\______/"
        + "\u001B[0;31m/\\\u001B[1;33m\\______|\u001B[0m \u001B[1;33mE\u001B[0m \u001B[1;33m"
        + calculateProgress(energy)
        + "\u001B["
        + (10 - energy)
        + "C\u001B[30m \u001B[0;31m |\u001B[32m*\u001B[1C";

    amdSpaceship[7] = "\u001B[40;37m\u001B[8C\u001B[30m \u001B[31m\\______/"
        + "\u001B[30m  \u001B[31m\\_/\\__________________/\u001B[3C";
    amdSpaceship[8] = emptySpace;
    amdSpaceship[9] = emptySpace;
    return amdSpaceship;
  }

  public String[] getAMDSpaceshipLeft(int life, int weapon, int repair, int energy, int droidId,
      String droidName) {
    String[] amdSpaceship = new String[10];
    String emptySpace = "\u001B[45C";
    amdSpaceship[0] = emptySpace;
    amdSpaceship[1] =
        "\u001B[4C" + droidId + ":" + droidName + "\u001B[" + (39 - droidName.length()) + "C";
    amdSpaceship[2] = " \u001B[40;37m  \u001B[31m/-------------\\/\\\u001B[37m"
        + "\u001B[5C\u001B[31m/-------\\\u001B[30m\u001B[6C\u001B[37m     \u001B[0m";

    amdSpaceship[3] = " \u001B[40;32m*\u001B[31m|\u001B[37m  "
        + "\u001B[32mL\u001B[37m \u001B[32m"
        + calculateProgress(life)
        + "\u001B["
        + (10 - life)
        + "C\u001B[37m\u001B[32m--\u001B[37m \u001B[31m\\---/\u001B[32m/------\u001B[37m "
        + "\u001B[32m\\\u001B[31m\\\u001B[30m\u001B[5C\u001B[37m     \u001B[0m";

    amdSpaceship[4] = " \u001B[40;37m \u001B[32m*\u001B[31m\\\u001B[37m "
        + "\u001B[31mW\u001B[37m \u001B[31m"
        + calculateProgress(weapon)
        + "\u001B["
        + (10 - weapon)
        + "C\u001B[37m\u001B[1;33m|\u001B[0m  \u001B[32m\\----\u001B[37m  "
        + "\u001B[31m AMD \u001B[37m  \u001B[32m\\\u001B[31m\\\u001B[30m \u001B[37m "
        + "\u001B[30m  \u001B[37m     \u001B[0m";

    amdSpaceship[5] = " \u001B[40;37m \u001B[32m*\u001B[31m/"
        + "\u001B[37m \u001B[36mR\u001B[37m \u001B[36m"
        + calculateProgress(repair)
        + "\u001B["
        + (10 - repair)
        + "C\u001B[37m\u001B[1;33m|\u001B[0;36m---@@--\u001B[37m "
        + "\u001B[1;33m__\u001B[0;36m--@@@--\u001B[37m \u001B[31m\\\u001B[30m "
        + "\u001B[37m\u001B[7C\u001B[0m";

    amdSpaceship[6] = " \u001B[40;32m*\u001B[31m|\u001B[37m  "
        + "\u001B[1;33mE\u001B[0m \u001B[1;33m"
        + calculateProgress(energy)
        + "\u001B["
        + (10 - energy)
        + "C\u001B[0m\u001B[1;33m|_______/\u001B[0;31m/\\\u001B[1;33m\\______/"
        + "\u001B[0;31m/\u001B[37m\u001B[8C\u001B[0m";

    amdSpaceship[7] = " \u001B[40;37m  \u001B[31m\\__________________/\\_/"
        + "\u001B[37m  \u001B[31m\\______/\u001B[37m\u001B[9C\u001B[0m";
    amdSpaceship[8] = emptySpace;
    amdSpaceship[9] = emptySpace;
    return amdSpaceship;
  }

  public String[] getIntelSpaceshipRight(int life, int weapon, int shield, int energy, int droidId,
      String droidName) {
    String[] intelSpaceship = new String[10];
    String emptySpace = "\u001B[45C";
    intelSpaceship[0] = emptySpace;
    intelSpaceship[1] =
        "\u001B[29C" + droidId + ":" + droidName + "\u001B[" + (14 - droidName.length()) + "C";
    intelSpaceship[2] = "\u001B[40;37m       \u001B[34m/-|--/\u001B[1;33m-------\\"
        + "\u001B[0;31m \u001B[30m***\u001B[31m \u001B[1;33m/\\/-------------\\\u001B[0;30m\u001B[2C\u001B[0m";

    intelSpaceship[3] =
        "\u001B[40;37m      \u001B[34m|\u001B[37m \u001B[34m|\u001B[37m \u001B[34m/\u001B[32m/ ------"
            + "\\\u001B[1;33m\\---/\u001B[0;32m--\u001B[37m \u001B[32mL\u001B[37m \u001B[32m"
            + calculateProgress(life)
            + "\u001B["
            + (10 - life)
            + "C\u001B[31m  \u001B[1;33m|\u001B[0;32m*\u001B[0C\u001B[0m";

    intelSpaceship[4] = "\u001B[40;37m      \u001B[34m\\\u001B[37m \u001B[34m|/"
        + "\u001B[32m/\u001B[31m \u001B[1;33m INTEL\u001B[0;31m  \u001B[32m----/\u001B[31m \u001B[1;33m|\u001B[0m \u001B[31mW\u001B[37m \u001B[31m"
        + calculateProgress(weapon)
        + "\u001B["
        + (10 - weapon)
        + "C \u001B[1;33m/\u001B[0;32m*\u001B[30m \u001B[0C\u001B[0m";

    intelSpaceship[5] = "\u001B[40;37m\u001B[7C\u001B[34m\\/\u001B[37m "
        + "\u001B[36m--@@@--\u001B[1;33m__\u001B[0;36m--@@---\u001B[1;33m|\u001B[0m \u001B[36mS\u001B[37m \u001B[36m"
        + calculateProgress(shield)
        + "\u001B["
        + (10 - shield)
        + "C\u001B[31m \u001B[1;33m\\\u001B[0;32m*\u001B[30m \u001B[0C\u001B[0m";

    intelSpaceship[6] = "\u001B[40;37m\u001B[8C\u001B[34m\\\u001B[1;33m\\______/"
        + "\u001B[0;34m/\\\u001B[1;33m\\______|\u001B[0m \u001B[1;33mE\u001B[0m \u001B[1;33m"
        + calculateProgress(energy)
        + "\u001B["
        + (10 - energy)
        + "C\u001B[30m \u001B[0;31m \u001B[1;33m|\u001B[0;32m*\u001B[0C\u001B[0m";

    intelSpaceship[7] = "\u001B[40;37m\u001B[8C\u001B[30m \u001B[34m\\______/"
        + "\u001B[30m  \u001B[34m\\_/\\\u001B[1;33m__________________/\u001B[0m  \u001B[0C\u001B[0m";
    intelSpaceship[8] = emptySpace;
    intelSpaceship[9] = emptySpace;
    return intelSpaceship;
  }

  public String[] getIntelSpaceshipLeft(int life, int weapon, int shield, int energy, int droidId,
      String droidName) {
    String[] intelSpaceship = new String[10];
    String emptySpace = "\u001B[45C";
    intelSpaceship[0] = emptySpace;
    intelSpaceship[1] =
        "\u001B[4C" + droidId + ":" + droidName + "\u001B[" + (39 - droidName.length()) + "C";
    intelSpaceship[2] = "\u001B[40;37m  \u001B[1;33m/-------------\\/\\\u001B[0m"
        + "\u001B[5C\u001B[1;33m/-------\u001B[0;34m\\--|-\\\u001B[37m       \u001B[0m";

    intelSpaceship[3] = "\u001B[40;32m*\u001B[1;33m|\u001B[0m  "
        + "\u001B[32mL\u001B[37m \u001B[32m"
        + calculateProgress(life)
        + "\u001B["
        + (10 - life)
        + "C\u001B[37m\u001B[32m--\u001B[37m \u001B[1;33m\\---/\u001B[0;32m/------\u001B[37m "
        + "\u001B[32m\\\u001B[34m\\\u001B[37m \u001B[34m|\u001B[37m \u001B[34m|\u001B[37m       "
        + "\u001B[0m";

    intelSpaceship[4] = "\u001B[40;37m \u001B[32m*\u001B[1;33m\\\u001B[0m "
        + "\u001B[31mW\u001B[37m \u001B[31m"
        + calculateProgress(weapon)
        + "\u001B["
        + (10 - weapon)
        + "C\u001B[37m\u001B[1;33m|\u001B[0m  \u001B[32m\\----\u001B[37m  \u001B[1;33mINTEL"
        + "\u001B[0m  \u001B[32m\\\u001B[34m\\|\u001B[37m \u001B[34m/\u001B[37m       "
        + "\u001B[0m";

    intelSpaceship[5] = "\u001B[40;37m \u001B[32m*\u001B[1;33m/\u001B[0m "
        + "\u001B[36mS\u001B[37m \u001B[36m"
        + calculateProgress(shield)
        + "\u001B["
        + (10 - shield)
        + "C\u001B[37m\u001B[1;33m|\u001B[0;36m---@@--\u001B[37m \u001B[1;33m__"
        + "\u001B[0;36m--@@@--\u001B[37m \u001B[34m\\/\u001B[37m\u001B[8C\u001B[0m";

    intelSpaceship[6] = "\u001B[40;32m*\u001B[1;33m|\u001B[0m  "
        + "\u001B[1;33mE\u001B[0m \u001B[1;33m"
        + calculateProgress(energy)
        + "\u001B["
        + (10 - energy)
        + "C\u001B[0m\u001B[1;33m|_______/\u001B[0;34m/\\\u001B[1;33m\\______/"
        + "\u001B[0;34m/\u001B[37m\u001B[9C\u001B[0m";

    intelSpaceship[7] = "\u001B[40;37m  \u001B[1;33m\\__________________\u001B[0;34m/"
        + "\\_/\u001B[37m  \u001B[34m\\______/\u001B[37m\u001B[10C\u001B[0m";
    intelSpaceship[8] = emptySpace;
    intelSpaceship[9] = emptySpace;
    return intelSpaceship;
  }

  private String[] getSpaceStation() {

    String spaceStation = "\u001B[42C\u001B[42;30m*\u001B[0m\n"

        + ":\u001B[42C\u001B[42;30m*\u001B[0m\n"
        + ":\u001B[42C\u001B[42;30m*\u001B[0m\n"
        + ":\u001B[25C\u001B[41;31mлллллллллллл\u001B[40;30mВ\u001B[37m   \u001B[41;31mл\u001B[42;30m*\u001B[0m\n"
        + ":\u001B[9C\u001B[30mВ\u001B[37m\u001B[13C\u001B[41;31mлл\u001B[5;1;40;30m  \u001B[0;43;34m*\u001B[5;1;40;30m   \u001B[0;43;34m*\u001B[5;1;40;30m  \u001B[0;41;31mл\u001B[5;1;40;30m \u001B[0;41;31mл\u001B[40;30mБ \u001B[37m \u001B[41;31mл\u001B[42;32m \u001B[30m*\u001B[0m\n"
        + ":\u001B[22C\u001B[41;31mл\u001B[5;1;40;30m  \u001B[33m*\u001B[30m \u001B[33m*\u001B[30m \u001B[0;42;34m*\u001B[5;1;40;33m*\u001B[30m В\u001B[0;41;31mл\u001B[5;1;40;30m  \u001B[0;41;31mл\u001B[40;30m \u001B[41;31mлл\u001B[42;32m  \u001B[30m*\u001B[0m\n"
        + ":\u001B[20C\u001B[41;31mлл\u001B[5;1;40;30m   \u001B[0;43;34m*\u001B[5;1;40;30m  \u001B[33m*\u001B[30m  \u001B[0;41;31mл\u001B[5;1;40;30m    \u001B[0;41;31mлл\u001B[42;32m   \u001B[41;31mл\u001B[42;30m*\u001B[0m\n"
        + ":\u001B[4C\u001B[41;31mАБлллллл\u001B[40;30mВ   АВВ\u001B[41;31mлллллллллллл\u001B[5;1;40;30m  \u001B[0;42;34m*\u001B[5;1;40;33m*\u001B[30m \u001B[0;41;31mл\u001B[42;32m   \u001B[41;31mл\u001B[42;32m \u001B[30m*\u001B[0m\n"
        + ":\u001B[4C\u001B[41;31mлл\u001B[42;32m       \u001B[41;31mлллл\u001B[40;30m  \u001B[41;31mл\u001B[5;1;40;30m      \u001B[0;43;34m*\u001B[5;1;40;30m   \u001B[0;41;31mл\u001B[5;1;40;30m     \u001B[0;41;31mл\u001B[42;32m \u001B[41;31mлл\u001B[42;32m  \u001B[30m*\u001B[0m\n"
        + ":\u001B[4C\u001B[41;31mл\u001B[42;32m \u001B[41;31mлллл\u001B[42;32m     \u001B[34m*\u001B[32m \u001B[41;31mллл\u001B[5;1;40;30m \u001B[33m*\u001B[30m        \u001B[0;41;31mл\u001B[5;1;40;30m     \u001B[0;41;31mлл\u001B[42;32m    \u001B[30m*\u001B[0m\n"
        + ":\u001B[4C\u001B[41;31mл\u001B[42;32m  \u001B[34m*\u001B[32m  \u001B[41;31mллл\u001B[42;32m      \u001B[41;31mл\u001B[5;1;40;30m \u001B[0;43;34m*\u001B[5;1;40;30m  \u001B[0;42;34mARM\u001B[5;1;40;30m   \u001B[0;41;31mл\u001B[5;1;40;30m \u001B[33m*\u001B[30m   \u001B[0;41;31mл\u001B[42;32m \u001B[34m*\u001B[32m  \u001B[41;31mл\u001B[42;30m*\u001B[0m\n"
        + ":\u001B[4C\u001B[30mВ\u001B[41;31mл\u001B[42;32m   \u001B[43;34m*\u001B[42;32m   \u001B[41;31mлллл\u001B[42;32m  \u001B[41;31mл\u001B[5;1;40;30m        \u001B[0;43;34m*\u001B[5;1;40;30m \u001B[0;41;31mл\u001B[5;1;40;30m \u001B[0;42;34m*\u001B[5;1;40;30m  \u001B[33m*\u001B[0;41;31mл\u001B[42;32m   \u001B[41;31mл\u001B[40;37m \u001B[42;30m*\u001B[0m\n"
        + ":\u001B[4C\u001B[30mБ\u001B[37m \u001B[41;31mллл\u001B[42;32m   \u001B[34m*\u001B[32m \u001B[43;34m*\u001B[42;32m  \u001B[41;31mллл\u001B[5;1;40;30m  \u001B[0;42;34m*\u001B[5;1;40;30m       \u001B[0;41;31mл\u001B[5;1;40;30m    \u001B[0;41;31mллл\u001B[42;32m \u001B[41;31mл\u001B[40;37m  \u001B[42;30m*\u001B[0m\n"
        + ":\u001B[9C\u001B[41;31mлллл\u001B[42;32m      \u001B[41;31mл\u001B[5;1;40;30m          \u001B[0;41;31mл\u001B[5;1;40;30m  \u001B[0;41;31mлл\u001B[42;32m В \u001B[41;31mлВ\u001B[40;37m  \u001B[42;30m*\u001B[0m\n"
        + ":\u001B[13C\u001B[41;31mллл\u001B[42;32m   \u001B[41;31mлллллллллл\u001B[5;1;40;30m \u001B[0;41;31mл\u001B[5;1;40;30m В\u001B[0;41;31mл\u001B[42;32m  В   \u001B[41;31mлл\u001B[42;30m*\u001B[0m\n"
        + ":\u001B[16C\u001B[41;31mлллл\u001B[32mллллл\u001B[31mлл\u001B[42;32m \u001B[41;31mл\u001B[5;1;40;30m \u001B[0;41;31mл\u001B[5;1;40;30m \u001B[0;41;31mл\u001B[42;32m \u001B[41;31mллл\u001B[42;32m  \u001B[34m*\u001B[32m  \u001B[30m*\u001B[0m\n"
        + ":\u001B[15C\u001B[41;31mлл\u001B[42;32m  В\u001B[41mллл\u001B[31mлл\u001B[42;32m   \u001B[41;31mл\u001B[5;1;40;30m \u001B[0;41;31mл\u001B[5;1;40;30m \u001B[0;41;31mл\u001B[42;32m   \u001B[41;31mлл\u001B[42;32m  \u001B[34m*\u001B[32m \u001B[30m*\u001B[0m\n"
        + ":\u001B[13C\u001B[41;31mлл\u001B[42;32m    \u001B[41;31mлллл\u001B[42;32m     \u001B[41;31mл\u001B[5;1;40;30m \u001B[0;41;31mл\u001B[5;1;40;30m \u001B[0;41;31mл\u001B[42;32m   В \u001B[41;31mлл\u001B[42;32m  \u001B[30m*\u001B[0m\n"
        + ":\u001B[11C\u001B[41;31mлл\u001B[42;32m     \u001B[41;31mлл\u001B[42;32m    \u001B[43;34m*\u001B[42;30m*\u001B[32m  \u001B[41;31mл\u001B[5;1;40;30m \u001B[0;41;31mл\u001B[5;1;40;30m \u001B[0;41;31mл\u001B[42;32m   \u001B[5;40;30m*\u001B[0;42;32m   \u001B[41;31mлл\u001B[42;30m*\u001B[0m\n"
        + ":\u001B[9C\u001B[41;31mлл\u001B[42;32m     \u001B[41;31mлл\u001B[42;32m В \u001B[30m*\u001B[32m \u001B[43;34m*\u001B[42;32m    \u001B[41;31mл\u001B[5;1;40;30m \u001B[0;41;31mл\u001B[5;1;40;30m \u001B[0;41;31mлл\u001B[42;32m  А\u001B[5;40;30m*\u001B[0;43;34m*\u001B[42;32m   \u001B[30m*\u001B[0m\n"
        + ":\u001B[7C\u001B[41;31mБлллллллл\u001B[42;32m  \u001B[30m*\u001B[32mВ\u001B[43;34m*\u001B[42;32m \u001B[30m*\u001B[32m   \u001B[41;31mлл\u001B[5;1;40;30m  \u001B[0;41;31mл\u001B[5;1;40;30m   \u001B[0;41;31mлл\u001B[42;32mБ     \u001B[30m*\u001B[0m\n"
        + ":\u001B[7C\u001B[41;31mл\u001B[5;1;40;30m \u001B[33m*\u001B[30m   \u001B[0;43;34m*\u001B[5;1;40;30m \u001B[0;41;31mл\u001B[42;32m   \u001B[43;34m*\u001B[42;30m*\u001B[32m   \u001B[41;31mлл\u001B[5;1;40;30m    \u001B[0;41;31mл\u001B[5;1;40;30m     \u001B[0;41;31mлллл\u001B[42;32m  \u001B[30m*\u001B[0m\n"
        + ":\u001B[7C\u001B[41;31mл\u001B[5;1;40;33m \u001B[30m      \u001B[0;41;31mл\u001B[42;32m \u001B[43;34m*\u001B[42;32m В  \u001B[41;31mлл\u001B[5;1;40;30m   \u001B[0;43;34m*\u001B[5;1;40;33m*\u001B[30m \u001B[0;41;31mл\u001B[5;1;40;30m  \u001B[33m*\u001B[30m  \u001B[0;41;31mл\u001B[40;37m   \u001B[41;31mлл\u001B[42;30m*\u001B[0m\n"
        + ":\u001B[7C\u001B[41;31mл\u001B[5;1;40;30m \u001B[33m*\u001B[0;42;34m*\u001B[5;40;30m*\u001B[34m*\u001B[1;33m*\u001B[30m \u001B[0;41;31mл\u001B[42;32m   \u001B[41;31mллл\u001B[5;1;40;30m      \u001B[0;42;34m*\u001B[5;1;40;30m \u001B[0;41;31mл\u001B[5;1;40;30m \u001B[0;43;34m*\u001B[5;1;40;30m  \u001B[0;41;31mлл\u001B[40;37m\u001B[5C\u001B[42;30m*\u001B[0m\n"
        + ":\u001B[7C\u001B[41;31mл\u001B[5;1;40;30m       \u001B[0;41;31mл\u001B[42;32m \u001B[41;30m \u001B[31mлл\u001B[5;1;40;30m   \u001B[0;42;34m*\u001B[5;1;40;33m*\u001B[30m  \u001B[33m*\u001B[30m  \u001B[0;41;31mл\u001B[5;1;40;30m  \u001B[0;41;31mлл\u001B[40;37m\u001B[7C\u001B[42;30m*\u001B[0m\n"
        + ":\u001B[7C\u001B[41;31mл       лл\u001B[40;30mВ\u001B[37m \u001B[41;31mл\u001B[5;1;40;30m          \u001B[0;41;31mллл\u001B[40;37m\u001B[9C\u001B[42;30m*\u001B[0m\n"
        + ":\u001B[7C\u001B[30mВВААВВВВВ\u001B[37m   \u001B[41;31mллллллллллллл\u001B[40;37m\u001B[10C\u001B[42;30m*\u001B[0m\n"
        + ":\u001B[42C\u001B[42;30m*\u001B[0m\n"
        + ":\u001B[42C\u001B[42;30m*\u001B[0m\n"
        + ":\u001B[42C\u001B[42;30m*\u001B[0m";
    return spaceStation.split(":");
  }

  private String[] getEmptySpace() {
    String[] intelSpaceship = new String[10];
    String emptySpace = "\u001B[45C";
    for (int i = 0; i < intelSpaceship.length; i++) {
      intelSpaceship[i] = emptySpace;
    }
    return intelSpaceship;
  }

  private Droid getDroidById(List<Droid> droids, int id) {
    for (Droid droid : droids) {
      if (id == droid.getDroidId()) {
        return droid;
      }
    }
    return null;
  }

}
