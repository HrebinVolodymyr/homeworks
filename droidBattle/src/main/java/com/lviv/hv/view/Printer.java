package com.lviv.hv.view;

public class Printer {

  public static final String ANSI_RESET = "\u001B[0m";
  public static final String ANSI_BLACK = "\u001B[30m";
  public static final String ANSI_RED = "\u001B[31m";
  public static final String ANSI_GREEN = "\u001B[32m";
  public static final String ANSI_YELLOW = "\u001B[33m";
  public static final String ANSI_BLUE = "\u001B[34m";
  public static final String ANSI_PURPLE = "\u001B[35m";
  public static final String ANSI_CYAN = "\u001B[36m";
  public static final String ANSI_WHITE = "\u001B[37m";
  public static final String ANSI_BLACK_BACKGROUND = "\u001B[40m";
  public static final String ANSI_RED_BACKGROUND = "\u001B[41m";
  public static final String ANSI_GREEN_BACKGROUND = "\u001B[42m";
  public static final String ANSI_YELLOW_BACKGROUND = "\u001B[43m";
  public static final String ANSI_BLUE_BACKGROUND = "\u001B[44m";
  public static final String ANSI_PURPLE_BACKGROUND = "\u001B[45m";
  public static final String ANSI_CYAN_BACKGROUND = "\u001B[46m";
  public static final String ANSI_WHITE_BACKGROUND = "\u001B[47m";

  public static void printMessage(String str) {
    String line = "  ***************************************************************** ";
    String line2 = " *                                                                 *";
    str = "MESSAGE: " + ANSI_WHITE + str + ANSI_RESET + ANSI_GREEN;
    char[] c = str.toCharArray();
    char[] s = line2.toCharArray();
    System.arraycopy(c, 0, s, s.length / 2 - c.length / 2, c.length);
    String lineX = new String(s);
    print();
    System.out.println(ANSI_GREEN + line);
    System.out.println(lineX);
    System.out.println(line + ANSI_RESET);
  }

  static void print() {
    System.out.println();
  }

}
