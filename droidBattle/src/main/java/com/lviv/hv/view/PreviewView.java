package com.lviv.hv.view;

public class PreviewView extends BaseView {

  @Override
  public void draw(String message) {
    System.out.println(getHeader() + "\n" + getBody() + "\n" + getFooter(message));
  }

  private String getBody() {
    String line =
        " \u001B[42;30m*\u001B[40;37m\u001B[54C\u001B[31m    \u001B[37m\u001B[57C\u001B[31m  \u001B[37m\u001B[13C\u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m\u001B[10C\u001B[31m*\u001B[37m  \u001B[31m*\u001B[37m   \u001B[31m*\u001B[37m  \u001B[31m*\u001B[37m\u001B[109C\u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m\u001B[5C\u001B[31m***\u001B[37m \u001B[31m*\u001B[37m \u001B[31m*\u001B[37m \u001B[31m*\u001B[37m \u001B[31m**\u001B[37m  \u001B[31m*\u001B[37m\u001B[8C\u001B[1;33mANDROMEDA\u001B[0m\u001B[93C\u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m\u001B[5C\u001B[31m**\u001B[37m\u001B[15C\u001B[31m*\u001B[37m \u001B[31m*\u001B[37m\u001B[22C\u001B[32mThe game describes events in the Andromeda galaxy where there are two  \u001B[31m  \u001B[37m\u001B[10C\u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m\u001B[5C\u001B[31m*\u001B[37m   \u001B[32m*\u001B[37m \u001B[32m***\u001B[37m \u001B[32m*\u001B[34m*\u001B[31m*\u001B[34m*\u001B[37m \u001B[31m*\u001B[37m \u001B[34m*\u001B[37m   \u001B[31m*\u001B[37m \u001B[31m*\u001B[37m\u001B[18C\u001B[32mintelligent forms of life in the civilizations of \u001B[1;33mINTEL\u001B[0;32m and \u001B[1;33mAMD\u001B[0;32m droves.\u001B[31m  \u001B[37m\u001B[10C\u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m\u001B[5C\u001B[31m**\u001B[37m \u001B[31m*\u001B[32m**\u001B[37m  \u001B[34m*\u001B[37m  \u001B[34m*\u001B[37m \u001B[1;33m*\u001B[0;32m*\u001B[37m   \u001B[31m*\u001B[37m  \u001B[34m*\u001B[31m**\u001B[37m  \u001B[31m*\u001B[37m\u001B[15C\u001B[32mBetween them, for three centuries, the war for systems in the galaxy   \u001B[37m\u001B[12C\u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m\u001B[7C\u001B[34m*\u001B[37m \u001B[31m**\u001B[37m \u001B[32m***\u001B[37m \u001B[32m*\u001B[37m \u001B[32m*\u001B[34m*\u001B[32m**\u001B[37m    \u001B[31m*\u001B[37m   \u001B[31m*\u001B[37m\u001B[16C\u001B[32mis rich in resources. In order to gain preference in the war, the\u001B[37m\u001B[18C\u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m\u001B[7C\u001B[34m*\u001B[37m    \u001B[34m*\u001B[37m\u001B[8C\u001B[34m*\u001B[32m**\u001B[37m\u001B[5C\u001B[34m*\u001B[37m   \u001B[31m*\u001B[37m\u001B[13C\u001B[32mintellectuals of INTEL on the \u001B[1;33mARM\u001B[0m \u001B[32m space station have created a \u001B[31mDNR\u001B[32m    \u001B[37m\u001B[12C\u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m\u001B[7C\u001B[34m*\u001B[37m\u001B[7C\u001B[34m*\u001B[37m\u001B[7C\u001B[1;33m*\u001B[0m   \u001B[31m*\u001B[37m  \u001B[31m*\u001B[37m\u001B[16C\u001B[32mvirus that infects droids that unites them into small republics\u001B[37m\u001B[20C\u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m\u001B[22C\u001B[32m**\u001B[34m*\u001B[37m \u001B[31m*\u001B[1;33m*\u001B[0;31m*\u001B[37m \u001B[34m*\u001B[37m \u001B[31m*\u001B[37m\u001B[13C\u001B[32m and hostilely tune in to other republics.\u001B[37m\u001B[15C\u001B[31m \u001B[37m\u001B[26C\u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m\u001B[20C\u001B[32m*\u001B[1;33m****\u001B[0;34m*\u001B[31m*\u001B[37m  \u001B[31m*\u001B[1;33m*\u001B[0m \u001B[31m*\u001B[37m\u001B[70C\u001B[31m \u001B[37m\u001B[26C\u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m\u001B[17C\u001B[1;33m***\u001B[0m    \u001B[1;33m*\u001B[0m \u001B[34m*\u001B[37m \u001B[31m*\u001B[37m \u001B[34m*\u001B[1;33m**\u001B[0m\u001B[13C\u001B[32m Due to the careless handling of the virus, he breaks out and begins to infect   \u001B[37m   \u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m\u001B[17C\u001B[1;33m**\u001B[0m   \u001B[32m*\u001B[37m \u001B[1;33m**\u001B[0;34m*\u001B[1;33m*\u001B[0m  \u001B[31m*\u001B[1;33m**\u001B[0m\u001B[13C\u001B[32m both civilizations. This led to the fact that the civilizations INTEL and AMD   \u001B[37m   \u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m\u001B[18C\u001B[1;33m**\u001B[0m   \u001B[1;33m*\u001B[0m \u001B[34m*\u001B[37m \u001B[34m*\u001B[31m*\u001B[1;33m*\u001B[0m  \u001B[32m*\u001B[1;33m*\u001B[0m\u001B[12C\u001B[32m began to split into small republics that fought against each other. In order    \u001B[37m   \u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m\u001B[19C\u001B[1;33m*\u001B[0m  \u001B[1;33m*\u001B[0m \u001B[34m*\u001B[37m \u001B[31m**\u001B[37m  \u001B[34m***\u001B[1;33m*\u001B[0m\u001B[12C\u001B[32m to prevent total destruction of yet infected droids INTEL and AMD are merged    \u001B[37m   \u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m\u001B[20C\u001B[1;33m*\u001B[0m \u001B[1;33m*\u001B[0m \u001B[1;33m*****\u001B[0m \u001B[1;33m**\u001B[0m \u001B[1;33m*\u001B[0;34m*\u001B[37m\u001B[12C\u001B[32mand sent to the AR\u001B[37m  \u001B[32m station to run an antivirus program that generates\u001B[37m\u001B[12C\u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m\u001B[23C\u001B[32m*\u001B[1;33m***\u001B[0m \u001B[31m**\u001B[37m\u001B[6C\u001B[32m*\u001B[37m\u001B[10C\u001B[32man infected droids and regains balance in the galaxy.\u001B[37m\u001B[30C\u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m\u001B[24C\u001B[31m*\u001B[37m  \u001B[31m*\u001B[37m\u001B[9C\u001B[34m*\u001B[37m \u001B[32m*\u001B[37m\u001B[90C\u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m\u001B[23C\u001B[32m*\u001B[37m  \u001B[31m*\u001B[37m  \u001B[31m*\u001B[34m*\u001B[37m  \u001B[34m*\u001B[37m    \u001B[34m*\u001B[31m*\u001B[37m \u001B[31m*\u001B[37m\u001B[88C\u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m\u001B[27C\u001B[31m*\u001B[37m\u001B[6C\u001B[1;33m*\u001B[0;34m*\u001B[37m \u001B[34m*\u001B[37m \u001B[34m*\u001B[37m\u001B[6C\u001B[31m*\u001B[37m\u001B[83C\u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m\u001B[23C\u001B[31m*\u001B[37m  \u001B[32m*\u001B[37m    \u001B[31m*\u001B[34m*\u001B[37m\u001B[6C\u001B[34m*\u001B[37m \u001B[31m*\u001B[37m \u001B[31m**\u001B[37m  \u001B[31m*\u001B[37m\u001B[82C\u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m\u001B[27C\u001B[32m*\u001B[31m*\u001B[37m\u001B[5C\u001B[34m*\u001B[1;33m*\u001B[0m \u001B[34m*\u001B[37m \u001B[34m*\u001B[37m  \u001B[31m*\u001B[37m \u001B[31m*\u001B[37m  \u001B[31m*\u001B[37m\u001B[82C\u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m\u001B[25C\u001B[31m*\u001B[1;33m*\u001B[0m   \u001B[31m*\u001B[37m  \u001B[31m*\u001B[37m\u001B[7C\u001B[31m*\u001B[37m  \u001B[31m*\u001B[37m  \u001B[31m*\u001B[37m\u001B[82C\u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m\u001B[27C\u001B[31m*\u001B[37m\u001B[8C\u001B[1;33m*\u001B[0;31m*\u001B[37m   \u001B[31m*\u001B[37m  \u001B[31m*\u001B[37m  \u001B[31m*\u001B[37m\u001B[82C\u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m\u001B[33C\u001B[31m*\u001B[37m\u001B[5C\u001B[31m*\u001B[37m\u001B[6C\u001B[31m*\u001B[37m\u001B[83C\u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m\u001B[29C\u001B[31m*\u001B[37m\u001B[5C\u001B[31m*\u001B[37m\u001B[6C\u001B[31m*\u001B[37m\u001B[87C\u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m\u001B[32C\u001B[31m*\u001B[37m\u001B[5C\u001B[31m***\u001B[37m   \u001B[31m*\u001B[37m \u001B[31m*\u001B[37m\u001B[83C\u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m\u001B[33C\u001B[31m*\u001B[37m  \u001B[31m*\u001B[37m    \u001B[31m*\u001B[37m\u001B[88C\u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m\u001B[130C\u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m\u001B[130C\u001B[42;30m*\u001B[0m";
    return line;
  }

}
