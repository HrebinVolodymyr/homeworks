package com.lviv.hv.view;

import com.lviv.hv.entity.Droid;
import java.util.List;

public class DroidBattleView extends BaseView {

  @Override
  public void draw(List<Droid> droids) {
    String message =
        Printer.ANSI_YELLOW + " 1 " + Printer.ANSI_RESET + "[SHOOT] | "
            + Printer.ANSI_YELLOW + "2" + Printer.ANSI_RESET + " [USE SKILL] | "
            + Printer.ANSI_YELLOW + "3;. " + Printer.ANSI_RESET
            + "[USE ENERGY ; 1(LIFE) 2 (WEAPON)]                    "
            + "                                ";

    String output = getHeader() + "\n"
        + getBody(droids) + "\n"
        + getFooter(message);
    System.out.println(output);
  }

  private String getBody(List<Droid> droids) {

    Droid friendlyDroid = droids.get(0);
    Droid enemyDroid = droids.get(1);

    String body =
        " \u001B[42;30m*\u001B[40;37m \u001B[44;34m                                \u001B[42;32m  \u001B[40;37m\u001B[15C\u001B[1;33mлллллллллллллллллллллл\u001B[0m\u001B[16C\u001B[42;32m  \u001B[44;34m                                        \u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m \u001B[44;34m                                  \u001B[42;32m  \u001B[40;37m\u001B[49C\u001B[42;32m  \u001B[44;34m                                          \u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m \u001B[44;34m                                    \u001B[42;32m  \u001B[40;37m\u001B[14C\u001B[1;33mллллллллллллллллл\u001B[0m\u001B[14C\u001B[42;32m  \u001B[44;34m                                            \u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m \u001B[44;34m                                      \u001B[42;32m  \u001B[40;37m\u001B[41C\u001B[42;32m  \u001B[44;34m                                              \u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m \u001B[44;34m                                        \u001B[42;32m  \u001B[40;37m\u001B[12C\u001B[1;33mллллллллллллл\u001B[0m\u001B[12C\u001B[42;32m  \u001B[44;34m                                                \u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m \u001B[44;34m                                          \u001B[42;32m  \u001B[40;37m\u001B[33C\u001B[42;32m  \u001B[44;34m                                                  \u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m \u001B[44;34m                                 \u001B[42;32m  \u001B[44;34m         \u001B[42;32m  \u001B[40;37m\u001B[12C\u001B[1;33mлллллл\u001B[0m\u001B[11C\u001B[42;32m  \u001B[44;34m    \u001B[42;32m  \u001B[44;34m                                              \u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m \u001B[44;34m                                 \u001B[42;32m \u001B[40;37m \u001B[42;32m   \u001B[44;34m        \u001B[42;32m  \u001B[40;37m\u001B[25C\u001B[42;32m  \u001B[44;34m    \u001B[42;32m  \u001B[5;1;43;33m \u001B[0;42;32m \u001B[44;34m                                              \u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m \u001B[44;34m                                 \u001B[42;32m \u001B[40;37m    \u001B[42;32m   \u001B[44;34m       \u001B[42;32m                         \u001B[44;34m    \u001B[42;32m  \u001B[5;1;43;33m   \u001B[0;42;32m \u001B[44;34m                                              \u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m \u001B[44;34m                                 \u001B[42;32m \u001B[40;37m\u001B[7C\u001B[42;32m   \u001B[44;34m     \u001B[42;32m \u001B[44;34m     \u001B[41;30m*******\u001B[44;34m         \u001B[42;32m \u001B[44;34m   \u001B[42;32m  \u001B[5;1;43;33m     \u001B[0;42;32m \u001B[44;34m                                              \u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m \u001B[44;34m                                 \u001B[42;32m \u001B[40;37m   \u001B[1m*\u001B[0m\u001B[6C\u001B[42;32m  \u001B[44;34m   \u001B[42;32m \u001B[44;34m \u001B[42;32m    \u001B[41;30m*\u001B[40m \u001B[1;33m*\u001B[0;30m \u001B[1;33m*\u001B[0;30m \u001B[41m*\u001B[42;32m        \u001B[44;34m \u001B[42;32m \u001B[44;34m   \u001B[42;32m \u001B[5;1;43;33m      \u001B[0;42;32m \u001B[44;34m                                              \u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m \u001B[44;34m                                 \u001B[42;32m \u001B[40;37m\u001B[8C\u001B[1m*\u001B[0m  \u001B[42;32m \u001B[44;34m   \u001B[42;32m \u001B[44;34m \u001B[42;32m \u001B[40;37m   \u001B[41;30m*******\u001B[40;37m\u001B[7C\u001B[42;32m \u001B[44;34m \u001B[42;32m \u001B[44;34m   \u001B[42;32m \u001B[5;1;43;33m      \u001B[0;42;32m \u001B[44;34m                                              \u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m \u001B[44;34m                                 \u001B[42;32m \u001B[40;37m\u001B[5C\u001B[1m*\u001B[0m\u001B[5C\u001B[42;32m \u001B[44;34m   \u001B[42;32m \u001B[44;34m \u001B[41;30m***************\u001B[1;40;37m*\u001B[0m  \u001B[42;32m \u001B[44;34m \u001B[42;32m \u001B[44;34m   \u001B[42;32m \u001B[5;1;43;33m      \u001B[0;42;32m \u001B[44;34m          \u001B[40;31m|\u001B[37m\u001B[5C\u001B[31mAMD\u001B[37m\u001B[5C\u001B[31m|\u001B[44;34m                     \u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m \u001B[44;34m                                 \u001B[42;32m \u001B[40;37m\u001B[9C\u001B[1m*\u001B[0m \u001B[42;32m \u001B[44;34m  \u001B[41;30m****\u001B[40m "
            + "\u001B[32mL "

            + this.calculateProgress(enemyDroid.getLife())
            + "\u001B["
            + (10 - enemyDroid.getLife())

            + "C\u001B[30m\u001B[41m****\u001B[42;32m \u001B[44;34m \u001B[42;32m \u001B[44;34m   \u001B[42;32m \u001B[5;1;43;33m      \u001B[0;42;32m \u001B[44;34m          \u001B[40;31m|\u001B[37m "
            + "\u001B[32mL "

            + this.calculateProgress(friendlyDroid.getLife())
            + "\u001B["
            + (10 - friendlyDroid.getLife())

            + "C\u001B[31m|\u001B[44;34m                     \u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m \u001B[44;34m                                 \u001B[42;32m \u001B[40;37m\u001B[11C\u001B[42;32m \u001B[44;34m \u001B[41;30m**\u001B[42;32m \u001B[41;30m**\u001B[40m "
            + "\u001B[31mW "

            + this.calculateProgress(enemyDroid.getWeapon())
            + "\u001B["
            + (10 - enemyDroid.getWeapon())

            + "C\u001B[30m\u001B[41m**\u001B[44;34m \u001B[41;30m**\u001B[44;34m \u001B[42;32m \u001B[44;34m   \u001B[42;32m \u001B[5;1;43;33m      \u001B[0;42;32m \u001B[44;34m          \u001B[40;31m|\u001B[37m "
            + "\u001B[31mW "

            + this.calculateProgress(friendlyDroid.getWeapon())
            + "\u001B["
            + (10 - friendlyDroid.getWeapon())

            + "C|\u001B[44;34m                     \u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m \u001B[44;34m                                 \u001B[42;32m \u001B[40;37m  \u001B[1m*\u001B[0m  \u001B[1m*\u001B[0m   \u001B[42;32m  \u001B[44;34m  \u001B[41;30m**\u001B[42;32m \u001B[44;34m \u001B[41;30m*\u001B[40m "
            + "\u001B[36mS "

            + this.calculateProgress(enemyDroid.getSkillQuantity())
            + "\u001B["
            + (10 - enemyDroid.getSkillQuantity())

            + "C\u001B[30m\u001B[41m*\u001B[44;34m  \u001B[41;30m**\u001B[44;34m \u001B[42;32m \u001B[44;34m   \u001B[42;32m \u001B[5;1;43;33m      \u001B[0;42;32m \u001B[44;34m          \u001B[40;31m| "
            + "\u001B[36mR "

            + this.calculateProgress(friendlyDroid.getSkillQuantity())
            + "\u001B["
            + (10 - friendlyDroid.getSkillQuantity())

            + "C\u001B[31m|\u001B[44;34m                     \u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m \u001B[44;34m                                 \u001B[42;32m \u001B[40;37m\u001B[7C\u001B[42;32m  \u001B[44;34m    \u001B[41;30m**\u001B[42;32m  \u001B[41;30m*\u001B[40m "
            + "\u001B[1;33mE "

            + this.calculateProgress(enemyDroid.getEnergy())
            + "\u001B["
            + (10 - enemyDroid.getEnergy())

            + "C\u001B[0;30m\u001B[41m*\u001B[42;32m  \u001B[41;30m**\u001B[42;32m  \u001B[44;34m   \u001B[42;32m \u001B[5;1;43;33m      \u001B[0;42;32m \u001B[44;34m          \u001B[40;31m|\u001B[36m "
            + "\u001B[1;33mE "

            + this.calculateProgress(friendlyDroid.getEnergy())
            + "\u001B["
            + (10 - friendlyDroid.getEnergy())

            + "C\u001B[0;31m|\u001B[44;34m                     \u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m \u001B[44;34m                                 \u001B[42;32m \u001B[40;37m\u001B[6C\u001B[42;32m \u001B[44;34m    \u001B[41;30m*****\u001B[40;37m \u001B[41;30m*****\u001B[40;31mINTEL\u001B[41;30m*****\u001B[40;37m \u001B[41;30m*****\u001B[42;32m  \u001B[44;34m \u001B[42;32m \u001B[5;1;43;33m      \u001B[0;42;32m \u001B[44;34m          \u001B[40;31m|_____________|\u001B[44;34m                     \u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m \u001B[44;34m                                 \u001B[42;32m \u001B[40;37m    \u001B[42;32m  \u001B[44;34m     \u001B[41;30m*\u001B[40m \u001B[37m  \u001B[41;30m*\u001B[40;37m \u001B[42;30mл\u001B[41m*************\u001B[40;37m  \u001B[41;30m*\u001B[40;37m   \u001B[41;30m*\u001B[40;37m  \u001B[42;32m  \u001B[5;1;43;33m      \u001B[0;42;32m \u001B[44;34m                                              \u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m \u001B[44;34m                                 \u001B[42;32m \u001B[40;37m  \u001B[42;32m  \u001B[44;34m      \u001B[42;32m \u001B[40;37m\u001B[8C\u001B[30m \u001B[41m**\u001B[40;37m\u001B[5C\u001B[41;30m**\u001B[40m \u001B[37m\u001B[12C\u001B[42;32m \u001B[5;1;43;33m     \u001B[0;42;32m \u001B[44;34m                                              \u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m \u001B[44;34m                                 \u001B[42;32m   \u001B[44;34m      \u001B[42;32m  \u001B[40;37m\u001B[8C\u001B[30m \u001B[41m***\u001B[40;37m\u001B[5C\u001B[41;30m***\u001B[40m \u001B[37m\u001B[12C\u001B[42;32m  \u001B[5;1;43;33m   \u001B[0;42;32m \u001B[44;34m                                              \u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m \u001B[44;34m                                 \u001B[42;32m \u001B[44;34m      \u001B[42;32m  \u001B[40;37m\u001B[9C\u001B[30m \u001B[41m****\u001B[40;37m\u001B[5C\u001B[41;30m****\u001B[40m \u001B[37m\u001B[13C\u001B[42;32m  \u001B[5;1;43;33m \u001B[0;42;32m \u001B[44;34m                                              \u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m \u001B[44;34m                                       \u001B[42;32m \u001B[40;37m\u001B[11C\u001B[41;30m******\u001B[40;37m   \u001B[41;30m******\u001B[40;37m\u001B[15C\u001B[42;32m  \u001B[44;34m                                              \u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m \u001B[44;34m                                     \u001B[42;32m  \u001B[40;37m\u001B[43C\u001B[42;32m  \u001B[44;34m                                             \u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m \u001B[44;34m                                    \u001B[42;32m \u001B[40;37m\u001B[47C\u001B[42;32m \u001B[44;34m                                            \u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m \u001B[44;34m                                  \u001B[42;32m  \u001B[40;37m\u001B[49C\u001B[42;32m  \u001B[44;34m                                          \u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m \u001B[44;34m                                \u001B[42;32m  \u001B[40;37m\u001B[53C\u001B[42;32m  \u001B[44;34m                                        \u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m \u001B[44;34m                               \u001B[42;32m \u001B[40;37m\u001B[57C\u001B[42;32m \u001B[44;34m                                       \u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m \u001B[44;34m                             \u001B[42;32m  \u001B[40;37m\u001B[59C\u001B[42;32m  \u001B[44;34m                                     \u001B[42;30m*\u001B[0m\n"
            + " \u001B[42;30m*\u001B[40;37m \u001B[44;34m                           \u001B[42;32m  \u001B[40;37m\u001B[63C\u001B[42;32m \u001B[44;34m                                    \u001B[42;30m*\u001B[0m";
    return body;
  }
}
