/*
 * Copyright (c) 2018, EPAM.
 */
package com.epam.hv;

import static com.epam.hv.Printer.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Class {@code Main} is the root of the program.
 *
 * @author Hrebin Vladimir
 * @version ver.1.0
 */
public class Main {

  public static void main(String... args) {
    Main main = new Main();
    main.go();
  }

  private void go() {
    printHead();
    while (true) {
      String data;
      int sizeFibonacciSet;
      IntervalNumbers interval;
      while (true) {
        println("Enter the interval (for example: 1;100):");
        data = readLine();
        if (data.equals("q")) {
          System.exit(0);
        }
        try {
          interval = new IntervalNumbers(data);
        } catch (IllegalArgumentException e) {
          printEror("Uncorect interval. Try egan.");
          continue;
        }
        break;
      }
      while (true) {
        println("Enter the size of the Fibonacci set: ");
        try {
          sizeFibonacciSet = Integer.parseInt(readLine());
          if (sizeFibonacciSet < 2) {
            throw new IllegalArgumentException();
          }
        } catch (IllegalArgumentException e) {
          printEror("Uncorect the size of set . Try egan.");
          continue;
        }
        break;
      }
      printDelimiter();

      printMassage("Odd numbers from start to end: ");
      println(interval.getOddNumbers());
      printMassage("Even numbers from end to start: ");
      println(reverseArray(interval.getEvenNumbers()));

      printMassage("The sum of odd numbers: ");
      println(sumUpNumbers(interval.getOddNumbers()));
      printMassage("The sum of even numbers: ");
      println(sumUpNumbers(interval.getEvenNumbers()));

      FibonacciCalculator fc = new FibonacciCalculator();
      long[] fibonacciSet = fc.buildFibonacciSet(
          maxValue(interval.getOddNumbers()),
          maxValue(interval.getEvenNumbers()),
          sizeFibonacciSet);
      printDelimiter();
      println("F I B O N A C C I   S E T :");
      println(fibonacciSet);

      IntervalNumbers fibonacciInterval = new IntervalNumbers(fibonacciSet);
      int oddNumbersCount = fibonacciInterval.getOddNumbers().length;
      int evenNumbersCount = fibonacciInterval.getEvenNumbers().length;
      double oddNumbersPercent =
          (oddNumbersCount * 1.0 / (oddNumbersCount + evenNumbersCount)) * 100.0;
      double evenNumbersPercent = 100 - oddNumbersPercent;
      printMassage("The percentage of odd and even Fibonacci numbers:  ");
      println(oddNumbersPercent + " / " + evenNumbersPercent);
      printDelimiter();
    }
  }

  private long maxValue(long[] array) {
    long max = array[0];
    for (long number : array) {
      if (number > max) {
        max = number;
      }
    }
    return max;
  }

  private long[] reverseArray(long[] array) {
    for (int leftIndex = 0; leftIndex < (array.length / 2); leftIndex++) {
      int rightIndex = (array.length - 1) - leftIndex;
      long temp = array[leftIndex];
      array[leftIndex] = array[rightIndex];
      array[rightIndex] = temp;
    }
    return array;
  }

  private long sumUpNumbers(long[] array) {
    long sum = 0L;
    for (long number : array) {
      sum += number;
    }
    return sum;
  }

  private String readLine() {
    String line = null;
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    try {
      line = br.readLine();
    } catch (IOException e) {
      System.out.println("IOException");
    }
    return line;
  }

}
