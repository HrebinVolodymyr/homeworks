/*
 * Copyright (c) 2018, EPAM.
 */
package com.epam.hv;

/**
 * Class {@code IntervalNumbers} creates an interval of numbers.
 *
 * @author Hrebin Vladimir
 * @version ver.1.0
 */
public class IntervalNumbers {

  private long[] interval;

  IntervalNumbers(String str) {
    super();
    try {
      String[] intervalProperties = str.split(";");
      int startInterval = Integer.parseInt(intervalProperties[0].trim());
      int endInterval = Integer.parseInt(intervalProperties[1].trim());
      if (startInterval >= endInterval) {
        throw new Exception();
      }
      fillInterval(startInterval, endInterval);
    } catch (Exception e) {
      throw new IllegalArgumentException();
    }
  }

  IntervalNumbers(long[] array) {
    super();
    long[] interval = new long[array.length];
    System.arraycopy(array, 0, interval, 0, array.length);
    this.interval = interval;
  }

  public long[] getInterval() {
    long[] array = new long[interval.length];
    System.arraycopy(interval, 0, array, 0, interval.length);
    return array;
  }

  long[] getOddNumbers() {
    int sizeOddNumbersInterval = 0;
    long[] oddNumbersInterval;
    for (long number : this.interval) {
      if (!isEvenNumber(number)) {
        sizeOddNumbersInterval++;
      }
    }
    oddNumbersInterval = new long[sizeOddNumbersInterval];
    int index = 0;
    for (long number : this.interval) {
      if (!isEvenNumber(number)) {
        oddNumbersInterval[index] = number;
        index++;
      }
    }
    return oddNumbersInterval;
  }

  long[] getEvenNumbers() {
    int sizeEvenNumbersInterval = 0;
    long[] evenNumbersInterval;
    for (long number : this.interval) {
      if (isEvenNumber(number)) {
        sizeEvenNumbersInterval++;
      }
    }
    evenNumbersInterval = new long[sizeEvenNumbersInterval];
    int index = 0;
    for (long number : this.interval) {
      if (isEvenNumber(number)) {
        evenNumbersInterval[index] = number;
        index++;
      }
    }
    return evenNumbersInterval;
  }

  private boolean isEvenNumber(long number) {
    return (number % 2.0) == 0;
  }

  private void fillInterval(int startInterval, int endInterval) {
    int sizeInterval = (endInterval - startInterval) + 1;
    long[] interval = new long[sizeInterval];
    int arrayIndex = 0;
    for (int i = startInterval; i <= endInterval; i++) {
      interval[arrayIndex] = i;
      arrayIndex++;
    }
    this.interval = interval;
  }
}
