/*
 * Copyright (c) 2018, EPAM.
 */
package com.epam.hv;

/**
 * Class {@code FibonacciCalculator} calculates Fibonacci numbers.
 *
 * @author Hrebin Vladimir
 * @version ver.1.0
 */
class FibonacciCalculator {

  /**
   * Calculates Fibonacci set.
   *
   * @param first first number of Fibonacci set
   * @param second second number of Fibonacci set
   * @param sizeSet Capacity of Fibonacci set
   * @return long[] return fibonacci set
   */
  long[] buildFibonacciSet(final long first, final long second, final int sizeSet) {
    long firsNumber = first;
    long secondNumber = second;
    long thirdNumber;
    long[] fibonacciNumbers = new long[sizeSet];
    fibonacciNumbers[0] = first;
    fibonacciNumbers[1] = second;
    for (int i = 2; i < sizeSet; i++) {
      thirdNumber = firsNumber + secondNumber;
      fibonacciNumbers[i] = thirdNumber;
      firsNumber = secondNumber;
      secondNumber = thirdNumber;
    }
    return fibonacciNumbers;
  }


}
