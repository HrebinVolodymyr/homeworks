/*
 * Copyright (c) 2018, EPAM.
 */
package com.epam.hv;

import java.util.Arrays;

/**
 * Class {@code Printer} forms and prints text in the console.
 *
 * @author Hrebin Vladimir
 * @version ver.1.0
 */
class Printer {

  static void printHead() {
    String str = new String(new char[39]).replace('\0', '*');
    System.out.println();
    System.out.println(str + " F I B O N A C C I   C A L C U L A T O R " + str);
  }

  static void printDelimiter() {
    System.out.println(new String(new char[48]).replace('\0', '-'));
  }

  static void println(long[] array) {
    System.out.println(Arrays.toString(array));
  }

  static void println(long number) {
    System.out.println(number);
  }

  static void println(String str) {
    System.out.println();
    System.out.println(str);
  }

  static void printMassage(String str) {
    System.out.println();
    System.out.print(str);
  }

  static void printEror(String str) {
    printDelimiter();
    System.out.println(String.format("%-20s", "ERROR: " + str));
  }

}
