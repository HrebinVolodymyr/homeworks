package com.lviv.hv.service;

import com.lviv.hv.dao.DaoException;
import com.lviv.hv.dao.UserDao;
import com.lviv.hv.entity.User;

import java.util.List;
import java.util.stream.Collectors;

public class UserService {

    private UserDao userDao;

    public boolean addUser(User user) {
        if (user == null) {
            throw new DaoException("user is null");
        }
        return userDao.addUser(user);
    }

    public List<User> getUsersByAge(final int age) {
        return userDao.getAllUsers().stream().filter(user -> user.getAge() == age)
                .collect(Collectors.toList());
    }

    public void deleteUserByLogin(String login) {
        User user = userDao.findUserByLogin(login);
        if (user != null) {
            userDao.deleteUser(user);
        }
    }
}
