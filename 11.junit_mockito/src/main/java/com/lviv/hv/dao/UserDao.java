package com.lviv.hv.dao;

import com.lviv.hv.entity.User;

import java.util.List;

public interface UserDao {

    User getUserById(Long id) throws DaoException;

    List<User> getAllUsers() throws DaoException;

    User findUserByLogin(String login) throws DaoException;

    boolean addUser(User user) throws DaoException;

    void updateUser(User user) throws DaoException;

    void deleteUser(User user) throws DaoException;


}
