package com.lviv.hv.dao;

import com.lviv.hv.entity.User;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UserDaoImpl implements UserDao {

    static final List<User> users = new ArrayList<>();

    @Override
    public User getUserById(Long id) throws DaoException {
        User user;
        try {
            user = users.stream().filter(user1 -> user1.getId() == 1).collect(Collectors.toList()).get(0);
        } catch (Exception e) {
            return null;
        }
        return user;
    }

    @Override
    public List<User> getAllUsers() throws DaoException {
        return users;
    }

    @Override
    public User findUserByLogin(String login) throws DaoException {
        User user;
        try {
            user = users.stream().filter(user1 -> user1.getLogin() == login).collect(Collectors.toList()).get(0);
        } catch (Exception e) {
            return null;
        }
        return user;
    }

    @Override
    public boolean addUser(User user) throws DaoException {
        return users.add(user);
    }

    @Override
    public void updateUser(User user) throws DaoException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void deleteUser(User user) throws DaoException {
        users.remove(user);

    }
}
