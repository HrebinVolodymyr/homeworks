package com.lviv.hv.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.lviv.hv.dao.DaoException;
import com.lviv.hv.dao.UserDao;
import com.lviv.hv.entity.User;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @InjectMocks
    UserService userService = new UserService();

    @Mock
    UserDao userDao;

    private User user;
    private List<User> users;

    @BeforeEach
    public void before() {
        user = new User(1L, "Jon1", 28, "test1", "pass1");
        users = new ArrayList<>();
        users.add(new User(2L, "Jon2", 29, "test2", "pass2"));
        users.add(new User(3L, "Jon3", 30, "test3", "pass3"));
        users.add(new User(4L, "Jon4", 29, "test4", "pass4"));
    }

    @AfterEach
    public void after() {
        user = null;
        users = null;
    }

    @Test
    void addUserTest() {
        when(userDao.addUser(user)).thenReturn(true);
        assertEquals(userService.addUser(user), true);
    }

    @Test
    void addUserThrowExceptionTest() {
        //when(userDao.addUser(user)).thenReturn(true);
        DaoException exception =
                assertThrows(DaoException.class, () -> userService.addUser(null), "is null");
        assertEquals(exception.getMessage(), "user is null");
    }

    @Test
    void getUsersByAgeTest() {
        when(userDao.getAllUsers()).thenReturn(users);
        List<User> usersAge29 =
                users.stream().filter(user -> user.getAge() == 29).collect(Collectors.toList());
        assertEquals(userService.getUsersByAge(29), usersAge29);
    }

    @Test
    void deleteUserByLoginTest() {
        when(userDao.findUserByLogin("test1")).thenReturn(user);
        doNothing().when(userDao).deleteUser(user);
        userService.deleteUserByLogin("test1");
        verify(userDao, times(1)).findUserByLogin("test1");
        verify(userDao, times(1)).deleteUser(user);
    }
}