package com.lviv.hv;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.IncludeClassNamePatterns;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.platform.suite.api.SuiteDisplayName;
import org.junit.runner.RunWith;

@RunWith(JUnitPlatform.class)
@SuiteDisplayName("JUnit 5 Suite Demo")
@SelectPackages("com.lviv.hv")
@IncludeClassNamePatterns({"^.*$"})
public class TestSuite {
}
