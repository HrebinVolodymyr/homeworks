package com.lviv.hv.dao;

import com.lviv.hv.entity.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assumptions.assumeTrue;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UserDaoImplTest {

    private UserDaoImpl userDao;
    private User user;
    private List<User> users;

    @BeforeEach
    public void before() {
        userDao = new UserDaoImpl();
        user = new User(1L, "Jon1", 28, "test1", "pass1");
        users = new ArrayList<>();
        users.add(new User(2L, "Jon2", 29, "test2", "pass2"));
        users.add(new User(3L, "Jon3", 30, "test3", "pass3"));
        users.add(new User(4L, "Jon4", 29, "test4", "pass4"));
    }

    @AfterEach
    public void after() {
        UserDaoImpl.users.clear();
        user = null;
        users = null;
    }

    @Test
    void getUserById() {
        userDao.addUser(user);
        assertEquals(user, userDao.getUserById(1L));
    }

    @Test
    void getUserByIdIsNull() {
        assertNull(userDao.getUserById(1L));
    }

    @Test
    void getUserByIdNotNull() {
        userDao.addUser(user);
        assertNotNull(userDao.getUserById(1L));
    }

    @Test
    void getAllUsers() {
        UserDaoImpl.users.addAll(users);
        assertEquals(userDao.getAllUsers(), users);

    }

    @Test
    void findUserByLogin() {
        userDao.addUser(user);
        assertEquals(user, userDao.findUserByLogin("test1"));
    }

    @Test
    void addUserTrue() {
        assertTrue(userDao.addUser(user));
    }

    @Test
    void addUser() {
        int usersCount = UserDaoImpl.users.size();
        userDao.addUser(user);
        assertEquals(UserDaoImpl.users.size(), usersCount + 1);
    }

    //@Disabled
    @Test
    void updateUser() {
        try {
            userDao.updateUser(user);
        } catch (UnsupportedOperationException e) {
            assumeTrue(false, "Test skipped: UnsupportedOperationException");
        }
    }

    @Test
        //@Disabled
    void deleteUser() {
        userDao.addUser(user);
        int usersCount = UserDaoImpl.users.size();
        userDao.deleteUser(user);
        assertEquals(UserDaoImpl.users.size(), usersCount - 1);
    }
}