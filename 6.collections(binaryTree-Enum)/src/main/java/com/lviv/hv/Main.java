package com.lviv.hv;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Random;
import java.util.TreeMap;

public class Main {

  public static void main(String... args) {
    System.out.println("***************** B I N A R Y   T R E E   M A P *********************");
    TreeMap<String, String> map = new TreeMap<String, String>();
    System.out.println("Size: " + map.size());
    System.out.println("Is Empty: " + map.isEmpty());

    System.out.println("Contains key \"Hello\": " + map.containsKey("Hello"));
    map.put("Hello", "World");
    System.out.println("Contains key \"Hello\": " + map.containsKey("Hello"));

    System.out.println(map.get("Hello"));
    System.out.println("Size: " + map.size());

    map.put("5", "Five");
    map.put("3", "Three");
    map.put("7", "Seven");
    map.put("10", "Ten");
    map.put("1", "ONE");
    map.put("4", "Four");
    map.put("10", "Ten");
    map.put("6", "Six");
    map.put("3", "Three");
    map.put("2", "TWO");
    System.out.println(map.get("1"));
    System.out.println(map.get("2"));
    System.out.println("Size: " + map.size());
    System.out.println("Contains key \"1\": " + map.containsKey("1"));
    System.out.println("Contains key \"2\": " + map.containsKey("2"));

    map.remove("Hello");
    System.out.println("Contains key \"Hello\": " + map.containsKey("Hello"));
    System.out.println("Size: " + map.size());

    System.out.println(map.get("Hello"));
    map.clear();
    System.out.println("Size: " + map.size());

    System.out.println("**************************** E N U M *********************************");
    showMenu();
  }

  public static void showMenu() {
    while (true) {
      List<String> items = Menu.getMenuItems();
      for (String item : items) {
        System.out.println(item);
      }
      String data;
      data = readLine();
      Integer menuItem = Integer.parseInt(data);
      switch (menuItem) {
        case 1:
          System.out.println("******* H E L L O *******");
          break;
        case 2:
          System.out.println("R A N D O M   I N T : " + randInt(0, 1000));
          System.out.println();
          break;
        case 3:
          System.exit(0);
      }
    }
  }

  private static String readLine() {
    String line = null;
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    try {
      line = br.readLine();
    } catch (IOException e) {
      System.out.println("IOException");
    }
    return line;
  }

  private static int randInt(int min, int max) {
    Random rand = new Random();
    int randomNum = rand.nextInt((max - min) + 1) + min;
    return randomNum;
  }
}
