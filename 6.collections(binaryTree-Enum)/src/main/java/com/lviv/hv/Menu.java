package com.lviv.hv;

import java.util.LinkedList;
import java.util.List;

public enum Menu {
  SHOW_MESSAGE(1, ".Show message"),
  PRINT_NUMBER(2, ".Print random int"),
  EXIT(3, ".Exit");

  private int id;
  private String message;

  Menu(int id, String message) {
    this.id = id;
    this.message = message;
  }

  public static List<String> getMenuItems() {
    List<String> items = new LinkedList<String>();
    Menu[] menu = Menu.values();
    for (Menu item : menu) {
      items.add(item.id + item.message);
    }
    return items;
  }
}
