package com.lviv.hv;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class BinaryTreeMap<K extends Comparable, V> implements Map<K, V> {

  private int size = 0;
  private Entry<K, V> root;

  public int size() {
    return size;
  }

  public boolean isEmpty() {
    return size() == 0;
  }

  public boolean containsKey(Object key) {
    return getEntry((K) key) != null;
  }

  public Entry<K, V> getEntry(K key) {
    Entry<K, V> entry = root;
    while (entry != null) {
      int cmp = key.compareTo(entry.key);
      if (cmp == 0) {
        return entry;
      }
      if (cmp < 0) {
        entry = entry.left;
      } else {
        entry = entry.right;
      }
    }
    return null;
  }

  public V get(Object key) {
    return getEntry((K) key).value;
  }

  public V put(K key, V value) {
    Entry<K, V> entry = root;
    Entry<K, V> nextEntry = null;
    while (entry != null) {
      int cmp = key.compareTo(entry.key);
      if (cmp == 0) {
        entry.value = value;
        return value;
      } else {
        nextEntry = entry;
        if (cmp < 0) {
          entry = entry.left;
        } else {
          entry = entry.right;
        }
      }
    }
    Entry<K, V> newEntry = new Entry<K, V>(key, value);
    if (nextEntry == null) {
      root = newEntry;
      size++;
    } else {
      if (key.compareTo(nextEntry.key) < 0) {
        nextEntry.left = newEntry;
        size++;
      } else {
        nextEntry.right = newEntry;
        size++;
      }
    }
    return newEntry.value;
  }

  public V remove(Object o) {
    K key = (K) o;
    Entry<K, V> entry = root;
    Entry<K, V> nextEntry = null;
    while (entry != null) {
      int cmp = key.compareTo(entry.key);
      if (cmp == 0) {
        break;
      } else {
        nextEntry = entry;
        if (cmp < 0) {
          entry = entry.left;
        } else {
          entry = entry.right;
        }
      }
    }
    if (entry == null) {
      return null;
    }
    if (entry.right == null) {
      if (nextEntry == null) {
        root = entry.left;
      } else {
        if (entry == nextEntry.left) {
          nextEntry.left = entry.left;
        } else {
          nextEntry.right = entry.left;
        }
      }
    } else {
      Entry<K, V> leftMost = entry.right;
      nextEntry = null;
      while (leftMost.left != null) {
        nextEntry = leftMost;
        leftMost = leftMost.left;
      }
      if (nextEntry != null) {
        nextEntry.left = leftMost.right;
      } else {
        entry.right = leftMost.right;
      }
      entry.key = leftMost.key;
      entry.value = leftMost.value;
    }
    return null;
  }

  public void putAll(Map m) {
    Set<Map.Entry<K, V>> set = m.entrySet();
    for (Map.Entry<K, V> entry : set) {
      put(entry.getKey(), entry.getValue());
    }
  }

  public void clear() {
    size = 0;
    root = null;
  }

  public Set keySet() {
    return null;
  }

  public Collection values() {
    return null;
  }

  public Set<Map.Entry<K, V>> entrySet() {
    return null;
  }

  public boolean containsValue(Object value) {
    return false;
  }

  static final class Entry<K, V> {

    K key;
    V value;
    BinaryTreeMap.Entry<K, V> left;
    BinaryTreeMap.Entry<K, V> right;

    public Entry(K key, V value) {
      this.key = key;
      this.value = value;
    }

    public K getKey() {
      return key;
    }

    public String toString() {
      return key + "=" + value;
    }
  }
}
