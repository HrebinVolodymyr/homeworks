package com.lviv.hv.stream;

import com.lviv.hv.Printer;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

  public static void main(String... args) {
    testStream();

    String text = "";
    Printer.println("");
    while (true) {
      String textLine = readLine();
      if (textLine.equals("")) {
        break;
      }
      text = text + " " + textLine;
    }

    List<String> list = Stream.of(text.split("\\."))
        .map(s -> s.trim())
        .flatMap(s -> Stream.of(s.split(",")))
        .map(s -> s.trim())
        .flatMap(s -> Stream.of(s.split(" ")))
        .collect(Collectors.toList());

    Printer.println(list);
    Printer.println("Number of unique words: "
        + list.stream().map(s -> s.toLowerCase()).distinct().count());
    Printer.println("Sorted list of all unique words: "
        + list.stream().distinct().sorted().collect(Collectors.toList()));
    Printer.println("Number of each word in the text: "
        + list.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting())));
    Printer.println("Number of each symbol except upper case characters: "
        + text.chars().filter(Character::isLowerCase).count());

  }

  private static void testStream() {
    List<Integer> list = getGenerateStream();
    Printer.println(list);
    double avg = list.stream().mapToInt(value -> value).average().getAsDouble();
    Printer.println("AVERAGE: " + avg);
    Printer.println("SUM: " + list.stream().mapToInt(value -> value).sum());
    Printer.println(
        "REDUCE SUM: " + list.stream().reduce((integer, integer2) -> integer + integer2).orElse(0));
    Printer.println("MIN: " + list.stream().mapToInt(value -> value).min());
    Printer.println("MAX: " + list.stream().mapToInt(value -> value).max());

    Printer.println("Count number of values that are bigger than average: " +
        list.stream().filter(integer -> integer > avg).count());

  }

  private static List<Integer> getIterateStream() {
    List<Integer> list = Stream.iterate(10000, (n) -> new Random().nextInt(n) + 1)
        .limit(10).collect(Collectors.toList());
    return list;
  }

  private static List<Integer> getGenerateStream() {
    List<Integer> list = Stream.generate(() -> new Random().nextInt(100))
        .limit(10).collect(Collectors.toList());
    return list;
  }

  private static String readLine() {
    String line = null;
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    try {
      line = br.readLine();
    } catch (IOException e) {
      System.out.println("IOException");
    }
    return line;
  }
}
