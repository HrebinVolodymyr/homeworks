package com.lviv.hv;

@FunctionalInterface
public interface Calculator {

  int calculate(int a, int b, int c);

  default void print(int i) {
    System.out.print(i);
  }

  static void println(int i) {
    System.out.println(i);
  }

}
