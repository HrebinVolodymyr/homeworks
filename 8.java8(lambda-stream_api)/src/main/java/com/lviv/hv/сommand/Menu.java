package com.lviv.hv.сommand;

import java.util.LinkedList;
import java.util.List;

public enum Menu {
  LAMBDA_FUNCTION("1", ".Command as lambda function"),
  METHOD_REFERENCE("2", ".Command as method reference"),
  ANONYMOUS_CLASS("3", ".Command as anonymous class"),
  OBJECT_OF_COMMAND("4", ".Command as object of command class"),
  EXIT("5", ".Exit");

  private String commandName;
  private String message;

  Menu(String commandName, String message) {
    this.commandName = commandName;
    this.message = message;
  }

  public static List<String> getMenuItems() {
    List<String> items = new LinkedList<String>();
    Menu[] menu = Menu.values();
    for (Menu item : menu) {
      items.add(item.commandName + item.message);
    }
    return items;
  }

  public String getCommandName() {
    return this.commandName;
  }
}
