package com.lviv.hv.сommand;

import com.lviv.hv.Printer;

public class CommandClass implements Command {

  @Override
  public void execute(String message) {
    Printer.println("Object of command class: " + message);
  }
}
