package com.lviv.hv.сommand;

@FunctionalInterface
public interface Command {

  public void execute(String message);

}
