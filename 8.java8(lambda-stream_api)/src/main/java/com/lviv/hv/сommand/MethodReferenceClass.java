package com.lviv.hv.сommand;

import com.lviv.hv.Printer;

public class MethodReferenceClass implements Command {

  @Override
  public void execute(String message) {
    Printer.println("Method reference: " + message);
  }
}
