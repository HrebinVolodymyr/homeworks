package com.lviv.hv.сommand;

import com.lviv.hv.Printer;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

public class Main {

  private static Map<String, Command> commandsMap = new LinkedHashMap<>();

  public static void main(String... args) {
    Main main = new Main();
    main.go();
  }

  private void go() {
    //************* Added Commands ************
    commandsMap.put("1", (message) -> Printer.println("Lambda function: " + message));
    commandsMap.put("2", new MethodReferenceClass()::execute);
    commandsMap.put("3", new Command() {
      @Override
      public void execute(String message) {
        Printer.println("Anonymous class: " + message);
      }
    });
    commandsMap.put("4", new CommandClass());

//**************** EXECUTE COMMANDS ************
    String keyMenu;
    while (true) {
      showMenu();
      keyMenu = readLine();
      if (keyMenu.equals("5")) {
        System.exit(0);
      }
      commandsMap.get(keyMenu).execute(readLine());
    }
  }

  private static void getCommand(String s) {
  }

  private static void showMenu() {
    Menu.getMenuItems().stream().forEach(Printer::printlnMenuItem);
  }

  private static String readLine() {
    String line = null;
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    try {
      line = br.readLine();
    } catch (IOException e) {
      System.out.println("IOException");
    }
    return line;
  }

}
