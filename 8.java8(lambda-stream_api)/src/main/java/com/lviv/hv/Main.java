package com.lviv.hv;

import com.sun.org.apache.regexp.internal.recompile;
import java.util.Arrays;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

public class Main {

  public static void main(String... args) {
    Calculator maxValue = (a, b, c) -> Stream.of(a, b, c).mapToInt(Integer::intValue).max()
        .getAsInt();
    Calculator average = (a, b, c) -> (int) Stream.of(a, b, c).mapToInt(Integer::byteValue)
        .average().getAsDouble();
    System.out.println(maxValue.calculate(2, 5, 3));
    System.out.println(average.calculate(2, 10, 18));


  }

}
