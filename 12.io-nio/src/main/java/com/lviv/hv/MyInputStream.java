package com.lviv.hv;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.Queue;

public class MyInputStream extends InputStream {

  Queue<Byte> bytes = new LinkedList<>();

  MyInputStream() {
    bytes.add((byte) 1);
    bytes.add((byte) 3);
    bytes.add((byte) 5);
    bytes.add((byte) 7);
    bytes.add((byte) 9);
    bytes.add((byte) 10);

  }

  @Override
  public int read() throws IOException {
    Byte b = bytes.poll();
    if (b == null) {
      return -1;
    } else {
      return (int) b;
    }
  }

  public void push(int data) {
    bytes.add((byte)data);
  }
}
