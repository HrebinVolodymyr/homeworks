package com.lviv.hv;

import com.lviv.hv.entity.Bot;
import com.lviv.hv.entity.IntelDroid;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Main {

  public static final String ANSI_RESET = "\u001B[0m";
  public static final String ANSI_RED = "\u001B[31m";
  private static int level = -2;

  public static void main(String... args) {
//    readWriteObject();
//    compareFileIO();
//    testMyInputStream();
//    printComments("tmp/Object.java");
    printContent("C:\\Program Files\\Java");
  }

  //
  public static void readWriteObject() {
    Spaceship<IntelDroid> spaceship = new Spaceship<>();
    spaceship.setName("Enterprise");
    spaceship.setComment("Intel droid");
    spaceship.addDroid(new IntelDroid(new Bot(), 1));
    spaceship.addDroid(new IntelDroid(new Bot(), 2));
    spaceship.addDroid(new IntelDroid(new Bot(), 3));
    System.out.println("******* Before serialization *******");
    System.out.println("Spaceship name: " + spaceship.getName());
    System.out.print("DROIDS: ");
    for (IntelDroid intelDroid : spaceship.getAllDroids()) {
      System.out.print("[IntelDroid: " + intelDroid.getUser().getName() + "] ");
    }
    System.out.println();
    System.out.println("Comment: " + spaceship.getComment());
    System.out.println();

    //writeObject
    try (FileOutputStream file = new FileOutputStream("tmp/enterprise.dat");
        ObjectOutputStream out = new ObjectOutputStream(file)) {
      out.writeObject(spaceship);
    } catch (IOException e) {
      e.printStackTrace();
    }

    //readObject
    try (FileInputStream file = new FileInputStream("tmp/enterprise.dat");
        ObjectInputStream in = new ObjectInputStream(file)) {
      Spaceship<IntelDroid> spaceshipIntel = (Spaceship<IntelDroid>) in.readObject();

      System.out.println("******* After serialization *******");
      System.out.println("Spaceship name: " + spaceshipIntel.getName());
      System.out.print("DROIDS: ");
      for (IntelDroid intelDroid : spaceshipIntel.getAllDroids()) {
        System.out.print("[IntelDroid: " + intelDroid.getUser().getName() + "] ");
      }
      System.out.println();
      System.out.println("Comment: " + spaceshipIntel.getComment());
    } catch (IOException | ClassNotFoundException e) {
      e.printStackTrace();
    }
  }

  //
  public static void compareFileIO() {
    System.out.println("*** File size 5MB ***");
    //Write without buffer
    try (FileOutputStream file = new FileOutputStream("tmp/compareFileIO.dat")) {
      long startTime = System.currentTimeMillis();
      for (int bytes = 1; bytes <= 5 * 1024 * 1024; bytes++) {
        file.write(255);
      }
      long estimatedTime = System.currentTimeMillis() - startTime;
      System.out.println("Write without buffer: " + estimatedTime / 1000.0 + " seconds.");
    } catch (IOException e) {
      e.printStackTrace();
    }

    //Write with buffer (1KB)
    try (FileOutputStream file = new FileOutputStream("tmp/compareFileIO.dat");
        BufferedOutputStream out = new BufferedOutputStream(file, 1024)) {
      long startTime = System.currentTimeMillis();
      for (int bytes = 1; bytes <= 5 * 1024 * 1024; bytes++) {
        out.write(255);
      }
      long estimatedTime = System.currentTimeMillis() - startTime;
      System.out.println("Write with buffer (1KB): " + estimatedTime / 1000.0 + " seconds.");
    } catch (IOException e) {
      e.printStackTrace();
    }
    System.out.println();

    //Read without buffer
    try (FileInputStream file = new FileInputStream("tmp/compareFileIO.dat")) {
      byte[] fileBytes = new byte[file.available()];
      int index = 0;
      long startTime = System.currentTimeMillis();
      int data = file.read();
      while (data != -1) {
        fileBytes[index] = (byte) data;
        index++;
        data = file.read();
      }
      long estimatedTime = System.currentTimeMillis() - startTime;
      System.out.println("Read without buffer: " + estimatedTime / 1000.0 + " seconds.");
    } catch (IOException e) {
      e.printStackTrace();
    }

    //Read with buffer
    for (int i = 1; i <= 100; i++) {
      try (FileInputStream file = new FileInputStream("tmp/compareFileIO.dat")) {
        BufferedInputStream in = new BufferedInputStream(file, 10 * i);
        byte[] fileBytes = new byte[file.available()];
        int index = 0;
        long startTime = System.nanoTime();
        int data = in.read();
        while (data != -1) {
          fileBytes[index] = (byte) data;
          index++;
          data = in.read();
        }
        long estimatedTime = System.nanoTime() - startTime;
        System.out.println(
            "Read with buffer(" + i * 10 + " bytes): " + estimatedTime / 1000000000.0
                + " seconds.");
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  //
  public static void testMyInputStream() {
    try (MyInputStream in = new MyInputStream()) {
      System.out.println(in.read());
      in.push(4);
      System.out.println(in.read());
      in.push(6);
      int data = in.read();
      while (data != -1) {
        data = in.read();
        System.out.println(data);
      }

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static void printComments(String path) {
    try (FileReader fileReader = new FileReader(new File(path));
        BufferedReader reader = new BufferedReader(fileReader)) {
      String line;
      while ((line = reader.readLine()) != null) {
        String data = line.trim();
        if (data.length() >= 1) {
          if (data.charAt(0) == '/' || data.charAt(0) == '*') {
            System.out.println(line);
          }
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static void printContent(String path) {
    File rootFile = new File(path);
    printFile(rootFile);
  }

  public static void printFile(File rootFile) {
    level++;
    if (rootFile.isDirectory()) {
      File[] files = rootFile.listFiles();
      for (File file : files) {
        if (file.isDirectory()) {
          System.out.println(getSpace(level) + "DIR: " + file.getName());
        }
        printFile(file);
      }
    } else {
      System.out.println(ANSI_RED + getSpace(level) + rootFile.getName() + ANSI_RESET);
    }
    level--;
  }

  private static String getSpace(int count) {
    String space = "";
    for (int i = 0; i <= count; i++) {
      space += "   ";
    }
    return space;
  }



}
