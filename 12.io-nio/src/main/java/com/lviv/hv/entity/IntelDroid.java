package com.lviv.hv.entity;

import java.io.Serializable;

public class IntelDroid extends Droid {

  private int shield = 9;

  public IntelDroid(User user, int droidId) {
    super(user, droidId);
  }

  @Override
  public void useSkill() {
    shield = shield - 1;
    setSkillActivated(true);
  }

  @Override
  public int getSkillQuantity() {
    return shield;
  }

  public int getShield() {
    return shield;
  }

  public void setShield(int shield) {
    this.shield = shield;
  }

  @Override
  public String toString() {
    return "IntelDroid";
  }
}
