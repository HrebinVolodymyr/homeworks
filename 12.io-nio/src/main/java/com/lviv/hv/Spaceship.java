package com.lviv.hv;

import com.lviv.hv.entity.Droid;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Spaceship<T extends Droid> implements Serializable {

  private String name;

  private ArrayList<T> droids = new ArrayList<>();

  private int life = 9;

  private transient String comment;

  public void addDroid(T droid) {
    droids.add(droid);
  }

  public void addAllDroids(List<? extends T> droids) {
    this.droids.addAll(droids);
  }

  public List<T> getAllDroids() {
    return droids;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getLife() {
    return life;
  }

  public void setLife(int life) {
    this.life = life;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }
}
