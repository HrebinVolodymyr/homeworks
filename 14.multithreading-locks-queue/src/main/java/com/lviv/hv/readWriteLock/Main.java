package com.lviv.hv.readWriteLock;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Main {

  private static ReadWriteLock rwLock = new ReentrantReadWriteLock();
  private static Lock readLock = rwLock.readLock();
  private static Lock writeLock = rwLock.writeLock();
  private static int count;

  public static void main(String... args) {
    ScheduledExecutorService pool = Executors.newScheduledThreadPool(5);

    for (int i = 0; i < 10; i++) {
      pool.schedule(new CountWriter(), 1, TimeUnit.SECONDS);
      for (int j = 0; j < 4; j++) {
        pool.schedule(new CountReader(), 1, TimeUnit.SECONDS);
      }
    }
    pool.shutdown();

  }

  static class CountReader implements Runnable {

    @Override
    public void run() {
      readLock.lock();
      try {
        System.out.println("Count: " + count);
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      } finally {
        readLock.unlock();
      }
    }
  }

  static class CountWriter implements Runnable {

    @Override
    public void run() {
      writeLock.lock();
      try {
        count++;
        Thread.sleep(5000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      } finally {
        writeLock.unlock();
      }
    }
  }

}
