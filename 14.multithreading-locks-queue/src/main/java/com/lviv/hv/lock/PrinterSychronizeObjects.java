package com.lviv.hv.lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class PrinterSychronizeObjects {

  Lock lockOne = new ReentrantLock();
  Lock lockTwo = new ReentrantLock();
  Lock lockThree = new ReentrantLock();

  public void printOne() {
    lockOne.lock();
    try {
      System.out.println(Thread.currentThread().getName() + ": ONE");
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    } finally {
      lockOne.unlock();
    }
  }

  public void printTwo() {
    lockTwo.lock();
    try {
      System.out.println(Thread.currentThread().getName() + ": TWO");
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    } finally {
      lockTwo.unlock();
    }
  }

  public void printThree() {
    lockThree.lock();
    try {
      System.out.println(Thread.currentThread().getName() + ": THREE");
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    } finally {
      lockThree.unlock();
    }
  }
}
