package com.lviv.hv.lock;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

  private static Printer printer = new Printer();
  private static PrinterSychronizeObjects printer2 = new PrinterSychronizeObjects();

  public static void main(String... args) {

    ExecutorService pool = Executors.newFixedThreadPool(5);

    for (int i = 0; i < 15; i++) {
      pool.execute(() -> {
        printer.printOne();
        printer.printTwo();
        printer.printThree();
      });
    }

    for (int i = 0; i < 30; i++) {
      pool.execute(() -> {
        printer2.printOne();
        printer2.printTwo();
        printer2.printThree();
      });
    }
    pool.shutdown();


  }

}
