package com.lviv.hv.lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Printer {

  Lock lock = new ReentrantLock();

  public void printOne() {
    lock.lock();
    try {
      System.out.println(Thread.currentThread().getName() + ": ONE");
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    } finally {
      lock.unlock();
    }
  }

  public void printTwo() {
    if(lock.tryLock()) {
      try {
        System.out.println(Thread.currentThread().getName() + ": TWO");
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      } finally {
        lock.unlock();
      }
    }else {
      System.out.println("Try lock false.");
    }
  }

  public void printThree() {
    lock.lock();
    try {
      System.out.println(Thread.currentThread().getName() + ": THREE");
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    } finally {
      lock.unlock();
    }
  }

}
