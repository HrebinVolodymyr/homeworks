package com.lviv.hv.blockingQueue;


import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Main {

  public static void main(String... args) throws Exception {

    BlockingQueue queue = new LinkedBlockingQueue(3);

    ScheduledExecutorService pool = Executors.newScheduledThreadPool(3);
    pool.schedule(new Printer(queue), 1, TimeUnit.SECONDS);
    pool.schedule(new Client(queue), 3, TimeUnit.SECONDS);
    pool.schedule(new Client(queue), 9, TimeUnit.SECONDS);
    pool.shutdown();
  }
}
