package com.lviv.hv.blockingQueue;

import java.util.concurrent.BlockingQueue;

public class Printer implements Runnable {

  BlockingQueue<String> queue;

  public Printer(BlockingQueue<String> queue) {
    this.queue = queue;
  }

  @Override
  public void run() {
    System.out.println("******* PRINTER STARTED ******* ");
    try {
      while (true) {
        String text = queue.take();
        if (text != null) {
          Thread.sleep(300);
          System.out.println(text);
        } else {
          break;
        }
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
