-- MySQL dump 10.13  Distrib 8.0.11, for Win64 (x86_64)
--
-- Host: localhost    Database: database n2
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `arrears`
--

DROP TABLE IF EXISTS `arrears`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `arrears` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title_subject` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `title_subject_UNIQUE` (`title_subject`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `arrears`
--

LOCK TABLES `arrears` WRITE;
/*!40000 ALTER TABLE `arrears` DISABLE KEYS */;
INSERT INTO `arrears` VALUES (2,'Computer Science'),(1,'network technology');
/*!40000 ALTER TABLE `arrears` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `region_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_city_region1_idx` (`region_id`),
  CONSTRAINT `fk_city_region1` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
INSERT INTO `city` VALUES (1,'Lviv',1),(2,'Stry',1),(3,'Kiev',2);
/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `group`
--

DROP TABLE IF EXISTS `group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `group_number` varchar(45) NOT NULL,
  `year_admission` year(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group`
--

LOCK TABLES `group` WRITE;
/*!40000 ALTER TABLE `group` DISABLE KEYS */;
INSERT INTO `group` VALUES (1,'EK','103',2015);
/*!40000 ALTER TABLE `group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `region`
--

DROP TABLE IF EXISTS `region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `region` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `region_code` int(2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `region_code_UNIQUE` (`region_code`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `region`
--

LOCK TABLES `region` WRITE;
/*!40000 ALTER TABLE `region` DISABLE KEYS */;
INSERT INTO `region` VALUES (1,'Lviv region',1),(2,'Kiev region',2);
/*!40000 ALTER TABLE `region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `secondary_education_institution`
--

DROP TABLE IF EXISTS `secondary_education_institution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `secondary_education_institution` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `director_institution` varchar(45) NOT NULL,
  `city_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_secondary_education_institution_city1_idx` (`city_id`),
  CONSTRAINT `fk_secondary_education_institution_city1` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `secondary_education_institution`
--

LOCK TABLES `secondary_education_institution` WRITE;
/*!40000 ALTER TABLE `secondary_education_institution` DISABLE KEYS */;
INSERT INTO `secondary_education_institution` VALUES (1,'school №100','259-85-95','Ivan Stepanov',1),(2,'school №3','589-24-78','Taras Ilkiv',2),(3,'school №45','624-78-35','Pavlo Tkach',3);
/*!40000 ALTER TABLE `secondary_education_institution` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `middle_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `overall_rating` smallint(3) DEFAULT NULL,
  `date_birth` date NOT NULL,
  `date_receipt` date NOT NULL,
  `student_ticket_number` bigint(20) NOT NULL,
  `email` varchar(45) NOT NULL,
  `city_id` int(11) NOT NULL,
  `secondary_education_institution_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `info` varchar(150) GENERATED ALWAYS AS (concat(`student_ticket_number`,_utf8mb4'-',year(`date_receipt`))) VIRTUAL,
  `age_entry` int(11) GENERATED ALWAYS AS ((year(`date_receipt`) - year(`date_birth`))) VIRTUAL,
  `full_name` varchar(45) GENERATED ALWAYS AS (concat(`last_name`,_utf8mb3' ',`first_name`,_utf8mb3' ',`middle_name`)) VIRTUAL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_Student_city_idx` (`city_id`),
  KEY `fk_Student_secondary_education_institution1_idx` (`secondary_education_institution_id`),
  KEY `fk_Student_group1_idx` (`group_id`),
  CONSTRAINT `fk_Student_city` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`),
  CONSTRAINT `fk_Student_group1` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`),
  CONSTRAINT `fk_Student_secondary_education_institution1` FOREIGN KEY (`secondary_education_institution_id`) REFERENCES `secondary_education_institution` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` (`id`, `first_name`, `middle_name`, `last_name`, `overall_rating`, `date_birth`, `date_receipt`, `student_ticket_number`, `email`, `city_id`, `secondary_education_institution_id`, `group_id`) VALUES (3,'Lybomur','Andrilovich','Salandjak',45,'1999-06-07','2015-08-08',5479657812,'Salandjak@gmail.com',1,1,1),(4,'Vasul','Stepanovuch','Kirichyk',50,'1999-05-06','2015-08-08',5668547865,'Kirichyk@gmail.com',2,2,1),(5,'Volodymyr','Igorovich','Hrebin',55,'1999-04-09','2015-08-08',5425987651,'Hrebin@gmail.com',3,3,1),(6,'Nazar','Stepanovuch','Grib',55,'1999-08-07','2015-08-08',5963489235,'Grib@gmail.com',1,1,1);
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student_arrears`
--

DROP TABLE IF EXISTS `student_arrears`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `student_arrears` (
  `Student_id` int(11) NOT NULL AUTO_INCREMENT,
  `arrears_id` int(11) NOT NULL,
  PRIMARY KEY (`Student_id`,`arrears_id`),
  KEY `fk_Student_has_arrears_arrears1_idx` (`arrears_id`),
  KEY `fk_Student_has_arrears_Student1_idx` (`Student_id`),
  CONSTRAINT `fk_Student_has_arrears_Student1` FOREIGN KEY (`Student_id`) REFERENCES `student` (`id`),
  CONSTRAINT `fk_Student_has_arrears_arrears1` FOREIGN KEY (`arrears_id`) REFERENCES `arrears` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student_arrears`
--

LOCK TABLES `student_arrears` WRITE;
/*!40000 ALTER TABLE `student_arrears` DISABLE KEYS */;
INSERT INTO `student_arrears` VALUES (3,1),(6,1),(4,2),(5,2),(6,2);
/*!40000 ALTER TABLE `student_arrears` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-11 13:50:02
