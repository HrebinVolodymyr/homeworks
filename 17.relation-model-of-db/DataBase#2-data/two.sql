-- MySQL dump 10.13  Distrib 8.0.11, for Win64 (x86_64)
--
-- Host: localhost    Database: database n2
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `arrears`
--

LOCK TABLES `arrears` WRITE;
/*!40000 ALTER TABLE `arrears` DISABLE KEYS */;
INSERT INTO `arrears` VALUES (2,'Computer Science'),(1,'network technology');
/*!40000 ALTER TABLE `arrears` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
INSERT INTO `city` VALUES (1,'Lviv',1),(2,'Stry',1),(3,'Kiev',2);
/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `group`
--

LOCK TABLES `group` WRITE;
/*!40000 ALTER TABLE `group` DISABLE KEYS */;
INSERT INTO `group` VALUES (1,'EK','103',2015);
/*!40000 ALTER TABLE `group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `region`
--

LOCK TABLES `region` WRITE;
/*!40000 ALTER TABLE `region` DISABLE KEYS */;
INSERT INTO `region` VALUES (1,'Lviv region',1),(2,'Kiev region',2);
/*!40000 ALTER TABLE `region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `secondary_education_institution`
--

LOCK TABLES `secondary_education_institution` WRITE;
/*!40000 ALTER TABLE `secondary_education_institution` DISABLE KEYS */;
INSERT INTO `secondary_education_institution` VALUES (1,'school №100','259-85-95','Ivan Stepanov',1),(2,'school №3','589-24-78','Taras Ilkiv',2),(3,'school №45','624-78-35','Pavlo Tkach',3);
/*!40000 ALTER TABLE `secondary_education_institution` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` (`id`, `first_name`, `middle_name`, `last_name`, `overall_rating`, `date_birth`, `date_receipt`, `student_ticket_number`, `email`, `city_id`, `secondary_education_institution_id`, `group_id`) VALUES (3,'Lybomur','Andrilovich','Salandjak',45,'1999-06-07','2015-08-08',5479657812,'Salandjak@gmail.com',1,1,1),(4,'Vasul','Stepanovuch','Kirichyk',50,'1999-05-06','2015-08-08',5668547865,'Kirichyk@gmail.com',2,2,1),(5,'Volodymyr','Igorovich','Hrebin',55,'1999-04-09','2015-08-08',5425987651,'Hrebin@gmail.com',3,3,1),(6,'Nazar','Stepanovuch','Grib',55,'1999-08-07','2015-08-08',5963489235,'Grib@gmail.com',1,1,1);
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `student_arrears`
--

LOCK TABLES `student_arrears` WRITE;
/*!40000 ALTER TABLE `student_arrears` DISABLE KEYS */;
INSERT INTO `student_arrears` VALUES (3,1),(6,1),(4,2),(5,2),(6,2);
/*!40000 ALTER TABLE `student_arrears` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'database n2'
--

--
-- Dumping routines for database 'database n2'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-11 14:13:55
