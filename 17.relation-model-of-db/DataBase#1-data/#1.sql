-- MySQL dump 10.13  Distrib 8.0.11, for Win64 (x86_64)
--
-- Host: localhost    Database: database n1
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `family_jewelry`
--

DROP TABLE IF EXISTS `family_jewelry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `family_jewelry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `estimated_cost` int(11) NOT NULL,
  `maximum_cost` int(11) NOT NULL,
  `minimum_cost` int(11) NOT NULL,
  `code_in_catalog` int(11) NOT NULL,
  `coefficient` float GENERATED ALWAYS AS ((sin(`minimum_cost`) + cos(`maximum_cost`))) VIRTUAL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `family_jewelry`
--

LOCK TABLES `family_jewelry` WRITE;
/*!40000 ALTER TABLE `family_jewelry` DISABLE KEYS */;
INSERT INTO `family_jewelry` (`id`, `name`, `estimated_cost`, `maximum_cost`, `minimum_cost`, `code_in_catalog`) VALUES (1,'picture flowers',5600,6200,4800,49358764),(2,'picture sea',8700,9500,8500,68234587);
/*!40000 ALTER TABLE `family_jewelry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `family_satellite`
--

DROP TABLE IF EXISTS `family_satellite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `family_satellite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `date_birth` date NOT NULL,
  `date_death` date DEFAULT NULL,
  `place_birth` varchar(45) NOT NULL,
  `place_death` varchar(45) DEFAULT NULL,
  `date_marriage` date NOT NULL,
  `info` varchar(150) GENERATED ALWAYS AS (concat(`first_name`,_utf8mb3' ',`last_name`,_utf8mb3' народ. ',dayofmonth(`date_birth`),_utf8mb3' дня ',year(`date_birth`),_utf8mb3'р. від різдва Христового')) VIRTUAL,
  `sex_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_family_member_sex1_idx` (`sex_id`),
  CONSTRAINT `fk_family_member_sex1` FOREIGN KEY (`sex_id`) REFERENCES `sex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `family_satellite`
--

LOCK TABLES `family_satellite` WRITE;
/*!40000 ALTER TABLE `family_satellite` DISABLE KEYS */;
INSERT INTO `family_satellite` (`id`, `first_name`, `last_name`, `date_birth`, `date_death`, `place_birth`, `place_death`, `date_marriage`, `sex_id`) VALUES (1,'Inna','Salandjak','1990-06-11',NULL,'c.Lviv, Sadova 15',NULL,'2013-10-05',2),(2,'Nazar','Grub','1915-02-11','1985-05-06','c.Lviv.Poljova 25','c.Lviv.Poljova 25','1940-07-02',1);
/*!40000 ALTER TABLE `family_satellite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `family_tree`
--

DROP TABLE IF EXISTS `family_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `family_tree` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `date_birth` date NOT NULL,
  `date_death` date DEFAULT NULL,
  `place_birth` varchar(45) NOT NULL,
  `place_death` varchar(45) DEFAULT NULL,
  `credit_card_number` bigint(20) DEFAULT NULL,
  `full_name` varchar(45) GENERATED ALWAYS AS (concat(`first_name`,_utf8mb3' ',`last_name`)) VIRTUAL,
  `sex_id` int(11) NOT NULL,
  `family_satellite_id` int(11) DEFAULT NULL,
  `family_tree_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_family_tree_sex1_idx` (`sex_id`),
  KEY `fk_family_tree_family_member1_idx` (`family_satellite_id`),
  KEY `fk_family_tree_family_tree1_idx` (`family_tree_id`),
  CONSTRAINT `fk_family_tree_family_member1` FOREIGN KEY (`family_satellite_id`) REFERENCES `family_satellite` (`id`),
  CONSTRAINT `fk_family_tree_family_tree1` FOREIGN KEY (`family_tree_id`) REFERENCES `family_tree` (`id`),
  CONSTRAINT `fk_family_tree_sex1` FOREIGN KEY (`sex_id`) REFERENCES `sex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `family_tree`
--

LOCK TABLES `family_tree` WRITE;
/*!40000 ALTER TABLE `family_tree` DISABLE KEYS */;
INSERT INTO `family_tree` (`id`, `first_name`, `last_name`, `date_birth`, `date_death`, `place_birth`, `place_death`, `credit_card_number`, `sex_id`, `family_satellite_id`, `family_tree_id`) VALUES (1,'Taras','Salandjak','1988-02-08',NULL,'c.Lviv.Poljova 48',NULL,682348956478236,1,1,2),(2,'Olja','Salandjak','1968-03-09',NULL,'c.Lviv.Poljova 48',NULL,254897532146587,2,2,NULL);
/*!40000 ALTER TABLE `family_tree` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `family_tree_family_jewelry`
--

DROP TABLE IF EXISTS `family_tree_family_jewelry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `family_tree_family_jewelry` (
  `family_tree_id` int(11) NOT NULL,
  `family_jewelry_id` int(11) NOT NULL,
  PRIMARY KEY (`family_tree_id`,`family_jewelry_id`),
  KEY `fk_family_tree_has_family_jewelry_family_jewelry1_idx` (`family_jewelry_id`),
  KEY `fk_family_tree_has_family_jewelry_family_tree1_idx` (`family_tree_id`),
  CONSTRAINT `fk_family_tree_has_family_jewelry_family_jewelry1` FOREIGN KEY (`family_jewelry_id`) REFERENCES `family_jewelry` (`id`),
  CONSTRAINT `fk_family_tree_has_family_jewelry_family_tree1` FOREIGN KEY (`family_tree_id`) REFERENCES `family_tree` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `family_tree_family_jewelry`
--

LOCK TABLES `family_tree_family_jewelry` WRITE;
/*!40000 ALTER TABLE `family_tree_family_jewelry` DISABLE KEYS */;
INSERT INTO `family_tree_family_jewelry` VALUES (2,1),(1,2);
/*!40000 ALTER TABLE `family_tree_family_jewelry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sex`
--

DROP TABLE IF EXISTS `sex`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `sex` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sex` enum('man','woman') NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sex`
--

LOCK TABLES `sex` WRITE;
/*!40000 ALTER TABLE `sex` DISABLE KEYS */;
INSERT INTO `sex` VALUES (1,'man'),(2,'woman');
/*!40000 ALTER TABLE `sex` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-11 12:54:20
