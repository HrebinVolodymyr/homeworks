-- MySQL dump 10.13  Distrib 8.0.11, for Win64 (x86_64)
--
-- Host: localhost    Database: database n1
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `family_jewelry`
--

LOCK TABLES `family_jewelry` WRITE;
/*!40000 ALTER TABLE `family_jewelry` DISABLE KEYS */;
INSERT INTO `family_jewelry` (`id`, `name`, `estimated_cost`, `maximum_cost`, `minimum_cost`, `code_in_catalog`) VALUES (1,'picture flowers',5600,6200,4800,49358764),(2,'picture sea',8700,9500,8500,68234587);
/*!40000 ALTER TABLE `family_jewelry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `family_satellite`
--

LOCK TABLES `family_satellite` WRITE;
/*!40000 ALTER TABLE `family_satellite` DISABLE KEYS */;
INSERT INTO `family_satellite` (`id`, `first_name`, `last_name`, `date_birth`, `date_death`, `place_birth`, `place_death`, `date_marriage`, `sex_id`) VALUES (1,'Inna','Salandjak','1990-06-11',NULL,'c.Lviv, Sadova 15',NULL,'2013-10-05',2),(2,'Nazar','Grub','1915-02-11','1985-05-06','c.Lviv.Poljova 25','c.Lviv.Poljova 25','1940-07-02',1);
/*!40000 ALTER TABLE `family_satellite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `family_tree`
--

LOCK TABLES `family_tree` WRITE;
/*!40000 ALTER TABLE `family_tree` DISABLE KEYS */;
INSERT INTO `family_tree` (`id`, `first_name`, `last_name`, `date_birth`, `date_death`, `place_birth`, `place_death`, `credit_card_number`, `sex_id`, `family_satellite_id`, `family_tree_id`) VALUES (1,'Taras','Salandjak','1988-02-08',NULL,'c.Lviv.Poljova 48',NULL,682348956478236,1,1,2),(2,'Olja','Salandjak','1968-03-09',NULL,'c.Lviv.Poljova 48',NULL,254897532146587,2,2,NULL);
/*!40000 ALTER TABLE `family_tree` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `family_tree_family_jewelry`
--

LOCK TABLES `family_tree_family_jewelry` WRITE;
/*!40000 ALTER TABLE `family_tree_family_jewelry` DISABLE KEYS */;
INSERT INTO `family_tree_family_jewelry` VALUES (2,1),(1,2);
/*!40000 ALTER TABLE `family_tree_family_jewelry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `sex`
--

LOCK TABLES `sex` WRITE;
/*!40000 ALTER TABLE `sex` DISABLE KEYS */;
INSERT INTO `sex` VALUES (1,'man'),(2,'woman');
/*!40000 ALTER TABLE `sex` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'database n1'
--

--
-- Dumping routines for database 'database n1'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-11 14:13:17
