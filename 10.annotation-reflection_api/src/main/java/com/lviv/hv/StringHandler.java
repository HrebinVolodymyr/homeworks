package com.lviv.hv;

public class StringHandler {

  public int getAmountOfNumbers(long l) {
    return Long.toString(l).length();
  }

  public String parseDouble(double d) {
    return Double.toString(d);
  }

  public String concat(int first, int second) {
    return Integer.toString(first) + Integer.toString(second);
  }

  public String concat(String string, int... args) {
    String result = string;
    for (int arg : args) {
      result += arg;
    }
    return result;
  }

  public int getCharsCount(String... args) {
    String result = "";
    for (String arg : args) {
      result += arg;
    }
    return result.length();
  }

}
