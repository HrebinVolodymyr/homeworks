package com.lviv.hv;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

public class ClassParser {

  public static final String YELLOW = "\033[43m";
  public static final String WHITE = "\033[47m";
  public static final String RESET = "\033[0m";
  public static final String BLACK_BOLD = "\033[1;30m";
  public static final String RED = "\033[41m";

  public static void showInfo(Object object) {

    Class clazz = object.getClass();

    System.out.println(RED + BLACK_BOLD);
    System.out.println("                CLASS: " + clazz.getName());
    printAnnotation(clazz.getDeclaredAnnotations());
    System.out.println();
    System.out.print(RESET);

    System.out.println(BLACK_BOLD + YELLOW);
    System.out.println(
        "********************************* F I E L D S *************************************");
    System.out.println();
    System.out.print(RESET);
    Field[] fields = clazz.getDeclaredFields();
    int countFields = 1;
    for (Field field : fields) {
      System.out.println(
          getColor(countFields) + countFields + ")  " + field.getName() + ": "
              + field.getType().getCanonicalName());
      countFields++;
      Annotation[] annotations = field.getAnnotations();
      printAnnotation(annotations);
    }

    System.out.println(BLACK_BOLD + YELLOW);
    System.out.println(
        "************************** C O N S T R U C T O R S ********************************");
    System.out.println();
    System.out.print(RESET);
    Constructor[] constructors = clazz.getDeclaredConstructors();
    int countConstructor = 1;
    for (Constructor constructor : constructors) {
      StringBuilder string = new StringBuilder();
      string.append(getColor(countConstructor) + countConstructor + ")  ")
          .append(constructor.getName())
          .append("(");
      Parameter[] parameters = constructor.getParameters();
      for (Parameter parameter : parameters) {
        string.append(parameter.getType().getCanonicalName())
            .append(" ")
            .append(parameter.getName())
            .append(", ");
      }
      System.out.println(string.append(")"));
      countConstructor++;
      Class[] exceptionTypes = constructor.getExceptionTypes();
      printExceptions(exceptionTypes);
      Annotation[] annotations = constructor.getAnnotations();
      printAnnotation(annotations);
    }

    System.out.println(BLACK_BOLD + YELLOW);
    System.out.println(
        "******************************* M E T H O D S *************************************");
    System.out.println();
    System.out.print(RESET);
    Method[] methods = clazz.getDeclaredMethods();
    int countMethod = 1;
    for (Method method : methods) {
      StringBuilder string = new StringBuilder();
      string.append(getColor(countMethod) + countMethod + ") ")
          .append(method.getName())
          .append("(");
      Parameter[] parameters = method.getParameters();
      for (Parameter parameter : parameters) {
        string.append(parameter.getType().getCanonicalName())
            .append(" ")
            .append(parameter.getName())
            .append(", ");
      }
      System.out.println(string.append(")"));
      countMethod++;
      System.out
          .println("    RETURN: " + method.getReturnType().getCanonicalName());
      Class[] exceptionTypes = method.getExceptionTypes();
      printExceptions(exceptionTypes);
      Annotation[] annotations = method.getAnnotations();
      printAnnotation(annotations);
    }
  }

  public static void printAnnotation(Annotation[] annotations) {
    if (annotations.length >= 1) {
      StringBuilder stringAnnotation = new StringBuilder()
          .append("    ANNOTATIONS: ");
      for (Annotation annotation : annotations) {
        stringAnnotation.append(annotation.annotationType().getCanonicalName())
            .append(", ");
      }
      System.out.println(stringAnnotation);
    }
  }

  public static void printExceptions(Class[] classes) {
    if (classes.length >= 1) {
      StringBuilder stringClasses = new StringBuilder()
          .append("    EXCEPTIONS: ");
      for (Class clazz : classes) {
        stringClasses.append(clazz.getCanonicalName()).append(", ");
      }
      System.out.println(stringClasses);
    }
  }

  public static String getColor(int count) {
    if (count % 2 == 0) {
      return RESET;
    } else {
      return WHITE;
    }
  }
}
