package com.lviv.hv;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {

  public static void main(String... args) {
    ConnectionProperty property = ConnectionPropertyFactory.getConnectionProperty("test");
    System.out.println(property.toString());

//    invokeMethods();
//
//    ClassParser.showInfo("");
//    ClassParser.showInfo(new ConnectionProperty("test"));

  }

  public static void invokeMethods() {
    StringHandler stringHandler = new StringHandler();

    try {
      //  public int getAmountOfNumbers(long l)
      Method getAmountOfNumbers =
          stringHandler.getClass().getDeclaredMethod("getAmountOfNumbers", long.class);
      int amountOfNumbers = (Integer) getAmountOfNumbers.invoke(stringHandler, 125689023L);
      System.out.println("Amount of numbers(125689023): " + amountOfNumbers);

      //  public String parseDouble(double d)
      Method parseDouble =
          stringHandler.getClass().getDeclaredMethod("parseDouble", double.class);
      String stringDouble = (String) parseDouble.invoke(stringHandler, 125.689023);
      System.out.println("Parse double(125.689023): " + stringDouble);

      //public String concat(int first, int second)
      Method concatInt =
          stringHandler.getClass().getDeclaredMethod("concat", int.class, int.class);
      String stringInt = (String) concatInt.invoke(stringHandler, 2, 8);
      System.out.println("Concat int(2, 8): " + stringInt);

      //public String concat(String string, int... args)
      Method concatStringInts =
          stringHandler.getClass().getDeclaredMethod("concat", String.class, int[].class);
      String stringInts =
          (String) concatStringInts.invoke(stringHandler, "Year ", new int[]{2, 0, 1, 8});
      System.out.println("Concat string and ints(Year , {2, 0, 1, 8}): " + stringInts);

      //  public int getCharsCount(String... args)
      Method getCharsCount =
          stringHandler.getClass().getDeclaredMethod("getCharsCount", String[].class);
      String[] words = new String[]{"Hello", "world"};
      int charsCount =
          (Integer) getCharsCount.invoke(stringHandler, new String[][]{words});
      System.out.println("Concat strings(Hello , world): " + charsCount);

    } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
      e.printStackTrace();
    }
  }
}
