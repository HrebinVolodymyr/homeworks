package com.lviv.hv;

import jdk.nashorn.internal.objects.annotations.Constructor;
import jdk.nashorn.internal.runtime.logging.Logger;

@Logger
public class ConnectionProperty {

  @Logger
  ConnectionProperty(String name) {
    this.name = name;
  }

  private String name;

  @Parameter(value = "localhost")
  private String host;

  @Parameter(value = "8080")
  private Integer port;

  @Parameter(value = "127.0.0.1")
  private String ipAddress;

  public String getName() {
    return name;
  }

  public void setName(String name) throws RuntimeException {
    this.name = name;
  }

  @Logger
  public String getHost() {
    return host;
  }

  public void setHost(String host) {
    this.host = host;
  }

  public Integer getPort() {
    return port;
  }

  public void setPort(Integer port) {
    this.port = port;
  }

  public String getIpAddress() {
    return ipAddress;
  }

  public void setIpAddress(String ipAddress) {
    this.ipAddress = ipAddress;
  }

  @Override
  public String toString() {
    return "ConnectionProperty{" +
        "name='" + name + '\'' +
        ", host='" + host + '\'' +
        ", port=" + port +
        ", ipAddress='" + ipAddress + '\'' +
        '}';
  }
}
