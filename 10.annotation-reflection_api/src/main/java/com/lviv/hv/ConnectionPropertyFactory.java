package com.lviv.hv;

import java.lang.reflect.Field;

public class ConnectionPropertyFactory {

  public static ConnectionProperty getConnectionProperty(String name) {
    ConnectionProperty connectionProperty = new ConnectionProperty(name);
    Field[] fields = connectionProperty.getClass().getDeclaredFields();
    for (Field field : fields) {
      if (field.isAnnotationPresent(Parameter.class)) {
        field.setAccessible(true);
        Parameter parameter = field.getDeclaredAnnotation(Parameter.class);
        System.out.println(
            "Field name: " + field.getName() + " - @Parameter.value = " + parameter.value());
        try {
          if (field.getType() == Integer.class) {
            field.set(connectionProperty, Integer.parseInt(parameter.value()));
            continue;
          }
          field.set(connectionProperty, parameter.value());
        } catch (IllegalAccessException e) {
          e.printStackTrace();
        }
      }
    }
    return connectionProperty;
  }
}
